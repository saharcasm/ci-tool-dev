﻿import { Component,ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    constructor(public router: Router) {}
    ngOnInit() {
    console.log("app module	");
        if (this.router.url === '/') {
            this.router.navigate(['/search']);

        }
    }
}
