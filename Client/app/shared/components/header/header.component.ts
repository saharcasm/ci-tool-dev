﻿import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public username: string;
    public userdata: any[];

    constructor(private translate: TranslateService, public router: Router, private http: Http) {
        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992) {
                this.toggleSidebar();
            }
        });
        //this.loggedInUser = localStorage.getItem('loggedInUser');
    }

    ngOnInit() {
        // Get the username when hosted, if empty put in Admin as default
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.get('/.auth/me', { headers })
            .map(x => x.json())
            .subscribe(ud => {
                this.userdata = ud;

                for (var prop in this.userdata[0]) {
                    if (prop == "user_id") {
                        this.username = this.userdata[0][prop]
                    }
                    else {
                        this.username = "Administrator";
                    }
                    console.log("Username: " + this.username);
                }
            },
            err => {
                console.log(err);
                this.username = "Administrator";
            });
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('push-right');
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        this.deleteAllCookies();
        window.location.href = "https://login.windows.net/common/oauth2/logout";
    }

    changeLang(language: string) {
        //this.translate.use(language);
    }

    deleteAllCookies() {
        let cookies = document.cookie.split(";");

        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i];
            let eqPos = cookie.indexOf("=");
            let name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
}
