var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Http, Headers } from '@angular/http';
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(translate, router, http) {
        var _this = this;
        this.translate = translate;
        this.router = router;
        this.http = http;
        this.router.events.subscribe(function (val) {
            if (val instanceof NavigationEnd && window.innerWidth <= 992) {
                _this.toggleSidebar();
            }
        });
        //this.loggedInUser = localStorage.getItem('loggedInUser');
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Get the username when hosted, if empty put in Admin as default
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.get('/.auth/me', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (ud) {
            _this.userdata = ud;
            for (var prop in _this.userdata[0]) {
                if (prop == "user_id") {
                    _this.username = _this.userdata[0][prop];
                }
                else {
                    _this.username = "Administrator";
                }
                console.log("Username: " + _this.username);
            }
        }, function (err) {
            console.log(err);
            _this.username = "Administrator";
        });
    };
    HeaderComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('push-right');
    };
    HeaderComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    HeaderComponent.prototype.onLoggedout = function () {
        this.deleteAllCookies();
        window.location.href = "https://login.windows.net/common/oauth2/logout";
    };
    HeaderComponent.prototype.changeLang = function (language) {
        //this.translate.use(language);
    };
    HeaderComponent.prototype.deleteAllCookies = function () {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name_1 = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name_1 + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    };
    HeaderComponent = __decorate([
        Component({
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.scss']
        }),
        __metadata("design:paramtypes", [TranslateService, Router, Http])
    ], HeaderComponent);
    return HeaderComponent;
}());
export { HeaderComponent };
