var ProductCategory = /** @class */ (function () {
    function ProductCategory() {
    }
    return ProductCategory;
}());
export { ProductCategory };
var Brand = /** @class */ (function () {
    function Brand() {
    }
    return Brand;
}());
export { Brand };
var ProductType = /** @class */ (function () {
    function ProductType() {
    }
    return ProductType;
}());
export { ProductType };
var ProductTypeSummary = /** @class */ (function () {
    function ProductTypeSummary() {
    }
    return ProductTypeSummary;
}());
export { ProductTypeSummary };
var Term = /** @class */ (function () {
    function Term() {
    }
    return Term;
}());
export { Term };
var TermSummary = /** @class */ (function () {
    function TermSummary() {
    }
    return TermSummary;
}());
export { TermSummary };
var SearchResult = /** @class */ (function () {
    function SearchResult() {
    }
    return SearchResult;
}());
export { SearchResult };
var ProductName = /** @class */ (function () {
    function ProductName() {
    }
    return ProductName;
}());
export { ProductName };
var ProductNameSummary = /** @class */ (function () {
    function ProductNameSummary() {
    }
    return ProductNameSummary;
}());
export { ProductNameSummary };
var FrequentFlyerProgram = /** @class */ (function () {
    function FrequentFlyerProgram() {
    }
    return FrequentFlyerProgram;
}());
export { FrequentFlyerProgram };
var MobileSearchResult = /** @class */ (function () {
    function MobileSearchResult() {
    }
    return MobileSearchResult;
}());
export { MobileSearchResult };
var MobilePlan = /** @class */ (function () {
    function MobilePlan() {
    }
    return MobilePlan;
}());
export { MobilePlan };
