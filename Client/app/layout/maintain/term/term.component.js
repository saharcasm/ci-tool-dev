var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Term } from "../../models";
import { Http, Headers } from '@angular/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
var TermComponent = /** @class */ (function () {
    function TermComponent(http, modalService) {
        this.http = http;
        this.modalService = modalService;
    }
    TermComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadTerms();
        // Get the username when hosted, if empty put in Admin as default
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/.auth/me', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (ud) {
            _this.userdata = ud;
            for (var prop in _this.userdata[0]) {
                if (prop == "user_id") {
                    _this.username = _this.userdata[0][prop];
                }
                else {
                    _this.username = "Administrator";
                }
                console.log("Username: " + _this.username);
            }
        }, function (err) {
            console.log(err);
            _this.username = "Administrator";
        });
    };
    TermComponent.prototype.loadTerms = function () {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/api/term/summary', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (sr) {
            _this.terms = sr;
        });
    };
    TermComponent.prototype.openAdd = function (addTerm) {
        this.term = new Term();
        this.modalMode = "Add";
        this.modalRef = this.modalService.open(addTerm);
    };
    TermComponent.prototype.openEdit = function (addTerm, termId) {
        var _this = this;
        this.modalMode = "Edit";
        this.modalRef = this.modalService.open(addTerm);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        //Get the product
        this.http.get('/api/term/' + termId, { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (t) {
            _this.term = t;
        });
    };
    TermComponent.prototype.saveEdit = function () {
        var _this = this;
        if (!this.term.termValue || !this.term.unit || !this.term.productCategoryId) {
            return;
        }
        else {
            // Save the new product name
            var body = "term=" + this.term.termValue + "&unit=" + this.term.unit + "&recordStatus=" + this.term.recordStatus + "&productCategoryId=" + this.term.productCategoryId + "&username=" + this.username;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Cache-Control', 'no-cache');
            headers.append('Pragma', 'no-cache');
            this.http.put('/api/term/' + this.term.termId, body, { headers: headers }).subscribe(function (data) {
                console.log('post ok');
                _this.loadTerms();
                _this.closeModal();
            }, function (error) {
                console.log(JSON.stringify(error.json()));
            });
            this.closeModal();
        }
    };
    TermComponent.prototype.saveAdd = function () {
        var _this = this;
        if (!this.term.termValue || !this.term.unit || !this.term.productCategoryId) {
            return;
        }
        else {
            // Save the new product name
            var body = "term=" + this.term.termValue + "&unit=" + this.term.unit + "&productCategoryId=" + this.term.productCategoryId + "&username=" + this.username;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Cache-Control', 'no-cache');
            headers.append('Pragma', 'no-cache');
            this.http.post('/api/term/', body, { headers: headers }).subscribe(function (data) {
                console.log('post ok');
                _this.loadTerms();
                _this.closeModal();
            }, function (error) {
                console.log(JSON.stringify(error.json()));
            });
            this.closeModal();
        }
    };
    TermComponent.prototype.closeModal = function () {
        this.modalRef.close();
    };
    TermComponent = __decorate([
        Component({
            selector: 'app-term',
            templateUrl: './term.component.html'
        }),
        __metadata("design:paramtypes", [Http, NgbModal])
    ], TermComponent);
    return TermComponent;
}());
export { TermComponent };
