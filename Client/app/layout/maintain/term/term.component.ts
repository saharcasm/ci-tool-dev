﻿import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TermSummary, Term } from "../../models";
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-term',
    templateUrl: './term.component.html'
})
export class TermComponent implements OnInit {
    public terms: TermSummary;
    public term: Term;
    public modalMode: string;
    private modalRef: any;
    public username: string;
    public userdata: any[];

    constructor(private http: Http, private modalService: NgbModal) { }
    ngOnInit() {
        this.loadTerms();

        // Get the username when hosted, if empty put in Admin as default
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/.auth/me', { headers })
            .map(x => x.json())
            .subscribe(ud => {
                this.userdata = ud;

                for (var prop in this.userdata[0]) {
                    if (prop == "user_id") {
                        this.username = this.userdata[0][prop]
                    }
                    else {
                        this.username = "Administrator";
                    }
                    console.log("Username: " + this.username);
                }
            },
            err => {
                console.log(err);
                this.username = "Administrator";
            });
    }

    private loadTerms() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        this.http.get('/api/term/summary', { headers })
            .map(x => x.json())
            .subscribe(sr => {
                this.terms = sr;
            });
    }

    openAdd(addTerm) {
        this.term = new Term();
        this.modalMode = "Add";
        this.modalRef = this.modalService.open(addTerm);
    }

    openEdit(addTerm, termId)
    {
        this.modalMode = "Edit";
        this.modalRef = this.modalService.open(addTerm);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        //Get the product
        this.http.get('/api/term/' + termId, { headers })
            .map(x => x.json())
            .subscribe(t => {
                this.term = t;
            });
    }

    saveEdit() {
        if (!this.term.termValue || !this.term.unit || !this.term.productCategoryId) {
            return;
        }
        else {
            // Save the new product name
            var body = "term=" + this.term.termValue + "&unit=" + this.term.unit + "&recordStatus=" + this.term.recordStatus + "&productCategoryId=" + this.term.productCategoryId + "&username=" + this.username;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Cache-Control', 'no-cache');
            headers.append('Pragma', 'no-cache');

            this.http.put('/api/term/' + this.term.termId, body, { headers }).subscribe(data => {
                console.log('post ok');
                this.loadTerms();
                this.closeModal();
            }, error => {
                console.log(JSON.stringify(error.json()));
            });

            this.closeModal();
        }
    }

    saveAdd() {
        if (!this.term.termValue || !this.term.unit || !this.term.productCategoryId) {
            return;
        }
        else {
            // Save the new product name
            var body = "term=" + this.term.termValue + "&unit=" + this.term.unit + "&productCategoryId=" + this.term.productCategoryId + "&username=" + this.username;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Cache-Control', 'no-cache');
            headers.append('Pragma', 'no-cache');

            this.http.post('/api/term/', body, { headers }).subscribe(data => {
                console.log('post ok');
                this.loadTerms();
                this.closeModal();
            }, error => {
                console.log(JSON.stringify(error.json()));
            });

            this.closeModal();
        }
    }

    public closeModal() {
        this.modalRef.close();
    }
}
