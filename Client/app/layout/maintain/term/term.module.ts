﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TermRoutingModule } from './term-routing.module';
import { TermComponent } from './term.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
      CommonModule,
      TermRoutingModule,
      NgbModule.forRoot(),
      FormsModule
  ],
  declarations: [TermComponent]
})
export class TermModule { }
