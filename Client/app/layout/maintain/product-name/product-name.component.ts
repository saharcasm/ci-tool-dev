﻿import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ProductName, Brand, ProductNameSummary } from "../../models";
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MatchesProductCategoryPipe } from '../../filters.pipe';

@Component({
    selector: 'app-maintain-productname',
    templateUrl: './product-name.component.html'
})
export class ProductNameComponent implements OnInit {
    public products: ProductNameSummary[];
    public brands: Brand[];
    public product: ProductName;
    public modalMode: string;
    public selectedBrand: number;
    private modalRef: any;
    public username: string;
    public userdata: any[];

    constructor(private http: Http, private modalService: NgbModal) { }

    openEdit(content, productNameId) {
        this.modalMode = "Edit";
        this.modalRef = this.modalService.open(content);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        //Get the product
        this.http.get('/api/productname/' + productNameId, { headers })
            .map(x => x.json())
            .subscribe(pn => {
                this.product = pn;
                this.loadBrands();
            });
    };

    openAdd(content) {
        this.modalMode = "Add";
        this.modalRef = this.modalService.open(content);
    };

    public closeModal() {
        this.modalRef.close();
    }

    saveEdit() {
        if (this.product.productNameName == undefined || this.product.productNameName == "") {
            return;
        }
        else {
            // Save the new product name
            var brandId = (this.product.brandId == undefined) ? 0 : this.product.brandId;
            var body = "productname=" + this.product.productNameName + "&brandId=" + brandId + "&recordStatus=" + this.product.recordStatus + "&username=" + this.username + "&categoryId=" + this.product.productCategoryId;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Cache-Control', 'no-cache');
            headers.append('Pragma', 'no-cache');
            this.http.put('/api/productname/' + this.product.productNameId, body, { headers }).subscribe(data => {
                console.log('post ok');
                this.loadProductNames();
                this.closeModal();
            }, error => {
                console.log(JSON.stringify(error.json()));
            });

            this.closeModal();
        }
    }

    saveAdd() {
        if (this.product.productNameName == undefined || this.product.productNameName == "") {
            return;
        }
        else {
            // Save the new product name
            var brandId = (this.product.brandId == undefined) ? 0 : this.product.brandId;
            var body = "productname=" + this.product.productNameName + "&brandId=" + brandId + "&username=" + this.username + "&categoryId=" + this.product.productCategoryId;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            this.http.post('/api/productname/', body, { headers }).subscribe(data => {
                console.log('post ok');
                this.loadProductNames();
                this.closeModal();
            }, error => {
                console.log(JSON.stringify(error.json()));
            });

        }
    }


    ngOnInit() {
        this.loadProductNames();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        // Get the username when hosted, if empty put in Admin as default
        this.http.get('/.auth/me', { headers })
            .map(x => x.json())
            .subscribe(ud => {
                this.userdata = ud;

                for (var prop in this.userdata[0]) {
                    if (prop == "user_id") {
                        this.username = this.userdata[0][prop]
                    }
                    else {
                        this.username = "Administrator";
                    }
                    console.log("Username: " + this.username);
                }
            },
            err => {
                console.log(err);
                this.username = "Administrator";
            });
    }


    private loadProductNames()
    {
        this.product = new ProductName();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        this.http.get('/api/productname/summary', { headers })
            .map(x => x.json())
            .subscribe(sr => {
                console.log("testing", sr);
                this.products=sr.sort( function(name1, name2) {
                    if ( name1.brandName < name2.brandName ){
                        return -1;
                    }else if( name1.brandName > name2.brandName ){
                        return 1;
                    }else{
                        return 0;    
                    }
                });
            });
    }

    loadBrands() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        // Get the brands
        this.http.get('/api/brand/active', { headers })
            .map(x => x.json())
            .subscribe(b => {
                this.brands = new MatchesProductCategoryPipe().transform(b, this.product.productCategoryId.toString());
            });
    }

}
