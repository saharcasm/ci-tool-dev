var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ProductName } from "../../models";
import { Http, Headers } from '@angular/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatchesProductCategoryPipe } from '../../filters.pipe';
var ProductNameComponent = /** @class */ (function () {
    function ProductNameComponent(http, modalService) {
        this.http = http;
        this.modalService = modalService;
    }
    ProductNameComponent.prototype.openEdit = function (content, productNameId) {
        var _this = this;
        this.modalMode = "Edit";
        this.modalRef = this.modalService.open(content);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        //Get the product
        this.http.get('/api/productname/' + productNameId, { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (pn) {
            _this.product = pn;
            _this.loadBrands();
        });
    };
    ;
    ProductNameComponent.prototype.openAdd = function (content) {
        this.modalMode = "Add";
        this.modalRef = this.modalService.open(content);
    };
    ;
    ProductNameComponent.prototype.closeModal = function () {
        this.modalRef.close();
    };
    ProductNameComponent.prototype.saveEdit = function () {
        var _this = this;
        if (this.product.productNameName == undefined || this.product.productNameName == "") {
            return;
        }
        else {
            // Save the new product name
            var brandId = (this.product.brandId == undefined) ? 0 : this.product.brandId;
            var body = "productname=" + this.product.productNameName + "&brandId=" + brandId + "&recordStatus=" + this.product.recordStatus + "&username=" + this.username + "&categoryId=" + this.product.productCategoryId;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Cache-Control', 'no-cache');
            headers.append('Pragma', 'no-cache');
            this.http.put('/api/productname/' + this.product.productNameId, body, { headers: headers }).subscribe(function (data) {
                console.log('post ok');
                _this.loadProductNames();
                _this.closeModal();
            }, function (error) {
                console.log(JSON.stringify(error.json()));
            });
            this.closeModal();
        }
    };
    ProductNameComponent.prototype.saveAdd = function () {
        var _this = this;
        if (this.product.productNameName == undefined || this.product.productNameName == "") {
            return;
        }
        else {
            // Save the new product name
            var brandId = (this.product.brandId == undefined) ? 0 : this.product.brandId;
            var body = "productname=" + this.product.productNameName + "&brandId=" + brandId + "&username=" + this.username + "&categoryId=" + this.product.productCategoryId;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            this.http.post('/api/productname/', body, { headers: headers }).subscribe(function (data) {
                console.log('post ok');
                _this.loadProductNames();
                _this.closeModal();
            }, function (error) {
                console.log(JSON.stringify(error.json()));
            });
        }
    };
    ProductNameComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadProductNames();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        // Get the username when hosted, if empty put in Admin as default
        this.http.get('/.auth/me', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (ud) {
            _this.userdata = ud;
            for (var prop in _this.userdata[0]) {
                if (prop == "user_id") {
                    _this.username = _this.userdata[0][prop];
                }
                else {
                    _this.username = "Administrator";
                }
                console.log("Username: " + _this.username);
            }
        }, function (err) {
            console.log(err);
            _this.username = "Administrator";
        });
    };
    ProductNameComponent.prototype.loadProductNames = function () {
        var _this = this;
        this.product = new ProductName();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/api/productname/summary', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (sr) {
            console.log("testing", sr);
            _this.products = sr.sort(function (name1, name2) {
                if (name1.brandName < name2.brandName) {
                    return -1;
                }
                else if (name1.brandName > name2.brandName) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        });
    };
    ProductNameComponent.prototype.loadBrands = function () {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        // Get the brands
        this.http.get('/api/brand/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (b) {
            _this.brands = new MatchesProductCategoryPipe().transform(b, _this.product.productCategoryId.toString());
        });
    };
    ProductNameComponent = __decorate([
        Component({
            selector: 'app-maintain-productname',
            templateUrl: './product-name.component.html'
        }),
        __metadata("design:paramtypes", [Http, NgbModal])
    ], ProductNameComponent);
    return ProductNameComponent;
}());
export { ProductNameComponent };
