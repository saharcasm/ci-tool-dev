﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductNameComponent } from './product-name.component';

const routes: Routes = [
    { path: '', component: ProductNameComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductNameRoutingModule { }
