﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductNameRoutingModule } from './product-name-routing.module';
import { ProductNameComponent } from './product-name.component';
import { FormsModule } from '@angular/forms';
import { PipeModule } from '../../pipe.module';

@NgModule({
  imports: [
    CommonModule,
    ProductNameRoutingModule,
    NgbModule.forRoot(),
    FormsModule,
    PipeModule
  ],
  declarations: [ProductNameComponent]
})
export class ProductNameModule { }
