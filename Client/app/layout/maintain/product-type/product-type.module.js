var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductTypeRoutingModule } from './product-type-routing.module';
import { ProductTypeComponent } from './product-type.component';
import { FormsModule } from '@angular/forms';
var ProductTypeModule = /** @class */ (function () {
    function ProductTypeModule() {
    }
    ProductTypeModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                ProductTypeRoutingModule,
                NgbModule.forRoot(),
                FormsModule
            ],
            declarations: [ProductTypeComponent]
        })
    ], ProductTypeModule);
    return ProductTypeModule;
}());
export { ProductTypeModule };
