var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ProductType } from "../../models";
import { Http, Headers } from '@angular/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
var ProductTypeComponent = /** @class */ (function () {
    function ProductTypeComponent(http, modalService) {
        this.http = http;
        this.modalService = modalService;
    }
    ProductTypeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadProductTypes();
        // Get the username when hosted, if empty put in Admin as default
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/.auth/me', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (ud) {
            _this.userdata = ud;
            for (var prop in _this.userdata[0]) {
                if (prop == "user_id") {
                    _this.username = _this.userdata[0][prop];
                }
                else {
                    _this.username = "Administrator";
                }
                console.log("Username: " + _this.username);
            }
        }, function (err) {
            console.log(err);
            _this.username = "Administrator";
        });
    };
    ProductTypeComponent.prototype.loadProductTypes = function () {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/api/producttype/summary', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (sr) {
            _this.productTypes = sr;
        });
    };
    ProductTypeComponent.prototype.openAdd = function (content) {
        this.productType = new ProductType();
        this.modalMode = "Add";
        this.modalRef = this.modalService.open(content);
    };
    ;
    ProductTypeComponent.prototype.openEdit = function (content, productTypeId) {
        var _this = this;
        this.modalMode = "Edit";
        this.modalRef = this.modalService.open(content);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        //Get the product
        this.http.get('/api/producttype/' + productTypeId, { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (pt) {
            _this.productType = pt;
        });
    };
    ;
    ProductTypeComponent.prototype.closeModal = function () {
        this.modalRef.close();
    };
    ProductTypeComponent.prototype.saveEdit = function () {
        var _this = this;
        if (!this.productType.productTypeName || !this.productType.productCategoryId || !this.productType.productTypeCategoryId) {
            return;
        }
        else {
            // Save the new product name
            var body = "productTypeName=" + this.productType.productTypeName + "&productTypeCategoryId=" + this.productType.productTypeCategoryId + "&recordStatus=" + this.productType.recordStatus + "&productCategoryId=" + this.productType.productCategoryId + "&username=" + this.username;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Cache-Control', 'no-cache');
            headers.append('Pragma', 'no-cache');
            this.http.put('/api/producttype/' + this.productType.productTypeId, body, { headers: headers }).subscribe(function (data) {
                console.log('post ok');
                _this.loadProductTypes();
                _this.closeModal();
            }, function (error) {
                console.log(JSON.stringify(error.json()));
            });
            this.closeModal();
        }
    };
    ProductTypeComponent.prototype.saveAdd = function () {
        var _this = this;
        if (!this.productType.productTypeName || !this.productType.productCategoryId || !this.productType.productTypeCategoryId) {
            return;
        }
        else {
            // Save the new product name
            var body = "productTypeName=" + this.productType.productTypeName + "&productTypeCategoryId=" + this.productType.productTypeCategoryId + "&productCategoryId=" + this.productType.productCategoryId + "&username=" + this.username;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            this.http.post('/api/producttype/', body, { headers: headers }).subscribe(function (data) {
                console.log('post ok');
                _this.loadProductTypes();
                _this.closeModal();
            }, function (error) {
                console.log(JSON.stringify(error.json()));
            });
            this.closeModal();
        }
    };
    ProductTypeComponent = __decorate([
        Component({
            selector: 'app-blank-page',
            templateUrl: './product-type.component.html'
        }),
        __metadata("design:paramtypes", [Http, NgbModal])
    ], ProductTypeComponent);
    return ProductTypeComponent;
}());
export { ProductTypeComponent };
