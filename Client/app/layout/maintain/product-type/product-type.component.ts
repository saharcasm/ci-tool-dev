﻿import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ProductType, ProductTypeSummary } from "../../models";
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-blank-page',
    templateUrl: './product-type.component.html'
})
export class ProductTypeComponent implements OnInit {
    public productTypes: ProductTypeSummary;
    public productType: ProductType;
    public modalMode: string;
    private modalRef: any;
    public username: string;
    public userdata: any[];

    constructor(private http: Http, private modalService: NgbModal) { }
    ngOnInit() {
        this.loadProductTypes();

        // Get the username when hosted, if empty put in Admin as default
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/.auth/me', { headers })
            .map(x => x.json())
            .subscribe(ud => {
                this.userdata = ud;

                for (var prop in this.userdata[0]) {
                    if (prop == "user_id") {
                        this.username = this.userdata[0][prop]
                    }
                    else {
                        this.username = "Administrator";
                    }
                    console.log("Username: " + this.username);
                }
            },
            err => {
                console.log(err);
                this.username = "Administrator";
            });
    }

    private loadProductTypes() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        this.http.get('/api/producttype/summary', { headers })
            .map(x => x.json())
            .subscribe(sr => {
                this.productTypes = sr;
            });
    }

    openAdd(content) {
        this.productType = new ProductType();
        this.modalMode = "Add";
        this.modalRef = this.modalService.open(content);
    };

    openEdit(content, productTypeId) {
        this.modalMode = "Edit";
        this.modalRef = this.modalService.open(content);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        //Get the product
        this.http.get('/api/producttype/' + productTypeId, { headers })
            .map(x => x.json())
            .subscribe(pt => {
                this.productType = pt;
            });
    };

    public closeModal() {
        this.modalRef.close();
    }

    saveEdit() {
        if (!this.productType.productTypeName || !this.productType.productCategoryId || !this.productType.productTypeCategoryId) {
            return;
        }
        else {
            // Save the new product name
            var body = "productTypeName=" + this.productType.productTypeName + "&productTypeCategoryId=" + this.productType.productTypeCategoryId + "&recordStatus=" + this.productType.recordStatus + "&productCategoryId=" + this.productType.productCategoryId + "&username=" + this.username;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Cache-Control', 'no-cache');
            headers.append('Pragma', 'no-cache');
            this.http.put('/api/producttype/' + this.productType.productTypeId, body, { headers }).subscribe(data => {
                console.log('post ok');
                this.loadProductTypes();
                this.closeModal();
            }, error => {
                console.log(JSON.stringify(error.json()));
            });

            this.closeModal();
        }
    }

    saveAdd() {
        if (!this.productType.productTypeName || !this.productType.productCategoryId || !this.productType.productTypeCategoryId) {
            return;
        }
        else {
            // Save the new product name
            var body = "productTypeName=" + this.productType.productTypeName + "&productTypeCategoryId=" + this.productType.productTypeCategoryId + "&productCategoryId=" + this.productType.productCategoryId + "&username=" + this.username;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            this.http.post('/api/producttype/', body, { headers }).subscribe(data => {
                console.log('post ok');
                this.loadProductTypes();
                this.closeModal();
            }, error => {
                console.log(JSON.stringify(error.json()));
            });

            this.closeModal();
        }
    }
}
