﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductTypeRoutingModule } from './product-type-routing.module';
import { ProductTypeComponent } from './product-type.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
      ProductTypeRoutingModule,
      NgbModule.forRoot(),
      FormsModule
  ],
  declarations: [ProductTypeComponent]
})
export class ProductTypeModule { }
