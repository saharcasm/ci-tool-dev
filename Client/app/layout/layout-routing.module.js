var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
var routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'search', loadChildren: './search/search.module#SearchModule' },
            { path: 'plan', loadChildren: './plan/plan.module#PlanModule' },
            { path: 'plan/:id/:categoryId', loadChildren: './plan/plan.module#PlanModule' },
            { path: 'reporting', loadChildren: './reporting/reporting.module#ReportingModule' },
            { path: 'maintain/product-name', loadChildren: './maintain/product-name/product-name.module#ProductNameModule' },
            { path: 'maintain/product-type', loadChildren: './maintain/product-type/product-type.module#ProductTypeModule' },
            { path: 'maintain/term', loadChildren: './maintain/term/term.module#TermModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' }
        ]
    }
];
var LayoutRoutingModule = /** @class */ (function () {
    function LayoutRoutingModule() {
    }
    LayoutRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule]
        })
    ], LayoutRoutingModule);
    return LayoutRoutingModule;
}());
export { LayoutRoutingModule };
