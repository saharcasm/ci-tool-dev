var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { MobilePlan } from "../models";
var Tab2Component = /** @class */ (function () {
    function Tab2Component() {
    }
    Tab2Component.prototype.ngOnInit = function () {
        // Default plan values where they are empty
        window.scrollTo(0, 0);
        this.plan.studentDiscount = this.plan.studentDiscountType ? 'Yes' : 'No';
        this.plan.studentDiscount = this.studentDiscount;
        this.setDefaults();
    };
    Tab2Component.prototype.setDefaults = function () {
        if (this.plan.studentDiscount == undefined)
            this.plan.studentDiscount = 'No';
    };
    Tab2Component.prototype.ngOnChanges = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (this.plan) {
            this.validateTab2();
        }
    };
    Tab2Component.prototype.validateFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
            if (fields[i] == undefined)
                return true; // disable the tab
        }
        return false;
    };
    Tab2Component.prototype.validateDependent = function (parentFields) {
        for (var i = 0; i < parentFields.length; i++) {
            if (this.validateFields(parentFields[i].dependentFields))
                return true;
        }
        return false;
    };
    Tab2Component.prototype.validateTab2 = function () {
        var parentFields = [];
        if (this.plan.studentDiscount == 'Yes') {
            var fields = [this.plan.studentDiscountType, this.plan.studentDiscountValue];
            parentFields.push({ parentField: this.plan.studentDiscountType, dependentFields: fields });
        }
        if (this.plan.bundleDiscount == 'All' || this.plan.bundleDiscount == 'Online') {
            var fields = [this.plan.bundleDiscountDetails];
            parentFields.push({ parentField: this.plan.bundleDiscount, dependentFields: fields });
        }
        if (this.plan.standardDomesticVoiceInclusions == 'Value Capped' || this.plan.standardDomesticVoiceInclusions == 'Minutes Capped') {
            var fields = [this.plan.standardDomesticVoiceValue];
            parentFields.push({ parentField: this.plan.standardDomesticVoiceInclusions, dependentFields: fields });
        }
        if (this.plan.standardDomesticTextInclusions == 'Number') {
            var fields = [this.plan.standardDomesticTextAmount];
            parentFields.push({ parentField: this.plan.standardDomesticTextInclusions, dependentFields: fields });
        }
        this.plan.disableTab3 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;
    };
    Tab2Component.prototype.preventKeyStrokes = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    };
    Tab2Component.prototype.preventEnter = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode == 13)
            return false;
        return true;
    };
    __decorate([
        Input(),
        __metadata("design:type", MobilePlan)
    ], Tab2Component.prototype, "plan", void 0);
    Tab2Component = __decorate([
        Component({
            selector: 'tab2',
            templateUrl: './tab2.component.html',
            styleUrls: ['./plan.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], Tab2Component);
    return Tab2Component;
}());
export { Tab2Component };
