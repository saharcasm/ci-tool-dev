var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { MobilePlan } from "../models";
var Tab3Component = /** @class */ (function () {
    function Tab3Component(http) {
        this.http = http;
    }
    Tab3Component.prototype.ngOnInit = function () {
        var _this = this;
        window.scrollTo(0, 0);
        // Get the FF Programs
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.get('/api/frequentflyer/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (ff) {
            _this.ffPrograms = ff;
        });
        // Default Radio states
        this.setDefaults();
    };
    Tab3Component.prototype.setDefaults = function () {
        this.plan.internationalTextIncludedCountries = (this.plan.internationalTextCountriesAmount == this.plan.internationalVoice1CountriesAmount &&
            this.plan.internationalTextCountriesList == this.plan.internationalVoice1CountriesList) ? 'Same' : 'Other';
        if (this.plan.internationalRoamIncludedInPlan == undefined)
            this.plan.internationalRoamIncludedInPlan = true;
    };
    Tab3Component.prototype.ngOnChanges = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (this.plan) {
            this.validateTab3();
        }
    };
    Tab3Component.prototype.validateFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
            if (fields[i] == undefined)
                return true; // disable the tab
        }
        return false;
    };
    Tab3Component.prototype.validateDependent = function (parentFields) {
        for (var i = 0; i < parentFields.length; i++) {
            console.log("this:: ", this.validateFields(parentFields[i].dependentFields));
            if (parentFields[i].dependentFields.length == 0)
                return false;
            if (this.validateFields(parentFields[i].dependentFields))
                return true;
        }
        return false;
    };
    Tab3Component.prototype.validateTab3 = function () {
        var parentFields = [];
        if (this.plan.internationalVoice1Inclusions == 'Minutes Capped' || this.plan.internationalVoice1Inclusions == 'Value Capped') {
            var fields = [this.plan.internationalVoice1Value, this.plan.internationalVoice1CountriesAmount, this.plan.internationalVoice1CountriesList];
            parentFields.push({ dependentFields: fields });
        }
        else if (this.plan.internationalVoice1Inclusions == 'Unlimited') {
            var fields = [this.plan.internationalVoice1IncludedInPlan, this.plan.internationalVoice1CountriesAmount, this.plan.internationalVoice1CountriesList];
            if (this.plan.internationalVoice1IncludedInPlan == false) {
                fields.push(this.plan.internationalVoice1AdditionalCost);
            }
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.internationalVoice2Inclusions == 'Minutes Capped' || this.plan.internationalVoice2Inclusions == 'Value Capped') {
            var fields = [this.plan.internationalVoice2Value];
            parentFields.push({ parentField: this.plan.internationalVoice2Inclusions, dependentFields: fields });
        }
        if (this.plan.internationalVoice2Inclusions == 'Unlimited') {
            var fields = [this.plan.internationalVoice2IncludedInPlan, 0];
            if (this.plan.internationalVoice2IncludedInPlan == false) {
                fields.push(this.plan.internationalVoice2AdditionalCost);
            }
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.internationalTextInclusions == 'Unlimited') {
            var fields = [this.plan.internationalTextIncludedCountries, this.plan.internationalTextDetails];
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.internationalTextInclusions == 'Number Capped' || this.plan.internationalTextInclusions == 'Included In Value') {
            var fields = [this.plan.internationalTextValue, this.plan.internationalTextIncludedCountries, this.plan.internationalTextDetails];
            if (this.plan.internationalTextIncludedCountries == 'Other') {
                fields.push(this.plan.internationalTextCountriesAmount);
                fields.push(this.plan.internationalTextCountriesList);
            }
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.internationalRoamIndicator == true) {
            var fields = [this.plan.internationalRoamIncludedInPlan, this.plan.internationalRoamDetails, this.plan.internationalRoamCountryAmount, this.plan.internationalRoamCountryList];
            if (this.plan.internationalRoamIncludedInPlan == false) {
                fields.push(this.plan.internationalRoamAdditionalCost);
            }
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.frequentFlyerIndicator == true) {
            var fields = [this.plan.frequentFlyerProgramId, this.plan.frequentFlyerProgramDetails];
            parentFields.push({ parentField: this.plan.frequentFlyerIndicator, dependentFields: fields });
        }
        this.plan.disableTab4 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;
    };
    Tab3Component.prototype.setSameAsIV = function () {
        if (this.plan && this.plan.internationalTextIncludedCountries == 'Same') {
            this.plan.internationalTextCountriesAmount = this.plan.internationalVoice1CountriesAmount;
            this.plan.internationalTextCountriesList = this.plan.internationalVoice1CountriesList;
        }
    };
    Tab3Component.prototype.preventKeyStrokes = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    };
    Tab3Component.prototype.intVoice1Clear = function () {
        this.plan.internationalVoice1IncludedInPlan = false;
        this.plan.internationalVoice1AdditionalCost = null;
        this.plan.internationalVoice1Value = null;
        this.plan.internationalVoice1CountriesAmount = null;
        this.plan.internationalVoice1CountriesList = null;
    };
    Tab3Component.prototype.intVoice2Clear = function () {
        this.plan.internationalVoice2IncludedInPlan = false;
        this.plan.internationalVoice2AdditionalCost = null;
        this.plan.internationalVoice2Value = null;
        this.plan.internationalVoice2CountriesAmount = null;
        this.plan.internationalVoice2CountriesList = null;
    };
    Tab3Component.prototype.intTextClear = function () {
        this.plan.internationalTextCountriesAmount = null;
        this.plan.internationalTextCountriesList = null;
        this.plan.internationalTextDetails = null;
        this.plan.internationalTextValue = null;
    };
    Tab3Component.prototype.intRoamClear = function () {
        this.plan.internationalRoamAdditionalCost = null;
        this.plan.internationalRoamCountryAmount = null;
        this.plan.internationalRoamCountryList = null;
        this.plan.internationalRoamDetails = null;
        this.plan.internationalRoamIncludedInPlan = null;
    };
    __decorate([
        Input(),
        __metadata("design:type", MobilePlan)
    ], Tab3Component.prototype, "plan", void 0);
    Tab3Component = __decorate([
        Component({
            selector: 'tab3',
            templateUrl: './tab3.component.html',
            styleUrls: ['./plan.component.scss']
        }),
        __metadata("design:paramtypes", [Http])
    ], Tab3Component);
    return Tab3Component;
}());
export { Tab3Component };
