var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { MobilePlan } from '../models';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
var PlanComponent = /** @class */ (function () {
    function PlanComponent(http, route, modalService) {
        this.http = http;
        this.route = route;
        this.modalService = modalService;
    }
    PlanComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.plan = new MobilePlan();
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = +params['id']; // (+) converts string 'id' to a number
            _this.categoryid = +params['categoryId'];
            if (isNaN(_this.id) || isNaN(_this.categoryid)) {
                _this.editMode = 'New';
                _this.plan.productChangeType = 'New';
                _this.plan.username = 'Administrator'; // Only needed in debug mode as the azure login doesn't pass through
                _this.plan.planType = 'Standard';
                _this.setDefaultRadioStates();
            }
            else {
                _this.editMode = 'Edit';
                _this.loadPlan(_this.id, _this.categoryid, 'Edit');
            }
        });
        // Get the username when hosted, if empty put in Admin as default
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.get('/.auth/me', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (ud) {
            _this.userdata = ud;
            for (var prop in _this.userdata[0]) {
                if (prop == 'user_id') {
                    _this.username = _this.userdata[0][prop];
                }
                else {
                    _this.username = 'Administrator';
                }
                console.log('Username: ' + _this.username);
            }
        }, function (err) {
            console.log(err);
            _this.username = 'Administrator';
        });
    };
    PlanComponent.prototype.loadPlan = function (planId, categoryId, editMode) {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        // Get the brands
        this.http.get('/api/mobile/' + planId + '?categoryId=' + categoryId, { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (p) {
            _this.plan = p;
            _this.plan.productChangeType = 'Update';
            console.log('Plan Loaded: ' + _this.plan.id);
            _this.setDefaultRadioStates();
        });
    };
    PlanComponent.prototype.openConfirm = function (content, formIncomplete) {
        if (this.checkFormValid()) {
            this.modalRef = this.modalService.open(content, { backdrop: 'static' });
        }
        else {
            this.modalRef = this.modalService.open(formIncomplete, { backdrop: 'static' });
        }
    };
    PlanComponent.prototype.closeModal = function () {
        this.modalRef.close();
    };
    PlanComponent.prototype.saveForm = function (postSubmission, formError) {
        var _this = this;
        this.closeModal();
        this.plan.username = this.username;
        this.plan.productStatus = this.plan.productChangeType == 'Deactivate' ? false : true;
        var body = JSON.stringify(this.plan);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var apiString = this.plan.productCategoryId == 1 ? '/api/mobile/postpaid/' : '/api/mobile/prepaid/';
        if (this.plan.productChangeType == 'New') {
            this.http.post(apiString, body, { headers: headers }).subscribe(function (data) {
                console.log('post ok');
                _this.modalRef = _this.modalService.open(postSubmission);
            }, function (error) {
                console.log(error);
                _this.modalRef = _this.modalService.open(formError);
            });
        }
        else {
            this.http.put(apiString, body, { headers: headers }).subscribe(function (data) {
                console.log('put ok');
                _this.modalRef = _this.modalService.open(postSubmission, { backdrop: 'static' });
            }, function (error) {
                console.log(error);
                _this.modalRef = _this.modalService.open(formError);
            });
        }
    };
    PlanComponent.prototype.checkFormValid = function () {
        if (this.plan.segment == undefined ||
            this.plan.brandId == undefined ||
            this.plan.productTypeId == undefined ||
            this.plan.termId == undefined ||
            this.plan.productNameId == undefined ||
            this.plan.productChangeType == undefined ||
            this.plan.planType == undefined ||
            this.plan.bundleDiscount == undefined ||
            this.plan.standardData == undefined ||
            this.plan.standardDomesticVoiceInclusions == undefined ||
            this.plan.standardDomesticTextInclusions == undefined ||
            this.plan.internationalVoice1Inclusions == undefined ||
            this.plan.internationalVoice2Inclusions == undefined ||
            this.plan.internationalTextInclusions == undefined ||
            this.plan.internationalRoamIndicator == undefined ||
            this.plan.frequentFlyerIndicator == undefined ||
            this.plan.standardContentIndicator == undefined ||
            this.plan.otherInclusionsIndicator == undefined ||
            this.plan.promotionalFeeIndicator == undefined ||
            this.plan.promotionalStudentDiscountIndicator == undefined ||
            this.plan.promotionalFeeFreeMonthsIndicator == undefined ||
            this.plan.promotionalBonusData1Indicator == undefined ||
            this.plan.promotionalBonusData2Indicator == undefined ||
            this.plan.promotionalDomesticVoiceIndicator == undefined ||
            this.plan.promotionalDomesticTextIndicator == undefined ||
            this.plan.promotionalInternationalVoiceIndicator == undefined ||
            this.plan.promotionalInternationalTextIndicator == undefined ||
            this.plan.promotionalInternationalRoamIndicator == undefined ||
            this.plan.promotionalFrequentFlyerIndicator == undefined ||
            this.plan.promotionalContentIndicator == undefined ||
            this.plan.promotionalContentIndicator == undefined ||
            this.plan.promotionalOtherIndicator == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    // TODO: REFACTOR
    PlanComponent.prototype.validatePromotional = function () {
        if (this.plan.planType === 'Promotional') {
            if (!this.plan.promotionalPlanFee)
                return true;
            if (!this.plan.promotionalOnlyOfferStartDate)
                return true;
            if (!this.plan.promotionalOnlyOfferEndDate)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateStudentDiscount = function () {
        if (this.plan.studentDiscount == 'Yes') {
            if (!this.plan.studentDiscountValue)
                return true;
            if (!this.plan.studentDiscountType)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateBundleDiscount = function () {
        if (this.plan.bundleDiscount !== 'None') {
            if (!this.plan.bundleDiscountDetails)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateCappedValues = function () {
        if (this.plan.standardDomesticVoiceInclusions == 'Value Capped'
            || this.plan.standardDomesticVoiceInclusions == 'Minutes Capped') {
            if (!this.plan.standardDomesticVoiceValue)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateDomesticText = function () {
        if (this.plan.standardDomesticTextInclusions == 'Number') {
            if (!this.plan.standardDomesticTextAmount)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateUnlimitedPlan1 = function () {
        if (this.plan.internationalVoice1Inclusions == 'Unlimited') {
            if (this.plan.internationalVoice1IncludedInPlan == undefined)
                return true;
            if (!this.plan.internationalVoice1CountriesAmount)
                return true;
            if (!this.plan.internationalVoice1CountriesList)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateUnlimitedPlan2 = function () {
        if (this.plan.internationalVoice2Inclusions == 'Unlimited') {
            if (this.plan.internationalVoice2IncludedInPlan == undefined)
                return true;
            if (!this.plan.internationalVoice2CountriesAmount)
                return true;
            if (!this.plan.internationalVoice2CountriesList)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validatePlanFree1 = function () {
        if (this.plan.internationalVoice1IncludedInPlan == false) {
            if (!this.plan.internationalVoice1AdditionalCost)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validatePlanFree2 = function () {
        if (this.plan.internationalVoice2IncludedInPlan == false) {
            if (!this.plan.internationalVoice2AdditionalCost)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateInternationalVoice1 = function () {
        if (this.plan.internationalVoice1Inclusions == 'Value Capped' ||
            this.plan.internationalVoice1Inclusions == 'Minutes Capped') {
            if (!this.plan.internationalVoice1Value)
                return true;
            if (!this.plan.internationalVoice1CountriesAmount)
                return true;
            if (!this.plan.internationalVoice1CountriesList)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateInternationalText = function () {
        if (this.plan.internationalTextInclusions == 'Unlimited') {
            if (!this.plan.internationalTextIncludedCountries)
                return true;
            if (!this.plan.internationalTextDetails)
                return true;
        }
        else if (this.plan.internationalTextInclusions == 'Included In Value' ||
            this.plan.internationalTextInclusions == 'Number Capped') {
            if (!this.plan.internationalTextIncludedCountries)
                return true;
            if (!this.plan.internationalTextDetails)
                return true;
            if (!this.plan.internationalTextValue)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateInternationalVoice2 = function () {
        if (this.plan.internationalVoice2Inclusions == 'Value Capped' ||
            this.plan.internationalVoice2Inclusions == 'Minutes Capped') {
            if (!this.plan.internationalVoice2Value)
                return true;
            if (!this.plan.internationalVoice2CountriesAmount)
                return true;
            if (!this.plan.internationalVoice2CountriesList)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateOtherCountries = function () {
        if (this.plan.internationalTextIncludedCountries == 'Other') {
            if (!this.plan.internationalTextCountriesAmount)
                return true;
            if (!this.plan.internationalTextCountriesList)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateRoaming = function () {
        if (this.plan.internationalRoamIndicator == true) {
            if (!this.plan.internationalRoamIncludedInPlan)
                return true;
            if (!this.plan.internationalRoamDetails)
                return true;
            if (!this.plan.internationalRoamCountryAmount)
                return true;
            if (!this.plan.internationalRoamCountryList)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validatePlanFreeRoaming = function () {
        if (this.plan.internationalRoamIncludedInPlan == false) {
            if (!this.plan.internationalRoamAdditionalCost)
                return true;
        }
        return false;
    };
    PlanComponent.prototype.validateStep1 = function () {
        if (this.validatePromotional())
            return true;
        return false;
    };
    PlanComponent.prototype.validateStep2 = function () {
        if (this.validateStudentDiscount())
            return true;
        if (this.validateBundleDiscount())
            return true;
        if (this.validateCappedValues())
            return true;
        if (this.validateDomesticText())
            return true;
        return false;
    };
    PlanComponent.prototype.validateStep3 = function () {
        if (this.validateInternationalVoice1())
            return true;
        if (this.validatePlanFree1())
            return true;
        if (this.validateInternationalVoice2())
            return true;
        if (this.validatePlanFree2())
            return true;
        if (this.validateUnlimitedPlan1())
            return true;
        if (this.validateUnlimitedPlan2())
            return true;
        if (this.validateInternationalText())
            return true;
        if (this.validateOtherCountries())
            return true;
        if (this.validateRoaming())
            return true;
        if (this.validatePlanFreeRoaming())
            return true;
        return false;
    };
    // Set the default states in the model that drive radio buttons (requirement that they are pre-selected)
    PlanComponent.prototype.setDefaultRadioStates = function () {
        this.plan.standardDomesticVoiceInclusions = this.plan.standardDomesticVoiceInclusions ? this.plan.standardDomesticVoiceInclusions : 'None';
        this.plan.standardDomesticTextInclusions = this.plan.standardDomesticTextInclusions ? this.plan.standardDomesticTextInclusions : 'None';
        this.plan.bundleDiscount = this.plan.bundleDiscount ? this.plan.bundleDiscount : 'None';
        this.plan.standardDomesticCustomerToCustomerIndicator = this.plan.standardDomesticCustomerToCustomerIndicator ? this.plan.standardDomesticCustomerToCustomerIndicator : false;
        this.plan.standardDomesticPrepaidExtraCreditIndicator = this.plan.standardDomesticPrepaidExtraCreditIndicator ? this.plan.standardDomesticPrepaidExtraCreditIndicator : false;
        this.plan.internationalVoice1Inclusions = this.plan.internationalVoice1Inclusions ? this.plan.internationalVoice1Inclusions : 'None';
        this.plan.internationalVoice2Inclusions = this.plan.internationalVoice2Inclusions ? this.plan.internationalVoice2Inclusions : 'None';
        this.plan.internationalTextInclusions = this.plan.internationalTextInclusions ? this.plan.internationalTextInclusions : 'None';
        this.plan.internationalRoamIndicator = this.plan.internationalRoamIndicator ? this.plan.internationalRoamIndicator : false;
        this.plan.frequentFlyerIndicator = this.plan.frequentFlyerIndicator ? this.plan.frequentFlyerIndicator : false;
        this.plan.standardContentIndicator = this.plan.standardContentIndicator ? this.plan.standardContentIndicator : false;
        this.plan.otherInclusionsIndicator = this.plan.otherInclusionsIndicator ? this.plan.otherInclusionsIndicator : false;
        this.plan.promotionalFeeIndicator = this.plan.promotionalFeeIndicator ? this.plan.promotionalFeeIndicator : false;
        this.plan.promotionalStudentDiscountIndicator = this.plan.promotionalStudentDiscountIndicator ? this.plan.promotionalStudentDiscountIndicator : false;
        this.plan.promotionalFeeFreeMonthsIndicator = this.plan.promotionalFeeFreeMonthsIndicator ? this.plan.promotionalFeeFreeMonthsIndicator : false;
        this.plan.promotionalBonusData1Indicator = this.plan.promotionalBonusData1Indicator ? this.plan.promotionalBonusData1Indicator : false;
        this.plan.promotionalBonusData2Indicator = this.plan.promotionalBonusData2Indicator ? this.plan.promotionalBonusData2Indicator : false;
        this.plan.promotionalDomesticVoiceIndicator = this.plan.promotionalDomesticVoiceIndicator ? this.plan.promotionalDomesticVoiceIndicator : false;
        this.plan.promotionalDomesticTextIndicator = this.plan.promotionalDomesticTextIndicator ? this.plan.promotionalDomesticTextIndicator : false;
        this.plan.promotionalInternationalVoiceIndicator = this.plan.promotionalInternationalVoiceIndicator ? this.plan.promotionalInternationalVoiceIndicator : false;
        this.plan.promotionalInternationalTextIndicator = this.plan.promotionalInternationalTextIndicator ? this.plan.promotionalInternationalTextIndicator : false;
        this.plan.promotionalInternationalRoamIndicator = this.plan.promotionalInternationalRoamIndicator ? this.plan.promotionalInternationalRoamIndicator : false;
        this.plan.promotionalFrequentFlyerIndicator = this.plan.promotionalFrequentFlyerIndicator ? this.plan.promotionalFrequentFlyerIndicator : false;
        this.plan.promotionalContentIndicator = this.plan.promotionalContentIndicator ? this.plan.promotionalContentIndicator : false;
        this.plan.promotionalOtherIndicator = this.plan.promotionalOtherIndicator ? this.plan.promotionalOtherIndicator : false;
        this.plan.promotionalPrepaidBonusCreditIndicator = this.plan.promotionalPrepaidBonusCreditIndicator ? this.plan.promotionalPrepaidBonusCreditIndicator : false;
    };
    PlanComponent = __decorate([
        Component({
            selector: 'app-plan',
            templateUrl: './plan.component.html',
            styleUrls: ['./plan.component.scss']
        }),
        __metadata("design:paramtypes", [Http, ActivatedRoute, NgbModal])
    ], PlanComponent);
    return PlanComponent;
}());
export { PlanComponent };
