﻿import { Component, OnInit, Input } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MobilePlan } from "../models";


@Component({
    selector: 'tab5',
    templateUrl: './tab5.component.html',
    styleUrls: ['./plan.component.scss']
})

export class Tab5Component implements OnInit {
    @Input() plan: MobilePlan;

    otherStandardInclusions: string;
    networkTrialInclusions: string;
    networkGuarantee: string;
    dataShare: string;
    dataRollover: string;
    deviceTradeUp: string;
    wifi: string;
    cloudStorage: string;
    referFriend: string;
    otherInclusion: string;
    businessApps: string;
    otherPrepaidAuto: string;

    constructor() { }
    ngOnInit() {
      window.scrollTo(0, 0);
        //Set the default state on the option radio buttons
        this.networkTrialInclusions = this.plan.otherInclusionsNetworkTrial  ? 'Yes' : 'No';
      this.networkGuarantee = this.plan.otherInclusionsNetworkGuarantee  ? 'Yes' : 'No';
        this.dataShare = this.plan.otherInclusionsDataShare  ? 'Yes' : 'No';
        this.dataRollover = this.plan.otherInclusionsDataRollover  ? 'Yes' : 'No';
        this.deviceTradeUp = this.plan.otherInclusionsDeviceTradeUp  ? 'Yes' : 'No';
        this.wifi = this.plan.otherInclusionsWifi  ? 'Yes' : 'No';
        this.cloudStorage = this.plan.otherInclusionsCloudStorage  ? 'Yes' : 'No';
        this.referFriend = this.plan.otherInclusionsReferFriend  ? 'Yes' : 'No';
        this.businessApps = this.plan.otherInclusionsBusinessApps  ? 'Yes' : 'No';
        this.otherInclusion = this.plan.otherInclusionsOther ? 'Yes' : 'No';
        this.otherPrepaidAuto = this.plan.otherInclusionsPrepaidAutopay ? 'Yes' : 'No';

    }
  ngOnChanges(...args: any[]) {
    if (this.plan) {

      this.validateTab5();
    }
  }

  validateFields(fields): boolean {
    for (let i = 0; i < fields.length; i++) {
      if (fields[i] == undefined) return true; // disable the tab
    }
    return false;
  }

  validateDependent (parentFields){
    for (let i = 0; i < parentFields.length; i++) {
      if (this.validateFields(parentFields[i].dependentFields)) return true;
    }
    return false;
  }
  preventEnter(event) {
    let charCode = (event.which) ? event.which : event.keyCode;

    if (charCode == 13)
      return false;
    return true;
  }

  validateTab5() {
    let parentFields = [];
    // if (this.plan.otherInclusionsIndicator == true){
    //   let fields = [this.networkTrialInclusions, this.networkGuarantee, this.dataShare,
    //     this.dataRollover, this.deviceTradeUp, this.wifi, this.cloudStorage, this.referFriend];
    //   parentFields.push({dependentFields: fields});
    // }
    if (this.networkTrialInclusions == 'Yes'){
      let fields = [this.plan.otherInclusionsNetworkTrial];
      parentFields.push({dependentFields: fields});
    }
    if (this.networkGuarantee == 'Yes'){
      let fields = [this.plan.otherInclusionsNetworkGuarantee];
      parentFields.push({dependentFields: fields});
    }
    if (this.dataShare == 'Yes'){
      let fields = [this.plan.otherInclusionsDataShare];
      parentFields.push({dependentFields: fields});
    }
    if (this.dataRollover == 'Yes'){
      let fields = [this.plan.otherInclusionsDataRollover];
      parentFields.push({dependentFields: fields});
    }
    if (this.deviceTradeUp == 'Yes'){
      let fields = [this.plan.otherInclusionsDeviceTradeUp];
      parentFields.push({dependentFields: fields});
    }
    if (this.wifi == 'Yes'){
      let fields = [this.plan.otherInclusionsWifi];
      parentFields.push({dependentFields: fields});
    }
    if (this.cloudStorage == 'Yes'){
      console.log("this.plan.otherInclusionsCloudStorage:: ", this.plan.otherInclusionsCloudStorage);

      let fields = [this.plan.otherInclusionsCloudStorage];
      parentFields.push({dependentFields: fields});
    }
    if (this.referFriend == 'Yes'){
      console.log('this.referFriend:: ',this.referFriend)

      let fields = [this.plan.otherInclusionsReferFriend];
      parentFields.push({dependentFields: fields});
    }
    if (this.otherInclusion == 'Yes'){
      let fields = [this.plan.otherInclusionsOther];
      parentFields.push({dependentFields: fields});
    }
    if (this.otherPrepaidAuto == 'Yes'){
      let fields = [this.plan.otherInclusionsPrepaidAutopay];
      parentFields.push({dependentFields: fields});
    }

    this.plan.disableTab6 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;
  }
}
