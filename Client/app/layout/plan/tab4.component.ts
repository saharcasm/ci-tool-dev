﻿import { Component, OnInit, Input } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MobilePlan } from "../models";

@Component({
    selector: 'tab4',
    templateUrl: './tab4.component.html',
    styleUrls: ['./plan.component.scss']
})

export class Tab4Component implements OnInit {
    @Input() plan: MobilePlan;

    contentIncluded: string;
    sportsIncluded: string;
    sportsFreeStreaming: string;
    sportsIncludedInFee: string;
    sportExtraCost: number;
    sportsStandardInclusions: string;
    sportsDetails: string;
    musicIncluded: string;
    musicFreeStreaming: string;
    musicIncludedInFee: string;
    musicExtraCost: number;
    musicStandardInclusions: string;
    musicDetails: string;
    svodIncluded: string;
    svodFreeStreaming: string;
    svodIncludedInFee: string;
    svodExtraCost: number;
    svodStandardInclusions: string;
    svodDetails: string;
    other1Included: string;
    other1Streaming: string;
    other1IncludedInFee: string;
    other1ExtraCost: number;
    other1tandardInclusions: string;
    other1Details: string;
    other2Included: string;
    other2Streaming: string;
    otherIncludedInFee: string;
    other2ExtraCost: number;
    other2tandardInclusions: string;
    otherDetails: string;

    constructor() { }
    ngOnInit() {
        window.scrollTo(0, 0);
        this.setDefaults();

    }
    ngOnChanges(...args: any[]) {
        if (this.plan) {
            this.validateTab4();
        }
    }
    setDefaults(){
        console.log("this.plan.standardContentSvodDataFree:: ", this.plan.standardContentSvodDataFree)
        if(this.plan.standardContentSportIndicator==undefined)
            this.plan.standardContentSportIndicator=false;
        if(this.plan.standardContentMusicIndicator==undefined)
            this.plan.standardContentMusicIndicator=false;
        if(this.plan.standardContentSvodIndicator==undefined)
            this.plan.standardContentSvodIndicator=false;
        if(this.plan.standardContentOther1Indicator==undefined)
            this.plan.standardContentOther1Indicator=false;
        if(this.plan.standardContentOther2Indicator==undefined)
            this.plan.standardContentOther2Indicator=false;
        //////////////////////////////////////////////////////////////////////////
        if(this.plan.standardContentSportDataFree==undefined)
            this.plan.standardContentSportDataFree=false;
        if(this.plan.standardContentSportIncludedInPlan==undefined)
            this.plan.standardContentSportIncludedInPlan=false;
        //////////////////////////////////////////////////////////////////////////
        if(this.plan.standardContentSvodDataFree==undefined)
            this.plan.standardContentSvodDataFree=false;
        if(this.plan.standardContentSvodIncludedInPlan==undefined)
            this.plan.standardContentSvodIncludedInPlan=false;
        //////////////////////////////////////////////////////////////////////////
        if(this.plan.standardContentMusicDataFree==undefined)
            this.plan.standardContentMusicDataFree=false;
        if(this.plan.standardContentMusicIncludedInPlan==undefined)
            this.plan.standardContentMusicIncludedInPlan=false;
        //////////////////////////////////////////////////////////////////////////
        if(this.plan.standardContentOther1DataFree==undefined)
            this.plan.standardContentOther1DataFree=false;
        if(this.plan.standardContentOther1IncludedInPlan==undefined)
            this.plan.standardContentOther1IncludedInPlan=false;
        //////////////////////////////////////////////////////////////////////////
        if(this.plan.standardContentOther2DataFree==undefined)
            this.plan.standardContentOther2DataFree=false;
        if(this.plan.standardContentOther2IncludedInPlan==undefined)
            this.plan.standardContentOther2IncludedInPlan=false;
        //////////////////////////////////////////////////////////////////////////
        if(this.plan.standardContentOther3Indicator==undefined)
            this.plan.standardContentOther3Indicator=false;
        if(this.plan.standardContentOther3DataFree==undefined)
            this.plan.standardContentOther3DataFree=false;
        if(this.plan.standardContentOther3IncludedInPlan==undefined)
            this.plan.standardContentOther3IncludedInPlan=false;
    }

    preventKeyStrokes(event) {
        let charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    validateFields(fields): boolean {
        for (let i = 0; i < fields.length; i++) {
            if (fields[i] == undefined) return true; // disable the tab
        }
        return false;
    }

    validateDependent (parentFields){
        for (let i = 0; i < parentFields.length; i++) {
            if (this.validateFields(parentFields[i].dependentFields)) return true;
        }
        return false;
    }

    validateTab4() {
        let parentFields = [];
        if (this.plan.standardContentIndicator == true){
            let fields = [this.plan.standardContentSportIndicator, this.plan.standardContentMusicIndicator, this.plan.standardContentSvodIndicator,
            this.plan.standardContentOther1Indicator, this.plan.standardContentOther2Indicator];
            parentFields.push({dependentFields: fields});
        }
        if (this.plan.standardContentSportIndicator == true){
            let fields = [this.plan.standardContentSportDataFree, this.plan.standardContentSportIncludedInPlan, this.plan.standardContentSportInclusions, 0];
            if (this.plan.standardContentSportIncludedInPlan == false){
                fields.push(this.plan.standardContentSportAdditionalCost);
            }
            parentFields.push({dependentFields: fields});
        }
        if (this.plan.standardContentMusicIndicator == true){
            let fields = [this.plan.standardContentMusicDataFree, this.plan.standardContentMusicIncludedInPlan, this.plan.standardContentMusicInclusions, 0];
            if(this.plan.standardContentMusicIncludedInPlan == false){
                fields.push(this.plan.standardContentMusicAdditionalCost);
            }
            parentFields.push({dependentFields: fields});
        }
        if (this.plan.standardContentSvodIndicator == true){
            let fields = [this.plan.standardContentSvodDataFree, this.plan.standardContentSvodIncludedInPlan, this.plan.standardContentSvodInclusions, 0];
            if(this.plan.standardContentSvodIncludedInPlan == false){
                fields.push(this.plan.standardContentSvodAdditionalCost);
            }
            parentFields.push({dependentFields: fields});
        }
        if (this.plan.standardContentOther1Indicator == true){
            let fields = [this.plan.standardContentOther1DataFree, this.plan.standardContentOther1IncludedInPlan, this.plan.standardContentOther1Inclusions, 0];
            if(this.plan.standardContentOther1IncludedInPlan == false){
                fields.push(this.plan.standardContentOther1AdditionalCost);
            }
            parentFields.push({dependentFields: fields});
        }
        if (this.plan.standardContentOther2Indicator == true){
            let fields = [this.plan.standardContentOther2DataFree, this.plan.standardContentOther2IncludedInPlan, this.plan.standardContentOther2Inclusions, 0];
            if(this.plan.standardContentOther2IncludedInPlan == false){
                fields.push(this.plan.standardContentOther2AdditionalCost);
            }
            parentFields.push({dependentFields: fields});
        }
        if (this.plan.standardContentOther3Indicator == true){
            let fields = [this.plan.standardContentOther3DataFree, this.plan.standardContentOther3IncludedInPlan, this.plan.standardContentOther3Inclusions, 0];
            if(this.plan.standardContentOther3IncludedInPlan == false){
                fields.push(this.plan.standardContentOther3AdditionalCost);
            }
            parentFields.push({dependentFields: fields});
        }

        this.plan.disableTab5 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;


    }
    clearAllContent()
    {
        console.log("clear all content");
        this.plan.standardContentSportIndicator = false;
        this.plan.standardContentSportDataFree = null;
        this.plan.standardContentSportIncludedInPlan = null;
        this.plan.standardContentSportAdditionalCost = null;
        this.plan.standardContentSportInclusions = null;
        this.plan.standardContentSportDetails = null;
        this.plan.standardContentMusicIndicator = false;
        this.plan.standardContentMusicDataFree = null;
        this.plan.standardContentMusicIncludedInPlan = null;
        this.plan.standardContentMusicAdditionalCost = null;
        this.plan.standardContentMusicInclusions = null;
        this.plan.standardContentMusicDetails = null;
        this.plan.standardContentSvodIndicator = false;
        this.plan.standardContentSvodDataFree = null;
        this.plan.standardContentSvodIncludedInPlan = null;
        this.plan.standardContentSvodAdditionalCost = null;
        this.plan.standardContentSvodInclusions = null;
        this.plan.standardContentSvodDetails = null;
        this.plan.standardContentOther1Indicator = false;
        this.plan.standardContentOther1DataFree = null;
        this.plan.standardContentOther1IncludedInPlan = null;
        this.plan.standardContentOther1AdditionalCost = null;
        this.plan.standardContentOther1Inclusions = null;
        this.plan.standardContentOther1Details = null;
        this.plan.standardContentOther2Indicator = false;
        this.plan.standardContentOther2DataFree = null;
        this.plan.standardContentOther2IncludedInPlan = null;
        this.plan.standardContentOther2AdditionalCost = null;
        this.plan.standardContentOther2Inclusions = null;
        this.plan.standardContentOther2Details = null;
        this.plan.standardContentOther3Indicator = false;
        this.plan.standardContentOther3DataFree = null;
        this.plan.standardContentOther3IncludedInPlan = null;
        this.plan.standardContentOther3AdditionalCost = null;
        this.plan.standardContentOther3Inclusions = null;
        this.plan.standardContentOther3Details=null;
    }

    clearSportContent() {
        this.plan.standardContentSportDataFree = null;
        this.plan.standardContentSportIncludedInPlan = null;
        this.plan.standardContentSportAdditionalCost = null;
        this.plan.standardContentSportInclusions = null;
        this.plan.standardContentSportDetails = null;
    }

    clearMusic() {
        this.plan.standardContentMusicDataFree = null;
        this.plan.standardContentMusicIncludedInPlan = null;
        this.plan.standardContentMusicAdditionalCost = null;
        this.plan.standardContentMusicInclusions = null;
        this.plan.standardContentMusicDetails = null;
    }

    clearSVOD() {
        this.plan.standardContentSvodDataFree = null;
        this.plan.standardContentSvodIncludedInPlan = null;
        this.plan.standardContentSvodAdditionalCost = null;
        this.plan.standardContentSvodInclusions = null;
        this.plan.standardContentSvodDetails = null;
    }

    clearOther1() {
        this.plan.standardContentOther1Indicator = false;
        this.plan.standardContentOther1DataFree = null;
        this.plan.standardContentOther1IncludedInPlan = null;
        this.plan.standardContentOther1AdditionalCost = null;
        this.plan.standardContentOther1Inclusions = null;
        this.plan.standardContentOther1Details = null;
    }

    clearOther2() {
        this.plan.standardContentOther2DataFree = false;
        this.plan.standardContentOther2IncludedInPlan = null;
        this.plan.standardContentOther2AdditionalCost = null;
        this.plan.standardContentOther2Inclusions = null;
        this.plan.standardContentOther2Details = null;
    }

    clearOther3()
    {
        this.plan.standardContentOther3DataFree = false;
        this.plan.standardContentOther3IncludedInPlan = null;
        this.plan.standardContentOther3AdditionalCost = null;
        this.plan.standardContentOther3Inclusions = null;
        this.plan.standardContentOther3Details = null;
    }
}
