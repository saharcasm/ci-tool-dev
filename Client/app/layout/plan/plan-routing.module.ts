﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PlanComponent } from './plan.component';

const routes: Routes = [
    { path: '', component: PlanComponent, pathMatch: 'full'},
    { path: ':id/:categoryId', component: PlanComponent, pathMatch: 'full'}

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PlanRoutingModule { }
