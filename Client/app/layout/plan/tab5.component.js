var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { MobilePlan } from "../models";
var Tab5Component = /** @class */ (function () {
    function Tab5Component() {
    }
    Tab5Component.prototype.ngOnInit = function () {
        window.scrollTo(0, 0);
        //Set the default state on the option radio buttons
        this.networkTrialInclusions = this.plan.otherInclusionsNetworkTrial ? 'Yes' : 'No';
        this.networkGuarantee = this.plan.otherInclusionsNetworkGuarantee ? 'Yes' : 'No';
        this.dataShare = this.plan.otherInclusionsDataShare ? 'Yes' : 'No';
        this.dataRollover = this.plan.otherInclusionsDataRollover ? 'Yes' : 'No';
        this.deviceTradeUp = this.plan.otherInclusionsDeviceTradeUp ? 'Yes' : 'No';
        this.wifi = this.plan.otherInclusionsWifi ? 'Yes' : 'No';
        this.cloudStorage = this.plan.otherInclusionsCloudStorage ? 'Yes' : 'No';
        this.referFriend = this.plan.otherInclusionsReferFriend ? 'Yes' : 'No';
        this.businessApps = this.plan.otherInclusionsBusinessApps ? 'Yes' : 'No';
        this.otherInclusion = this.plan.otherInclusionsOther ? 'Yes' : 'No';
        this.otherPrepaidAuto = this.plan.otherInclusionsPrepaidAutopay ? 'Yes' : 'No';
    };
    Tab5Component.prototype.ngOnChanges = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (this.plan) {
            this.validateTab5();
        }
    };
    Tab5Component.prototype.validateFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
            if (fields[i] == undefined)
                return true; // disable the tab
        }
        return false;
    };
    Tab5Component.prototype.validateDependent = function (parentFields) {
        for (var i = 0; i < parentFields.length; i++) {
            if (this.validateFields(parentFields[i].dependentFields))
                return true;
        }
        return false;
    };
    Tab5Component.prototype.preventEnter = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode == 13)
            return false;
        return true;
    };
    Tab5Component.prototype.validateTab5 = function () {
        var parentFields = [];
        // if (this.plan.otherInclusionsIndicator == true){
        //   let fields = [this.networkTrialInclusions, this.networkGuarantee, this.dataShare,
        //     this.dataRollover, this.deviceTradeUp, this.wifi, this.cloudStorage, this.referFriend];
        //   parentFields.push({dependentFields: fields});
        // }
        if (this.networkTrialInclusions == 'Yes') {
            var fields = [this.plan.otherInclusionsNetworkTrial];
            parentFields.push({ dependentFields: fields });
        }
        if (this.networkGuarantee == 'Yes') {
            var fields = [this.plan.otherInclusionsNetworkGuarantee];
            parentFields.push({ dependentFields: fields });
        }
        if (this.dataShare == 'Yes') {
            var fields = [this.plan.otherInclusionsDataShare];
            parentFields.push({ dependentFields: fields });
        }
        if (this.dataRollover == 'Yes') {
            var fields = [this.plan.otherInclusionsDataRollover];
            parentFields.push({ dependentFields: fields });
        }
        if (this.deviceTradeUp == 'Yes') {
            var fields = [this.plan.otherInclusionsDeviceTradeUp];
            parentFields.push({ dependentFields: fields });
        }
        if (this.wifi == 'Yes') {
            var fields = [this.plan.otherInclusionsWifi];
            parentFields.push({ dependentFields: fields });
        }
        if (this.cloudStorage == 'Yes') {
            console.log("this.plan.otherInclusionsCloudStorage:: ", this.plan.otherInclusionsCloudStorage);
            var fields = [this.plan.otherInclusionsCloudStorage];
            parentFields.push({ dependentFields: fields });
        }
        if (this.referFriend == 'Yes') {
            console.log('this.referFriend:: ', this.referFriend);
            var fields = [this.plan.otherInclusionsReferFriend];
            parentFields.push({ dependentFields: fields });
        }
        if (this.otherInclusion == 'Yes') {
            var fields = [this.plan.otherInclusionsOther];
            parentFields.push({ dependentFields: fields });
        }
        if (this.otherPrepaidAuto == 'Yes') {
            var fields = [this.plan.otherInclusionsPrepaidAutopay];
            parentFields.push({ dependentFields: fields });
        }
        this.plan.disableTab6 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;
    };
    __decorate([
        Input(),
        __metadata("design:type", MobilePlan)
    ], Tab5Component.prototype, "plan", void 0);
    Tab5Component = __decorate([
        Component({
            selector: 'tab5',
            templateUrl: './tab5.component.html',
            styleUrls: ['./plan.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], Tab5Component);
    return Tab5Component;
}());
export { Tab5Component };
