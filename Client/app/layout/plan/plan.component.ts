﻿import { Component, OnInit } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Brand, ProductType, Term, ProductName, MobilePlan } from '../models';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {
  public plan: MobilePlan;
  editMode: string;
  id: number;
  categoryid: number;
  private sub: any;
  private modalRef: any;
  public username: string;
  public userdata: any[];

  constructor(private http: Http, private route: ActivatedRoute, private modalService: NgbModal) { }

  ngOnInit() {
    this.plan = new MobilePlan();
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      this.categoryid = +params['categoryId'];
      if (isNaN(this.id) || isNaN(this.categoryid)) {
        this.editMode = 'New';
        this.plan.productChangeType = 'New';
        this.plan.username = 'Administrator';  // Only needed in debug mode as the azure login doesn't pass through
        this.plan.planType = 'Standard';
        this.setDefaultRadioStates();
      }
      else
      {
        this.editMode = 'Edit';
        this.loadPlan(this.id, this.categoryid, 'Edit');
      }

    });

    // Get the username when hosted, if empty put in Admin as default
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.get('/.auth/me', { headers })
      .map(x => x.json())
      .subscribe(ud => {
          this.userdata = ud;

          for (var prop in this.userdata[0]) {
            if (prop == 'user_id') {
              this.username = this.userdata[0][prop]
            }
            else {
              this.username = 'Administrator';
            }
            console.log('Username: ' + this.username);
          }
        },
        err => {
          console.log(err);
          this.username = 'Administrator';
        });
  }

  loadPlan(planId: number, categoryId: number,  editMode: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Cache-Control', 'no-cache');
    headers.append('Pragma', 'no-cache');


    // Get the brands
    this.http.get('/api/mobile/' + planId + '?categoryId=' + categoryId, { headers })
      .map(x => x.json())
      .subscribe(p => {
        this.plan = p;
        this.plan.productChangeType = 'Update';
        console.log('Plan Loaded: ' + this.plan.id);
        this.setDefaultRadioStates();
      });
  }

  openConfirm(content, formIncomplete) {
    if (this.checkFormValid()) {
      this.modalRef = this.modalService.open(content, { backdrop: 'static' });
    } else
    {
      this.modalRef = this.modalService.open(formIncomplete, { backdrop: 'static' });
    }
  }

  public closeModal() {
    this.modalRef.close();
  }

  saveForm(postSubmission, formError) {
    this.closeModal();
    this.plan.username = this.username;
    this.plan.productStatus = this.plan.productChangeType == 'Deactivate' ? false : true;

    let body = JSON.stringify(this.plan);
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');

    let apiString = this.plan.productCategoryId == 1 ? '/api/mobile/postpaid/' : '/api/mobile/prepaid/';

    if (this.plan.productChangeType == 'New') {
      this.http.post(apiString, body, { headers }).subscribe(data => {
        console.log('post ok');
        this.modalRef = this.modalService.open(postSubmission);
      }, error => {
        console.log(error);
        this.modalRef = this.modalService.open(formError);
      });
    } else
    {
      this.http.put(apiString, body, { headers }).subscribe(data => {
        console.log('put ok');
        this.modalRef = this.modalService.open(postSubmission, { backdrop: 'static' });
      }, error => {
        console.log(error);
        this.modalRef = this.modalService.open(formError);
      });
    }
  }

  checkFormValid():boolean
  {
    if (
      this.plan.segment == undefined ||
      this.plan.brandId == undefined ||
      this.plan.productTypeId == undefined ||
      this.plan.termId == undefined ||
      this.plan.productNameId == undefined ||
      this.plan.productChangeType == undefined ||
      this.plan.planType == undefined ||
      this.plan.bundleDiscount == undefined ||
      this.plan.standardData == undefined ||
      this.plan.standardDomesticVoiceInclusions == undefined ||
      this.plan.standardDomesticTextInclusions == undefined ||
      this.plan.internationalVoice1Inclusions == undefined ||
      this.plan.internationalVoice2Inclusions == undefined ||
      this.plan.internationalTextInclusions == undefined ||
      this.plan.internationalRoamIndicator == undefined ||
      this.plan.frequentFlyerIndicator == undefined ||
      this.plan.standardContentIndicator == undefined ||
      this.plan.otherInclusionsIndicator == undefined ||
      this.plan.promotionalFeeIndicator == undefined ||
      this.plan.promotionalStudentDiscountIndicator == undefined ||
      this.plan.promotionalFeeFreeMonthsIndicator == undefined ||
      this.plan.promotionalBonusData1Indicator == undefined ||
      this.plan.promotionalBonusData2Indicator == undefined ||
      this.plan.promotionalDomesticVoiceIndicator == undefined ||
      this.plan.promotionalDomesticTextIndicator == undefined ||
      this.plan.promotionalInternationalVoiceIndicator == undefined ||
      this.plan.promotionalInternationalTextIndicator == undefined ||
      this.plan.promotionalInternationalRoamIndicator == undefined ||
      this.plan.promotionalFrequentFlyerIndicator == undefined ||
      this.plan.promotionalContentIndicator == undefined ||
      this.plan.promotionalContentIndicator == undefined ||
      this.plan.promotionalOtherIndicator == undefined
    )
    { return false }
    else {
      return true;
    }
  }
  // TODO: REFACTOR

  validatePromotional(): boolean {
    if (this.plan.planType === 'Promotional') {
      if (!this.plan.promotionalPlanFee) return true;
      if (!this.plan.promotionalOnlyOfferStartDate) return true;
      if (!this.plan.promotionalOnlyOfferEndDate) return true;
    }
    return false;
  }
  validateStudentDiscount(): boolean {
    if (this.plan.studentDiscount == 'Yes') {
      if (!this.plan.studentDiscountValue) return true;
      if (!this.plan.studentDiscountType) return true;
    }
    return false;
  }
  validateBundleDiscount(): boolean {
    if (this.plan.bundleDiscount !== 'None') {
      if (!this.plan.bundleDiscountDetails) return true;
    }
    return false;
  }
  validateCappedValues(): boolean {
    if (this.plan.standardDomesticVoiceInclusions == 'Value Capped'
      || this.plan.standardDomesticVoiceInclusions == 'Minutes Capped' ) {
      if (!this.plan.standardDomesticVoiceValue) return true;
    }
    return false;
  }
  validateDomesticText(): boolean {
    if (this.plan.standardDomesticTextInclusions == 'Number') {
      if (!this.plan.standardDomesticTextAmount) return true;
    }
    return false;
  }
  validateUnlimitedPlan1(): boolean {
    if (this.plan.internationalVoice1Inclusions == 'Unlimited') {
      if (this.plan.internationalVoice1IncludedInPlan == undefined) return true;
      if (!this.plan.internationalVoice1CountriesAmount) return true;
      if (!this.plan.internationalVoice1CountriesList) return true;
    }
    return false;
  }
  validateUnlimitedPlan2(): boolean {
    if (this.plan.internationalVoice2Inclusions == 'Unlimited') {
      if (this.plan.internationalVoice2IncludedInPlan== undefined) return true;
      if (!this.plan.internationalVoice2CountriesAmount) return true;
      if (!this.plan.internationalVoice2CountriesList) return true;
    }
    return false;
  }
  validatePlanFree1(): boolean {
    if (this.plan.internationalVoice1IncludedInPlan == false) {
      if (!this.plan.internationalVoice1AdditionalCost) return true;
    }
    return false;
  }
  validatePlanFree2(): boolean {
    if (this.plan.internationalVoice2IncludedInPlan == false) {
      if (!this.plan.internationalVoice2AdditionalCost) return true;
    }
    return false;
  }

  validateInternationalVoice1(): boolean {
    if (this.plan.internationalVoice1Inclusions == 'Value Capped' ||
      this.plan.internationalVoice1Inclusions == 'Minutes Capped') {
      if (!this.plan.internationalVoice1Value) return true;
      if (!this.plan.internationalVoice1CountriesAmount) return true;
      if (!this.plan.internationalVoice1CountriesList) return true;
    }
    return false;
  }
  validateInternationalText(): boolean {
    if (this.plan.internationalTextInclusions == 'Unlimited') {
      if (!this.plan.internationalTextIncludedCountries) return true;
      if (!this.plan.internationalTextDetails) return true;
    } else if (this.plan.internationalTextInclusions == 'Included In Value' ||
      this.plan.internationalTextInclusions == 'Number Capped') {
      if (!this.plan.internationalTextIncludedCountries) return true;
      if (!this.plan.internationalTextDetails) return true;
      if (!this.plan.internationalTextValue) return true;
    }
    return false;
  }

  validateInternationalVoice2(): boolean {
    if (this.plan.internationalVoice2Inclusions == 'Value Capped' ||
      this.plan.internationalVoice2Inclusions == 'Minutes Capped') {
      if (!this.plan.internationalVoice2Value) return true;
      if (!this.plan.internationalVoice2CountriesAmount) return true;
      if (!this.plan.internationalVoice2CountriesList) return true;
    }
    return false;
  }
  validateOtherCountries(): boolean {
    if (this.plan.internationalTextIncludedCountries == 'Other') {
      if (!this.plan.internationalTextCountriesAmount) return true;
      if (!this.plan.internationalTextCountriesList) return true;
    }
    return false;
  }
  validateRoaming(): boolean {
    if (this.plan.internationalRoamIndicator== true) {
      if (!this.plan.internationalRoamIncludedInPlan) return true;
      if (!this.plan.internationalRoamDetails) return true;
      if (!this.plan.internationalRoamCountryAmount) return true;
      if (!this.plan.internationalRoamCountryList) return true;
    }
    return false;
  }
  validatePlanFreeRoaming(): boolean {
    if (this.plan.internationalRoamIncludedInPlan == false) {
      if (!this.plan.internationalRoamAdditionalCost) return true;
    }
    return false;
  }
  validateStep1(): boolean {
    if (this.validatePromotional()) return true;
    return false;
  }
  validateStep2(): boolean {
    if (this.validateStudentDiscount()) return true;
    if (this.validateBundleDiscount()) return true;
    if (this.validateCappedValues()) return true;
    if (this.validateDomesticText()) return true;
    return false;
  }
  validateStep3(): boolean {
    if (this.validateInternationalVoice1()) return true;
    if (this.validatePlanFree1()) return true;
    if (this.validateInternationalVoice2()) return true;
    if (this.validatePlanFree2()) return true;
    if (this.validateUnlimitedPlan1()) return true;
    if (this.validateUnlimitedPlan2()) return true;
    if (this.validateInternationalText()) return true;
    if (this.validateOtherCountries()) return true;
    if (this.validateRoaming()) return true;
    if (this.validatePlanFreeRoaming()) return true;
    return false;
  }

  // Set the default states in the model that drive radio buttons (requirement that they are pre-selected)
  private setDefaultRadioStates() {
    this.plan.standardDomesticVoiceInclusions = this.plan.standardDomesticVoiceInclusions ? this.plan.standardDomesticVoiceInclusions : 'None';
    this.plan.standardDomesticTextInclusions = this.plan.standardDomesticTextInclusions ? this.plan.standardDomesticTextInclusions : 'None';
    this.plan.bundleDiscount = this.plan.bundleDiscount ? this.plan.bundleDiscount : 'None';
    this.plan.standardDomesticCustomerToCustomerIndicator = this.plan.standardDomesticCustomerToCustomerIndicator ? this.plan.standardDomesticCustomerToCustomerIndicator : false;
    this.plan.standardDomesticPrepaidExtraCreditIndicator = this.plan.standardDomesticPrepaidExtraCreditIndicator ? this.plan.standardDomesticPrepaidExtraCreditIndicator : false;

    this.plan.internationalVoice1Inclusions = this.plan.internationalVoice1Inclusions ? this.plan.internationalVoice1Inclusions : 'None';
    this.plan.internationalVoice2Inclusions = this.plan.internationalVoice2Inclusions ? this.plan.internationalVoice2Inclusions : 'None';
    this.plan.internationalTextInclusions = this.plan.internationalTextInclusions ? this.plan.internationalTextInclusions : 'None';
    this.plan.internationalRoamIndicator = this.plan.internationalRoamIndicator ? this.plan.internationalRoamIndicator : false;
    this.plan.frequentFlyerIndicator = this.plan.frequentFlyerIndicator ? this.plan.frequentFlyerIndicator : false;

    this.plan.standardContentIndicator = this.plan.standardContentIndicator ? this.plan.standardContentIndicator : false;

    this.plan.otherInclusionsIndicator = this.plan.otherInclusionsIndicator ? this.plan.otherInclusionsIndicator : false;

    this.plan.promotionalFeeIndicator = this.plan.promotionalFeeIndicator ? this.plan.promotionalFeeIndicator : false;
    this.plan.promotionalStudentDiscountIndicator = this.plan.promotionalStudentDiscountIndicator ? this.plan.promotionalStudentDiscountIndicator : false;
    this.plan.promotionalFeeFreeMonthsIndicator = this.plan.promotionalFeeFreeMonthsIndicator ? this.plan.promotionalFeeFreeMonthsIndicator : false;
    this.plan.promotionalBonusData1Indicator = this.plan.promotionalBonusData1Indicator ? this.plan.promotionalBonusData1Indicator : false;
    this.plan.promotionalBonusData2Indicator = this.plan.promotionalBonusData2Indicator ? this.plan.promotionalBonusData2Indicator : false;
    this.plan.promotionalDomesticVoiceIndicator = this.plan.promotionalDomesticVoiceIndicator ? this.plan.promotionalDomesticVoiceIndicator : false;
    this.plan.promotionalDomesticTextIndicator = this.plan.promotionalDomesticTextIndicator ? this.plan.promotionalDomesticTextIndicator : false;
    this.plan.promotionalInternationalVoiceIndicator = this.plan.promotionalInternationalVoiceIndicator ? this.plan.promotionalInternationalVoiceIndicator : false;
    this.plan.promotionalInternationalTextIndicator = this.plan.promotionalInternationalTextIndicator ? this.plan.promotionalInternationalTextIndicator : false;
    this.plan.promotionalInternationalRoamIndicator = this.plan.promotionalInternationalRoamIndicator ? this.plan.promotionalInternationalRoamIndicator : false;
    this.plan.promotionalFrequentFlyerIndicator = this.plan.promotionalFrequentFlyerIndicator ? this.plan.promotionalFrequentFlyerIndicator : false;
    this.plan.promotionalContentIndicator = this.plan.promotionalContentIndicator ? this.plan.promotionalContentIndicator : false;
    this.plan.promotionalOtherIndicator = this.plan.promotionalOtherIndicator ? this.plan.promotionalOtherIndicator : false;
    this.plan.promotionalPrepaidBonusCreditIndicator = this.plan.promotionalPrepaidBonusCreditIndicator ? this.plan.promotionalPrepaidBonusCreditIndicator : false;


  }




}
