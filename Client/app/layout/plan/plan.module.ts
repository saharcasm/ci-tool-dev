﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PlanRoutingModule } from './plan-routing.module';
import { PlanComponent } from './plan.component';
import { FormsModule } from '@angular/forms';
import { Tab1Component } from "./tab1.component";
import { Tab2Component } from "./tab2.component";
import { Tab3Component } from "./tab3.component";
import { Tab4Component } from "./tab4.component";
import { Tab5Component } from "./tab5.component";
import { Tab6Component } from "./tab6.component";
import { PlanDetailsComponent } from "./planDetails.component";
import { MyDatePickerModule } from 'mydatepicker';
import { PipeModule } from '../pipe.module';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';

@NgModule({
  imports: [
    CommonModule,
    PlanRoutingModule,
    NgbModule.forRoot(),
    FormsModule,
    MyDatePickerModule,
    PipeModule,
    Ng2DeviceDetectorModule.forRoot()
  ],
  declarations: [
    PlanComponent,
    PlanDetailsComponent,
    Tab1Component,
    Tab2Component,
    Tab3Component,
    Tab4Component,
    Tab5Component,
    Tab6Component,
  ]
})
export class PlanModule { }
