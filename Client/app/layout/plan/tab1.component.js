var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { MobilePlan } from '../models';
import { MatchesProductCategoryPipe, MatchesProductCategoryBrandPipe } from '../filters.pipe';
import { Ng2DeviceService } from 'ng2-device-detector';
var Tab1Component = /** @class */ (function () {
    function Tab1Component(http, deviceService) {
        this.http = http;
        this.deviceService = deviceService;
        this.highDate = { date: { year: 9999, month: 12, day: 31 } };
        this.myDatePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su',
            width: '250px'
        };
    }
    Tab1Component.prototype.ngOnInit = function () {
        var _this = this;
        window.scrollTo(0, 0);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.promotionalOfferUntilWithdrawn = this.plan.promotionalOnlyOfferEndDate ?
            (this.plan.promotionalOnlyOfferEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
        // Get the Product Categories
        this.http.get('/api/productcategory/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (pc) {
            _this.productCategories = pc;
            _this.promotionalOfferUntilWithdrawn = _this.plan.promotionalOnlyOfferEndDate ?
                (_this.plan.promotionalOnlyOfferEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
            var productCategory = _this.getStringById(_this.plan.productCategoryId, _this.productCategories, 'productCategoryId');
            if (productCategory)
                _this.plan.productCategoryString = productCategory.productCategoryName;
        });
        // default the Segment Selector if this is a new plan
        if (this.editMode == 'New') {
            this.selectedSegment = 'Consumer';
        }
    };
    Tab1Component.prototype.preventKeyStrokes = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    };
    Tab1Component.prototype.getHighDate = function (inputCheckBox) {
        this.deviceInfo = this.deviceService.getDeviceInfo();
        // console.log(this.deviceInfo['browser']);
        if (this.deviceInfo['browser'] == 'ie' || this.deviceInfo['browser'] == 'ms-edge') {
            return inputCheckBox ? this.highDate : null;
        }
        else {
            return !inputCheckBox ? this.highDate : null;
        }
    };
    Tab1Component.prototype.validateFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
            if (fields[i] == undefined)
                return true; // disable the tab
        }
        return false;
    };
    Tab1Component.prototype.validateDependent = function (parentFields) {
        for (var i = 0; i < parentFields.length; i++) {
            if (this.validateFields(parentFields[i].dependentFields))
                return true;
        }
        return false;
    };
    Tab1Component.prototype.validateTab1 = function () {
        var parentFields = [];
        if (this.plan.planType == 'Promotional') {
            var fields = [this.plan.promotionalPlanFee, this.plan.promotionalOnlyOfferStartDate, this.plan.promotionalOnlyOfferEndDate];
            parentFields.push({ parentField: this.plan.planType, dependentFields: fields });
        }
        this.plan.disableTab2 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;
    };
    Tab1Component.prototype.getStringById = function (id, selectorArray, selector) {
        if (selectorArray)
            return selectorArray.find(function (i) { return i[selector] == id; });
        return null;
    };
    Tab1Component.prototype.setBrand = function () {
        var brand = this.getStringById(this.plan.brandId, this.plan.brands, 'brandId');
        if (brand) {
            this.plan.brandString = brand.brandName;
            this.loadProducts();
        }
    };
    Tab1Component.prototype.setProductType = function () {
        var productType = this.getStringById(this.plan.productTypeId, this.plan.productTypes, 'productTypeId');
        console.log("productType:: ", productType);
        if (productType)
            this.plan.productTypeString = productType.productTypeName;
    };
    Tab1Component.prototype.setProductName = function () {
        var productName = this.getStringById(this.plan.productNameId, this.plan.productNames, 'productNameId');
        console.log("productName:: ", productName);
        if (productName)
            this.plan.productNameString = productName.productNameName;
    };
    Tab1Component.prototype.setTerm = function () {
        var term = this.getStringById(this.plan.termId, this.plan.terms, 'termId');
        console.log("term:: ", term);
        if (term)
            this.plan.termString = term.termValue + ' - ' + term.unit;
    };
    Tab1Component.prototype.loadProducts = function () {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/api/productname/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (t) {
            _this.plan.productNames = new MatchesProductCategoryBrandPipe().transform(t, _this.plan.productCategoryId.toString(), _this.plan.brandId.toString());
            console.log("herehereh:: ", t);
            _this.setProductName();
        });
    };
    Tab1Component.prototype.loadSelectors = function () {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        // Get the brands
        var productCategory = this.getStringById(this.plan.productCategoryId, this.productCategories, 'productCategoryId');
        if (productCategory)
            this.plan.productCategoryString = productCategory.productCategoryName;
        console.log("productCategory:: ", productCategory);
        this.http.get('/api/brand/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (b) {
            _this.plan.brands = new MatchesProductCategoryPipe().transform(b, _this.plan.productCategoryId.toString());
            _this.setBrand();
        });
        // Get Product Types
        this.http.get('/api/producttype/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (pt) {
            _this.plan.productTypes = new MatchesProductCategoryPipe().transform(pt, _this.plan.productCategoryId.toString());
            _this.setProductType();
        });
        // Get Terms
        this.http.get('/api/term/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (t) {
            _this.plan.terms = new MatchesProductCategoryPipe().transform(t, _this.plan.productCategoryId.toString());
            _this.setTerm();
        });
        // Get Product Names
        // this.http.get('/api/productname/active', { headers })
        // .map(x => x.json())
        // .subscribe(t => {
        //   console.log("herehereh:: ", t);
        //   this.plan.productNames = new MatchesProductCategoryBrandPipe().transform(t, this.plan.productCategoryId.toString(), this.plan.brandId.toString());
        //   this.setProductName();
        // });
    };
    Tab1Component.prototype.ngOnChanges = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        // Set the required Date fields
        if (this.plan) {
            this.promotionalOnlyOfferStartDate = this.plan.promotionalOnlyOfferStartDate ? this.returnDateStruct(this.plan.promotionalOnlyOfferStartDate) : null;
            this.promotionalOnlyOfferEndDate = this.plan.promotionalOnlyOfferEndDate ? this.returnDateStruct(this.plan.promotionalOnlyOfferEndDate) : null;
            this.plan.planType = this.plan.planType ? this.plan.planType : 'Standard';
            this.validateTab1();
        }
        if (this.plan && this.plan.productCategory) {
            this.loadSelectors();
        }
    };
    Tab1Component.prototype.returnDateStruct = function (datein) {
        var newDate = new Date(datein);
        return { date: { year: newDate.getFullYear(), month: (newDate.getMonth() + 1), day: newDate.getDate() } };
    };
    Tab1Component.prototype.formatDate = function (event) {
        if (!event)
            return undefined;
        return event.jsdate.getFullYear() + '-' + (event.jsdate.getMonth() + 1) + '-' + event.jsdate.getDate();
    };
    __decorate([
        Input(),
        __metadata("design:type", MobilePlan)
    ], Tab1Component.prototype, "plan", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], Tab1Component.prototype, "editMode", void 0);
    Tab1Component = __decorate([
        Component({
            selector: 'tab1',
            templateUrl: './tab1.component.html',
            styleUrls: ['./plan.component.scss']
        }),
        __metadata("design:paramtypes", [Http, Ng2DeviceService])
    ], Tab1Component);
    return Tab1Component;
}());
export { Tab1Component };
