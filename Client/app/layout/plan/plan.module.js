var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PlanRoutingModule } from './plan-routing.module';
import { PlanComponent } from './plan.component';
import { FormsModule } from '@angular/forms';
import { MdRadioModule } from '@angular/material';
import { Tab1Component } from "./tab1.component";
import { Tab2Component } from "./tab2.component";
import { Tab3Component } from "./tab3.component";
import { Tab4Component } from "./tab4.component";
import { Tab5Component } from "./tab5.component";
import { Tab6Component } from "./tab6.component";
import { PlanDetailsComponent } from "./planDetails.component";
import { MyDatePickerModule } from 'mydatepicker';
import { PipeModule } from '../pipe.module';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';
var PlanModule = /** @class */ (function () {
    function PlanModule() {
    }
    PlanModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                PlanRoutingModule,
                NgbModule.forRoot(),
                FormsModule,
                MyDatePickerModule,
                PipeModule,
                MdRadioModule,
                Ng2DeviceDetectorModule.forRoot()
            ],
            declarations: [
                PlanComponent,
                PlanDetailsComponent,
                Tab1Component,
                Tab2Component,
                Tab3Component,
                Tab4Component,
                Tab5Component,
                Tab6Component,
            ]
        })
    ], PlanModule);
    return PlanModule;
}());
export { PlanModule };
