import { Component, OnInit, Input, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Brand, ProductType, Term, ProductName, MobilePlan, ProductCategory } from '../models';
import { IMyDpOptions } from 'mydatepicker';
import { MatchesProductCategoryPipe, MatchesProductCategoryBrandPipe } from '../filters.pipe';
import { Ng2DeviceService } from 'ng2-device-detector';

@Component({
  selector: 'tab1',
  templateUrl: './tab1.component.html',
  styleUrls: ['./plan.component.scss']
})

export class Tab1Component implements OnInit {
  @Input() plan: MobilePlan;
  @Input() editMode: string;

  brands: Brand[];
  productTypes: ProductType[];
  terms: Term[];
  productNames: ProductName[];
  productCategories: ProductCategory[];
  selectedSegment: string;
  promotionalOnlyOfferStartDate: object;
  promotionalOnlyOfferEndDate: object;
  selectedUnit: string;
  promotionalOfferUntilWithdrawn: boolean;
  deviceInfo: string;
  highDate = { date: { year: 9999, month: 12, day: 31 } };


  constructor(private http: Http, private deviceService: Ng2DeviceService) { }

  ngOnInit() {
    window.scrollTo(0, 0);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Cache-Control', 'no-cache');
    headers.append('Pragma', 'no-cache');

    this.promotionalOfferUntilWithdrawn = this.plan.promotionalOnlyOfferEndDate ? 
    (this.plan.promotionalOnlyOfferEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;

    // Get the Product Categories
    this.http.get('/api/productcategory/active', { headers })
    .map(x => x.json())
    .subscribe(pc => {
      this.productCategories = pc;
    this.promotionalOfferUntilWithdrawn = this.plan.promotionalOnlyOfferEndDate ? 
    (this.plan.promotionalOnlyOfferEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;



      let productCategory = this.getStringById(this.plan.productCategoryId, this.productCategories, 'productCategoryId');
      if (productCategory)
        this.plan.productCategoryString = productCategory.productCategoryName;


    });





    // default the Segment Selector if this is a new plan
    if (this.editMode == 'New')
    {
      this.selectedSegment = 'Consumer';
    }


  }

  preventKeyStrokes(event) {
    let charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }



  private getHighDate(inputCheckBox: boolean) {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    // console.log(this.deviceInfo['browser']);
    if (this.deviceInfo['browser'] == 'ie' || this.deviceInfo['browser'] == 'ms-edge') {
      return inputCheckBox ? this.highDate : null;
    } else {
      return !inputCheckBox ? this.highDate : null;
    }
  }

  validateFields(fields): boolean {
    for (let i = 0; i < fields.length; i++) {
      if (fields[i] == undefined) return true; // disable the tab
    }
    return false;
  }

  validateDependent (parentFields){
    for (let i = 0; i < parentFields.length; i++) {
      if (this.validateFields(parentFields[i].dependentFields)) return true;
    }
    return false;
  }

  validateTab1() {
    let parentFields = [];
    if (this.plan.planType == 'Promotional'){
      let fields = [this.plan.promotionalPlanFee, this.plan.promotionalOnlyOfferStartDate, this.plan.promotionalOnlyOfferEndDate];
      parentFields.push({parentField: this.plan.planType, dependentFields: fields});
    }

    this.plan.disableTab2 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;
  }

  getStringById(id, selectorArray, selector){
    if(selectorArray)
      return selectorArray.find(i => i[selector]== id);
    return null;
  }

  setBrand(){
    let brand=this.getStringById(this.plan.brandId, this.plan.brands, 'brandId');

    if(brand){
      this.plan.brandString=brand.brandName;
      this.loadProducts();
    }
  }
  setProductType(){
    let productType=this.getStringById(this.plan.productTypeId, this.plan.productTypes,'productTypeId');
    console.log("productType:: ", productType);

    if(productType)
      this.plan.productTypeString=productType.productTypeName;
  }
  setProductName(){
    let productName=this.getStringById(this.plan.productNameId, this.plan.productNames, 'productNameId');
    console.log("productName:: ", productName);

    if(productName)
      this.plan.productNameString=productName.productNameName;
  }
  setTerm(){
    let term=this.getStringById(this.plan.termId, this.plan.terms, 'termId');
    console.log("term:: ", term);

    if(term)
      this.plan.termString=term.termValue + ' - ' + term.unit;
  }

  loadProducts(){
   let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Cache-Control', 'no-cache');
    headers.append('Pragma', 'no-cache');
 
    this.http.get('/api/productname/active', { headers })
    .map(x => x.json())
    .subscribe(t => {
      this.plan.productNames = new MatchesProductCategoryBrandPipe().transform(t, this.plan.productCategoryId.toString(), this.plan.brandId.toString());
            console.log("herehereh:: ", t);

      this.setProductName();
    });


  }


  loadSelectors() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Cache-Control', 'no-cache');
    headers.append('Pragma', 'no-cache');
    // Get the brands

    let productCategory = this.getStringById(this.plan.productCategoryId, this.productCategories, 'productCategoryId');
    if (productCategory)
      this.plan.productCategoryString = productCategory.productCategoryName;

    console.log("productCategory:: ", productCategory)

    this.http.get('/api/brand/active', { headers })
    .map(x => x.json())
    .subscribe(b => {
      this.plan.brands = new MatchesProductCategoryPipe().transform(b,this.plan.productCategoryId.toString());
      this.setBrand()
    });

    // Get Product Types
    this.http.get('/api/producttype/active', { headers })
    .map(x => x.json())
    .subscribe(pt => {
      this.plan.productTypes = new MatchesProductCategoryPipe().transform(pt, this.plan.productCategoryId.toString());
      this.setProductType();
    });

    // Get Terms
    this.http.get('/api/term/active', { headers })
    .map(x => x.json())
    .subscribe(t => {
      this.plan.terms = new MatchesProductCategoryPipe().transform(t, this.plan.productCategoryId.toString());
      this.setTerm();
    });

    // Get Product Names
    // this.http.get('/api/productname/active', { headers })
    // .map(x => x.json())
    // .subscribe(t => {
    //   console.log("herehereh:: ", t);
    //   this.plan.productNames = new MatchesProductCategoryBrandPipe().transform(t, this.plan.productCategoryId.toString(), this.plan.brandId.toString());
    //   this.setProductName();
    // });
  }

  ngOnChanges(...args: any[]) {
    // Set the required Date fields
    if (this.plan) {
      this.promotionalOnlyOfferStartDate = this.plan.promotionalOnlyOfferStartDate ? this.returnDateStruct(this.plan.promotionalOnlyOfferStartDate) : null;
      this.promotionalOnlyOfferEndDate = this.plan.promotionalOnlyOfferEndDate ? this.returnDateStruct(this.plan.promotionalOnlyOfferEndDate) : null;
      this.plan.planType = this.plan.planType ? this.plan.planType : 'Standard';
      this.validateTab1();
    }

    if (this.plan && this.plan.productCategory) {
      this.loadSelectors();
    }
  }
  private returnDateStruct(datein: any){
    let newDate = new Date(datein);
    return { date: { year: newDate.getFullYear(), month: (newDate.getMonth() + 1), day: newDate.getDate() } };
  }

  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su',
    width: '250px'
  };

  private formatDate(event) {
    if(!event) return undefined;
    return event.jsdate.getFullYear() + '-' + (event.jsdate.getMonth() + 1) + '-' + event.jsdate.getDate();
  }
}
