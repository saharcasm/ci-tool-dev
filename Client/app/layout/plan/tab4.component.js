var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { MobilePlan } from "../models";
var Tab4Component = /** @class */ (function () {
    function Tab4Component() {
    }
    Tab4Component.prototype.ngOnInit = function () {
        window.scrollTo(0, 0);
        this.setDefaults();
    };
    Tab4Component.prototype.ngOnChanges = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (this.plan) {
            this.validateTab4();
        }
    };
    Tab4Component.prototype.setDefaults = function () {
        console.log("this.plan.standardContentSvodDataFree:: ", this.plan.standardContentSvodDataFree);
        if (this.plan.standardContentSportIndicator == undefined)
            this.plan.standardContentSportIndicator = false;
        if (this.plan.standardContentMusicIndicator == undefined)
            this.plan.standardContentMusicIndicator = false;
        if (this.plan.standardContentSvodIndicator == undefined)
            this.plan.standardContentSvodIndicator = false;
        if (this.plan.standardContentOther1Indicator == undefined)
            this.plan.standardContentOther1Indicator = false;
        if (this.plan.standardContentOther2Indicator == undefined)
            this.plan.standardContentOther2Indicator = false;
        //////////////////////////////////////////////////////////////////////////
        if (this.plan.standardContentSportDataFree == undefined)
            this.plan.standardContentSportDataFree = false;
        if (this.plan.standardContentSportIncludedInPlan == undefined)
            this.plan.standardContentSportIncludedInPlan = false;
        //////////////////////////////////////////////////////////////////////////
        if (this.plan.standardContentSvodDataFree == undefined)
            this.plan.standardContentSvodDataFree = false;
        if (this.plan.standardContentSvodIncludedInPlan == undefined)
            this.plan.standardContentSvodIncludedInPlan = false;
        //////////////////////////////////////////////////////////////////////////
        if (this.plan.standardContentMusicDataFree == undefined)
            this.plan.standardContentMusicDataFree = false;
        if (this.plan.standardContentMusicIncludedInPlan == undefined)
            this.plan.standardContentMusicIncludedInPlan = false;
        //////////////////////////////////////////////////////////////////////////
        if (this.plan.standardContentOther1DataFree == undefined)
            this.plan.standardContentOther1DataFree = false;
        if (this.plan.standardContentOther1IncludedInPlan == undefined)
            this.plan.standardContentOther1IncludedInPlan = false;
        //////////////////////////////////////////////////////////////////////////
        if (this.plan.standardContentOther2DataFree == undefined)
            this.plan.standardContentOther2DataFree = false;
        if (this.plan.standardContentOther2IncludedInPlan == undefined)
            this.plan.standardContentOther2IncludedInPlan = false;
        //////////////////////////////////////////////////////////////////////////
        if (this.plan.standardContentOther3Indicator == undefined)
            this.plan.standardContentOther3Indicator = false;
        if (this.plan.standardContentOther3DataFree == undefined)
            this.plan.standardContentOther3DataFree = false;
        if (this.plan.standardContentOther3IncludedInPlan == undefined)
            this.plan.standardContentOther3IncludedInPlan = false;
    };
    Tab4Component.prototype.preventKeyStrokes = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    };
    Tab4Component.prototype.validateFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
            if (fields[i] == undefined)
                return true; // disable the tab
        }
        return false;
    };
    Tab4Component.prototype.validateDependent = function (parentFields) {
        for (var i = 0; i < parentFields.length; i++) {
            if (this.validateFields(parentFields[i].dependentFields))
                return true;
        }
        return false;
    };
    Tab4Component.prototype.validateTab4 = function () {
        var parentFields = [];
        if (this.plan.standardContentIndicator == true) {
            var fields = [this.plan.standardContentSportIndicator, this.plan.standardContentMusicIndicator, this.plan.standardContentSvodIndicator,
                this.plan.standardContentOther1Indicator, this.plan.standardContentOther2Indicator];
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.standardContentSportIndicator == true) {
            var fields = [this.plan.standardContentSportDataFree, this.plan.standardContentSportIncludedInPlan, this.plan.standardContentSportInclusions, 0];
            if (this.plan.standardContentSportIncludedInPlan == false) {
                fields.push(this.plan.standardContentSportAdditionalCost);
            }
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.standardContentMusicIndicator == true) {
            var fields = [this.plan.standardContentMusicDataFree, this.plan.standardContentMusicIncludedInPlan, this.plan.standardContentMusicInclusions, 0];
            if (this.plan.standardContentMusicIncludedInPlan == false) {
                fields.push(this.plan.standardContentMusicAdditionalCost);
            }
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.standardContentSvodIndicator == true) {
            var fields = [this.plan.standardContentSvodDataFree, this.plan.standardContentSvodIncludedInPlan, this.plan.standardContentSvodInclusions, 0];
            if (this.plan.standardContentSvodIncludedInPlan == false) {
                fields.push(this.plan.standardContentSvodAdditionalCost);
            }
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.standardContentOther1Indicator == true) {
            var fields = [this.plan.standardContentOther1DataFree, this.plan.standardContentOther1IncludedInPlan, this.plan.standardContentOther1Inclusions, 0];
            if (this.plan.standardContentOther1IncludedInPlan == false) {
                fields.push(this.plan.standardContentOther1AdditionalCost);
            }
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.standardContentOther2Indicator == true) {
            var fields = [this.plan.standardContentOther2DataFree, this.plan.standardContentOther2IncludedInPlan, this.plan.standardContentOther2Inclusions, 0];
            if (this.plan.standardContentOther2IncludedInPlan == false) {
                fields.push(this.plan.standardContentOther2AdditionalCost);
            }
            parentFields.push({ dependentFields: fields });
        }
        if (this.plan.standardContentOther3Indicator == true) {
            var fields = [this.plan.standardContentOther3DataFree, this.plan.standardContentOther3IncludedInPlan, this.plan.standardContentOther3Inclusions, 0];
            if (this.plan.standardContentOther3IncludedInPlan == false) {
                fields.push(this.plan.standardContentOther3AdditionalCost);
            }
            parentFields.push({ dependentFields: fields });
        }
        this.plan.disableTab5 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;
    };
    Tab4Component.prototype.clearAllContent = function () {
        console.log("clear all content");
        this.plan.standardContentSportIndicator = false;
        this.plan.standardContentSportDataFree = null;
        this.plan.standardContentSportIncludedInPlan = null;
        this.plan.standardContentSportAdditionalCost = null;
        this.plan.standardContentSportInclusions = null;
        this.plan.standardContentSportDetails = null;
        this.plan.standardContentMusicIndicator = false;
        this.plan.standardContentMusicDataFree = null;
        this.plan.standardContentMusicIncludedInPlan = null;
        this.plan.standardContentMusicAdditionalCost = null;
        this.plan.standardContentMusicInclusions = null;
        this.plan.standardContentMusicDetails = null;
        this.plan.standardContentSvodIndicator = false;
        this.plan.standardContentSvodDataFree = null;
        this.plan.standardContentSvodIncludedInPlan = null;
        this.plan.standardContentSvodAdditionalCost = null;
        this.plan.standardContentSvodInclusions = null;
        this.plan.standardContentSvodDetails = null;
        this.plan.standardContentOther1Indicator = false;
        this.plan.standardContentOther1DataFree = null;
        this.plan.standardContentOther1IncludedInPlan = null;
        this.plan.standardContentOther1AdditionalCost = null;
        this.plan.standardContentOther1Inclusions = null;
        this.plan.standardContentOther1Details = null;
        this.plan.standardContentOther2Indicator = false;
        this.plan.standardContentOther2DataFree = null;
        this.plan.standardContentOther2IncludedInPlan = null;
        this.plan.standardContentOther2AdditionalCost = null;
        this.plan.standardContentOther2Inclusions = null;
        this.plan.standardContentOther2Details = null;
        this.plan.standardContentOther3Indicator = false;
        this.plan.standardContentOther3DataFree = null;
        this.plan.standardContentOther3IncludedInPlan = null;
        this.plan.standardContentOther3AdditionalCost = null;
        this.plan.standardContentOther3Inclusions = null;
        this.plan.standardContentOther3Details = null;
    };
    Tab4Component.prototype.clearSportContent = function () {
        this.plan.standardContentSportDataFree = null;
        this.plan.standardContentSportIncludedInPlan = null;
        this.plan.standardContentSportAdditionalCost = null;
        this.plan.standardContentSportInclusions = null;
        this.plan.standardContentSportDetails = null;
    };
    Tab4Component.prototype.clearMusic = function () {
        this.plan.standardContentMusicDataFree = null;
        this.plan.standardContentMusicIncludedInPlan = null;
        this.plan.standardContentMusicAdditionalCost = null;
        this.plan.standardContentMusicInclusions = null;
        this.plan.standardContentMusicDetails = null;
    };
    Tab4Component.prototype.clearSVOD = function () {
        this.plan.standardContentSvodDataFree = null;
        this.plan.standardContentSvodIncludedInPlan = null;
        this.plan.standardContentSvodAdditionalCost = null;
        this.plan.standardContentSvodInclusions = null;
        this.plan.standardContentSvodDetails = null;
    };
    Tab4Component.prototype.clearOther1 = function () {
        this.plan.standardContentOther1Indicator = false;
        this.plan.standardContentOther1DataFree = null;
        this.plan.standardContentOther1IncludedInPlan = null;
        this.plan.standardContentOther1AdditionalCost = null;
        this.plan.standardContentOther1Inclusions = null;
        this.plan.standardContentOther1Details = null;
    };
    Tab4Component.prototype.clearOther2 = function () {
        this.plan.standardContentOther2DataFree = false;
        this.plan.standardContentOther2IncludedInPlan = null;
        this.plan.standardContentOther2AdditionalCost = null;
        this.plan.standardContentOther2Inclusions = null;
        this.plan.standardContentOther2Details = null;
    };
    Tab4Component.prototype.clearOther3 = function () {
        this.plan.standardContentOther3DataFree = false;
        this.plan.standardContentOther3IncludedInPlan = null;
        this.plan.standardContentOther3AdditionalCost = null;
        this.plan.standardContentOther3Inclusions = null;
        this.plan.standardContentOther3Details = null;
    };
    __decorate([
        Input(),
        __metadata("design:type", MobilePlan)
    ], Tab4Component.prototype, "plan", void 0);
    Tab4Component = __decorate([
        Component({
            selector: 'tab4',
            templateUrl: './tab4.component.html',
            styleUrls: ['./plan.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], Tab4Component);
    return Tab4Component;
}());
export { Tab4Component };
