﻿import { Component, OnInit, Input } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FrequentFlyerProgram, MobilePlan } from "../models";
@Component({
  selector: 'tab3',
  templateUrl: './tab3.component.html',
  styleUrls: ['./plan.component.scss']
})

export class Tab3Component implements OnInit {
  @Input() plan: MobilePlan;

  public ffPrograms: FrequentFlyerProgram[];
  internationalTextIncludedCountries: string;

  constructor(private http: Http) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // Get the FF Programs
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.get('/api/frequentflyer/active', { headers })
    .map(x => x.json())
    .subscribe(ff => {
      this.ffPrograms = ff;
    });
    // Default Radio states
    this.setDefaults();
  }

  setDefaults(){
    this.plan.internationalTextIncludedCountries = (this.plan.internationalTextCountriesAmount == this.plan.internationalVoice1CountriesAmount &&
      this.plan.internationalTextCountriesList == this.plan.internationalVoice1CountriesList) ? 'Same' : 'Other';

    if(this.plan.internationalRoamIncludedInPlan==undefined)
      this.plan.internationalRoamIncludedInPlan=true;

  }

  ngOnChanges(...args: any[]) {
    if (this.plan) {
      this.validateTab3();
    }
  }

  validateFields(fields): boolean {
    for (let i = 0; i < fields.length; i++) {
      if (fields[i] == undefined) return true; // disable the tab
    }
    return false;
  }

  validateDependent (parentFields){
    for (let i = 0; i < parentFields.length; i++) {
      console.log("this:: ",this.validateFields(parentFields[i].dependentFields));
      if (parentFields[i].dependentFields.length == 0) return false;
      if (this.validateFields(parentFields[i].dependentFields)) return true;
    }
    return false;
  }

  validateTab3() {
    let parentFields = [];
    if (this.plan.internationalVoice1Inclusions == 'Minutes Capped' || this.plan.internationalVoice1Inclusions == 'Value Capped'){
      let fields = [this.plan.internationalVoice1Value, this.plan.internationalVoice1CountriesAmount, this.plan.internationalVoice1CountriesList];
      parentFields.push({dependentFields: fields});
    }
    else if (this.plan.internationalVoice1Inclusions == 'Unlimited'){
      let fields = [this.plan.internationalVoice1IncludedInPlan, this.plan.internationalVoice1CountriesAmount, this.plan.internationalVoice1CountriesList];
      if (this.plan.internationalVoice1IncludedInPlan == false){
        fields.push(this.plan.internationalVoice1AdditionalCost);
      }
      parentFields.push({dependentFields: fields});
    }

    if (this.plan.internationalVoice2Inclusions == 'Minutes Capped' || this.plan.internationalVoice2Inclusions == 'Value Capped'){
      let fields = [this.plan.internationalVoice2Value];
      parentFields.push({parentField: this.plan.internationalVoice2Inclusions, dependentFields: fields});
    }
    if (this.plan.internationalVoice2Inclusions == 'Unlimited'){
      let fields = [this.plan.internationalVoice2IncludedInPlan, 0];
      if (this.plan.internationalVoice2IncludedInPlan == false){
        fields.push(this.plan.internationalVoice2AdditionalCost);
      }
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.internationalTextInclusions == 'Unlimited'){
      let fields = [this.plan.internationalTextIncludedCountries, this.plan.internationalTextDetails];
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.internationalTextInclusions == 'Number Capped' || this.plan.internationalTextInclusions == 'Included In Value') {
      let fields = [this.plan.internationalTextValue, this.plan.internationalTextIncludedCountries, this.plan.internationalTextDetails];
      if (this.plan.internationalTextIncludedCountries == 'Other') {
        fields.push(this.plan.internationalTextCountriesAmount);
        fields.push(this.plan.internationalTextCountriesList);
      }
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.internationalRoamIndicator == true) {
      let fields = [this.plan.internationalRoamIncludedInPlan, this.plan.internationalRoamDetails, this.plan.internationalRoamCountryAmount, this.plan.internationalRoamCountryList];
      if (this.plan.internationalRoamIncludedInPlan == false) {
        fields.push(this.plan.internationalRoamAdditionalCost);
      }
      parentFields.push({dependentFields: fields});

    }
    if (this.plan.frequentFlyerIndicator == true) {
      let fields = [this.plan.frequentFlyerProgramId, this.plan.frequentFlyerProgramDetails];
      parentFields.push({parentField: this.plan.frequentFlyerIndicator, dependentFields: fields});
    }


    this.plan.disableTab4 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;


  }

  setSameAsIV() {
    if (this.plan && this.plan.internationalTextIncludedCountries == 'Same') {
      this.plan.internationalTextCountriesAmount = this.plan.internationalVoice1CountriesAmount;
      this.plan.internationalTextCountriesList = this.plan.internationalVoice1CountriesList;
    }
  }

  preventKeyStrokes(event) {
    let charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  intVoice1Clear() {
    this.plan.internationalVoice1IncludedInPlan = false;
    this.plan.internationalVoice1AdditionalCost = null;
    this.plan.internationalVoice1Value = null;
    this.plan.internationalVoice1CountriesAmount = null;
    this.plan.internationalVoice1CountriesList = null;
  }

  intVoice2Clear() {
    this.plan.internationalVoice2IncludedInPlan = false;
    this.plan.internationalVoice2AdditionalCost = null;
    this.plan.internationalVoice2Value = null;
    this.plan.internationalVoice2CountriesAmount = null;
    this.plan.internationalVoice2CountriesList = null;
  }

  intTextClear() {
    this.plan.internationalTextCountriesAmount = null;
    this.plan.internationalTextCountriesList = null;
    this.plan.internationalTextDetails = null;
    this.plan.internationalTextValue = null;
  }

  intRoamClear() {
    this.plan.internationalRoamAdditionalCost = null;
    this.plan.internationalRoamCountryAmount = null;
    this.plan.internationalRoamCountryList = null;
    this.plan.internationalRoamDetails = null;
    this.plan.internationalRoamIncludedInPlan = null;
  }
}
