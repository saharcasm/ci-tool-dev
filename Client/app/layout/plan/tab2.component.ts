﻿import { Component, OnInit, Input } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MobilePlan } from "../models";

@Component({
  selector: 'tab2',
  templateUrl: './tab2.component.html',
  styleUrls: ['./plan.component.scss']
})

export class Tab2Component implements OnInit {
  @Input() plan: MobilePlan;

  studentDiscount: string;

  constructor() { }
  ngOnInit() {
    // Default plan values where they are empty
    window.scrollTo(0, 0);
    this.plan.studentDiscount = this.plan.studentDiscountType ? 'Yes' : 'No';
    this.plan.studentDiscount = this.studentDiscount;
    this.setDefaults();

  }
  setDefaults(){
    if(this.plan.studentDiscount==undefined)
      this.plan.studentDiscount='No';
  }

  ngOnChanges(...args: any[]) {
    if (this.plan) {
      this.validateTab2();
    }
  }

  validateFields(fields): boolean {
    for (let i = 0; i < fields.length; i++) {
      if (fields[i] == undefined) return true; // disable the tab
    }
    return false;
  }

  validateDependent (parentFields){
    for (let i = 0; i < parentFields.length; i++) {
      if (this.validateFields(parentFields[i].dependentFields)) return true;
    }
    return false;
  }

  validateTab2() {
    let parentFields = [];
    if (this.plan.studentDiscount == 'Yes'){
      let fields = [this.plan.studentDiscountType, this.plan.studentDiscountValue];
      parentFields.push({parentField: this.plan.studentDiscountType, dependentFields: fields});
    }
    if (this.plan.bundleDiscount == 'All' || this.plan.bundleDiscount == 'Online') {
      let fields = [this.plan.bundleDiscountDetails];
      parentFields.push({parentField: this.plan.bundleDiscount, dependentFields: fields});
    }
    if (this.plan.standardDomesticVoiceInclusions == 'Value Capped' || this.plan.standardDomesticVoiceInclusions == 'Minutes Capped') {
      let fields = [this.plan.standardDomesticVoiceValue];
      parentFields.push({parentField: this.plan.standardDomesticVoiceInclusions, dependentFields: fields});
    }
    if (this.plan.standardDomesticTextInclusions == 'Number') {
      let fields = [this.plan.standardDomesticTextAmount];
      parentFields.push({parentField: this.plan.standardDomesticTextInclusions, dependentFields: fields});
    }
    this.plan.disableTab3 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;
  }

  preventKeyStrokes(event) {
    let charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
  preventEnter(event) {
    let charCode = (event.which) ? event.which : event.keyCode;

    if (charCode == 13)
      return false;
    return true;
  }

}
