﻿import { Component, OnInit, Input, OnChanges, ChangeDetectionStrategy  } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MobilePlan } from "../models";
import { IMyDpOptions } from 'mydatepicker';
import { Ng2DeviceService } from 'ng2-device-detector';

@Component({
  selector: 'tab6',
  templateUrl: './tab6.component.html',
  styleUrls: ['./plan.component.scss']
})

export class Tab6Component implements OnInit {
  @Input() plan: MobilePlan;
  @Input() editMode: string;
  initialised: boolean;

  highDate = { date: { year: 9999, month: 12, day: 31 } };

  bonusData1Months: string;
  bonusData2Months: string;
  internationalRoamingPromotion: string;
  promotionalInternationalRoamingUntilWithdrawn: boolean;
  promotionalPrepaidBonusCreditUntilWithdrawn: boolean;
  promotionalPrepaidInternationalBonusCreditUntilWithdrawn: boolean;
  domesticVoiceUntilWithdrawn: boolean;
  bonusData2UntilWithdrawn: boolean;
  domesticTextUntilWithdrawn: boolean;
  internationalVoiceeUntilWithdrawn: boolean;
  internationalTexteUntilWithdrawn: boolean;
  frequentFlyerPromotion: string;
  promotionalFrequentFlyerUntilWithdrawn: boolean;
  promotionalContent: string;
  sportsPromotion: string;
  promotionalSportsUntilWithdrawn: boolean;
  studentFeeUntilWithdrawn: boolean;
  musicPromotion: string;
  promotionalMusicUntilWithdrawn: boolean;
  svodtvPromotion: string;
  promotionalSVODTVUntilWithdrawn: boolean;
  otherContentPromotion: string;
  promotionalOtherContentUntilWithdrawn: boolean;
  otherPromotionUntilWithdrawn: boolean;
  promotionalFeeUntilWithdrawn: boolean;
  feeFeeUntilWithdrawn: boolean;
  promoMonthlyFeeStartDate: object;
  promoMonthlyFeeEndDate: object;
  studentMonthlyFeeStartDate: object;
  studentMonthlyFeeEndDate: object;
  bonusPrepaidCreditDataStart: object;
  bonusPrepaidCreditDataEnd: object;
  feeFreeStart: object;
  feeFreeEnd: object;
  bonusData1Start: object
  bonusData1End: object;
  bonusData2Start: object;
  bonusData2End: object;
  bonusPrepaidBonusDataStart: object;
  bonusPrepaidBonusDataEnd: object;
  domesticVoicePromotionStart: object;
  domesticVoicePromotionEnd: object;
  domesticTextPromotionStart: object;
  domesticTextPromotionEnd: object;
  internationalVoicePromotionStart: object;
  internationalVoicePromotionEnd: object;
  internationalTextPromotionStart: object;
  internationalTextPromotionEnd: object;
  internationalRoamingPromotionStart: object;
  internationalRoamingPromotionEnd: object;
  frequentFlyerPromotionStart: object;
  frequentFlyerPromotionEnd: object;
  sportsPromotionStart: object;
  sportsPromotionEnd: object;
  musicPromotionStart: object;
  musicPromotionEnd: object;
  svodtvPromotionStart: object;
  svodtvPromotionEnd: object;
  otherContentPromotionStart: object;
  otherContentPromotionEnd: object;
  otherPromotionStart: object;
  otherPromotionEnd: object;
  deviceInfo: string;
  promotionalPrepaidInternationalBonusCreditsStart: object;
  promotionalPrepaidInternationalBonusCreditsEnd: object;

  constructor(private deviceService: Ng2DeviceService) {}

  ngOnInit() {
    //Defaults on toggles
    window.scrollTo(0, 0);
    this.initialised = false;
    this.sportsPromotion = this.plan.promotionalContentSport ? "Yes" : "No";
    this.musicPromotion = this.plan.promotionalContentMusic ? "Yes" : "No";
    this.svodtvPromotion = this.plan.promotionalContentSvod ? "Yes" : "No";
    this.otherContentPromotion = this.plan.promotionalContentOtherDetails ? "Yes" : "No";
    if(this.plan.promotionalPrepaidInternationalBonusCreditsIndicator==undefined){
      this.plan.promotionalPrepaidInternationalBonusCreditsIndicator=false;
    }

    this.setDateFields();
  }

  resetOtherFields()
  {
    this.plan.promotionalOtherDescription = null;
    this.plan.promotionalOtherDetails = null;
    this.plan.promotionalOtherStartDate = null;
    this.plan.promotionalOtherEndDate = null;
  }

  ngOnChanges(...args: any[]) {
    this.setDateFields();
    this.validateTab6();
  }
  validateFields(fields): boolean {
    for (let i = 0; i < fields.length; i++) {
      if (fields[i] == undefined) return true; // disable the tab
    }
    return false;
  }

  validateDependent (parentFields){
    for (let i = 0; i < parentFields.length; i++) {
      if (this.validateFields(parentFields[i].dependentFields)) return true;
    }
    return false;
  }

  validateTab6() {
    let parentFields = [];
    if (this.plan.promotionalFeeIndicator == true){
      let fields = [this.plan.promotionalFeeDiscountType, this.plan.promotionalFeeChannel, this.plan.promotionalFeeStartDate, this.plan.promotionalFeeEndDate, 0];
      if(this.plan.promotionalFeeDiscountType){
        fields.push(this.plan.promotionalFeeDiscountValue);
      }
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalStudentDiscountIndicator == true){
      let fields = [this.plan.promotionalStudentDiscountType, this.plan.promotionalStudentDiscountStartDate, this.plan.promotionalStudentDiscountEndDate, 0];
      if(this.plan.promotionalStudentDiscountType){
        fields.push(this.plan.promotionalStudentDiscountValue);
      }
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalFeeFreeMonthsIndicator == true){
      let fields = [this.plan.promotionalFeeFreeMonthsAmount, this.plan.promotionalFeeFreeMonthsChannel, this.plan.promotionalFeeFreeMonthsStartDate, this.plan.promotionalFeeFreeMonthsEndDate, 0];
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalBonusData1Indicator == true){
      let fields = [this.plan.promotionalBonusData1Amount, this.plan.promotionalBonusData1Channel, this.plan.promotionalBonusData1StartDate, this.plan.promotionalBonusData1EndDate, 0];
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalBonusData2Indicator == true){
      let fields = [this.plan.promotionalBonusData2Amount, this.plan.promotionalBonusData2Channel, this.plan.promotionalBonusData2StartDate, this.plan.promotionalBonusData2EndDate, 0];
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalDomesticVoiceIndicator == true){
      let fields = [this.plan.promotionalDomesticVoiceType, this.plan.promotionalDomesticVoiceStartDate, this.plan.promotionalDomesticVoiceEndDate, 0];
      if(this.plan.promotionalDomesticVoiceType){
        fields.push(this.plan.promotionalDomesticVoiceValue);
      }
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalDomesticTextIndicator == true){
      let fields = [this.plan.promotionalDomesticTextType, this.plan.promotionalDomesticTextStartDate, this.plan.promotionalDomesticTextEndDate, 0];
      if(this.plan.promotionalDomesticTextType){
        fields.push(this.plan.promotionalDomesticTextValue);
      }
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalInternationalVoiceIndicator == true){
      let fields = [this.plan.promotionalInternationalVoiceType, this.plan.promotionalInternationalVoiceCountryAmount,
        this.plan.promotionalInternationalVoiceCountryList, this.plan.promotionalInternationalVoiceStartDate, this.plan.promotionalInternationalVoiceEndDate, 0];
      if(this.plan.promotionalInternationalVoiceType=='Value' || this.plan.promotionalInternationalVoiceType=='Minutes'){
        fields.push(this.plan.promotionalInternationalVoiceValue);
      }
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalInternationalTextIndicator == true){
      let fields = [this.plan.promotionalInternationalTextType, this.plan.promotionalInternationalTextStartDate, this.plan.promotionalInternationalTextEndDate, 0];
      if(this.plan.promotionalInternationalTextType){
        fields.push(this.plan.promotionalInternationalTextValue);
      }
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalInternationalRoamIndicator == true){
      let fields = [this.plan.promotionalInternationalRoamDetails, this.plan.promotionalInternationalRoamStartDate, this.plan.promotionalInternationalRoamEndDate, 0];
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalInternationalRoamIndicator == true){
      let fields = [this.plan.promotionalInternationalRoamDetails, this.plan.promotionalInternationalRoamStartDate, this.plan.promotionalInternationalRoamEndDate, 0];
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalFrequentFlyerIndicator == true){
      let fields = [this.plan.promotionalFrequentFlyerDetails, this.plan.promotionalFrequentFlyerStartDate, this.plan.promotionalFrequentFlyerEndDate, 0];
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalOtherIndicator == true){
      let fields = [this.plan.promotionalOtherDescription, this.plan.promotionalOtherDetails,this.plan.promotionalOtherStartDate, this.plan.promotionalOtherEndDate];
      parentFields.push({dependentFields: fields});
    }
    if (this.plan.promotionalContentIndicator == true){
      let fields: any;
      fields = [this.sportsPromotion, this.musicPromotion, this.svodtvPromotion, this.otherContentPromotion, 0];
      if(this.sportsPromotion=='Yes'){
        fields.push(this.plan.promotionalContentSport, this.plan.promotionalContentSportStartDate, this.plan.promotionalContentSportEndDate);
      }
      if(this.musicPromotion =='Yes'){
        fields.push(this.plan.promotionalContentMusic, this.plan.promotionalContentMusicStartDate, this.plan.promotionalContentMusicEndDate)
      }
      if(this.svodtvPromotion == 'Yes'){
        fields.push(this.plan.promotionalContentSvod, this.plan.promotionalContentSvodStartDate, this.plan.promotionalContentSvodEndDate)
      }
      if(this.otherContentPromotion == 'Yes'){
        fields.push(this.plan.promotionalContentOtherDetails, this.plan.promotionalContentOtherStartDate, this.plan.promotionalContentOtherEndDate)
      }
      parentFields.push({dependentFields: fields});
    }

    this.plan.disableTab7 = (parentFields.length > 0) ? this.validateDependent(parentFields) : false;
  }

  setDateFields(){   // Set the required Date fields
    if (this.plan) { // && this.editMode == "Edit" ) { //&& !this.initialised) {

      this.promoMonthlyFeeStartDate = this.plan.promotionalFeeStartDate ? this.returnDateStruct(this.plan.promotionalFeeStartDate) : null;
      this.promoMonthlyFeeEndDate = this.plan.promotionalFeeEndDate ? this.returnDateStruct(this.plan.promotionalFeeEndDate) : null;
      this.promotionalFeeUntilWithdrawn = this.plan.promotionalFeeEndDate ? (this.plan.promotionalFeeEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.studentMonthlyFeeStartDate = this.plan.promotionalStudentDiscountStartDate ? this.returnDateStruct(this.plan.promotionalStudentDiscountStartDate) : null;
      this.studentMonthlyFeeEndDate = this.plan.promotionalStudentDiscountEndDate ? this.returnDateStruct(this.plan.promotionalStudentDiscountEndDate) : null;
      this.studentFeeUntilWithdrawn = this.plan.promotionalStudentDiscountEndDate ? (this.plan.promotionalStudentDiscountEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.feeFreeStart = this.plan.promotionalFeeFreeMonthsStartDate ? this.returnDateStruct(this.plan.promotionalFeeFreeMonthsStartDate) : null;
      this.feeFreeEnd = this.plan.promotionalFeeFreeMonthsEndDate ? this.returnDateStruct(this.plan.promotionalFeeFreeMonthsEndDate) : null;
      this.feeFeeUntilWithdrawn = this.plan.promotionalFeeFreeMonthsEndDate ? (this.plan.promotionalFeeFreeMonthsEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.bonusData1Start = this.plan.promotionalBonusData1StartDate ? this.returnDateStruct(this.plan.promotionalBonusData1StartDate) : null;
      this.bonusData1End = this.plan.promotionalBonusData1EndDate ? this.returnDateStruct(this.plan.promotionalBonusData1EndDate) : null;
      this.promotionalPrepaidBonusCreditUntilWithdrawn = this.plan.promotionalBonusData1EndDate ? (this.plan.promotionalBonusData1EndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.bonusData2Start = this.plan.promotionalBonusData2StartDate ? this.returnDateStruct(this.plan.promotionalBonusData2StartDate) : null;
      this.bonusData2End = this.plan.promotionalBonusData2EndDate ? this.returnDateStruct(this.plan.promotionalBonusData2EndDate) : null;
      this.bonusData2UntilWithdrawn = this.plan.promotionalBonusData2EndDate ? (this.plan.promotionalBonusData2EndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.domesticVoicePromotionStart = this.plan.promotionalDomesticVoiceStartDate ? this.returnDateStruct(this.plan.promotionalDomesticVoiceStartDate) : null;
      this.domesticVoicePromotionEnd = this.plan.promotionalDomesticVoiceEndDate ? this.returnDateStruct(this.plan.promotionalDomesticVoiceEndDate) : null;
      this.domesticVoiceUntilWithdrawn = this.plan.promotionalDomesticVoiceEndDate ? (this.plan.promotionalDomesticVoiceEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.domesticTextPromotionStart = this.plan.promotionalDomesticTextStartDate ? this.returnDateStruct(this.plan.promotionalDomesticTextStartDate) : null;
      this.domesticTextPromotionEnd = this.plan.promotionalDomesticTextEndDate ? this.returnDateStruct(this.plan.promotionalDomesticTextEndDate) : null;
      this.domesticTextUntilWithdrawn = this.plan.promotionalDomesticTextEndDate ? (this.plan.promotionalDomesticTextEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.internationalVoicePromotionStart = this.plan.promotionalInternationalVoiceStartDate ? this.returnDateStruct(this.plan.promotionalInternationalVoiceStartDate) : null;
      this.internationalVoicePromotionEnd = this.plan.promotionalInternationalVoiceEndDate ? this.returnDateStruct(this.plan.promotionalInternationalVoiceEndDate) : null;
      this.internationalVoiceeUntilWithdrawn = this.plan.promotionalInternationalVoiceEndDate ? (this.plan.promotionalInternationalVoiceEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.internationalTextPromotionStart = this.plan.promotionalInternationalTextStartDate ? this.returnDateStruct(this.plan.promotionalInternationalTextStartDate) : null;
      this.internationalTextPromotionEnd = this.plan.promotionalInternationalTextEndDate ? this.returnDateStruct(this.plan.promotionalInternationalTextEndDate) : null;
      this.internationalTexteUntilWithdrawn = this.plan.promotionalInternationalTextEndDate ? (this.plan.promotionalInternationalTextEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.internationalRoamingPromotionStart = this.plan.promotionalInternationalRoamStartDate ? this.returnDateStruct(this.plan.promotionalInternationalRoamStartDate) : null;
      this.internationalRoamingPromotionEnd = this.plan.promotionalInternationalRoamEndDate ? this.returnDateStruct(this.plan.promotionalInternationalRoamEndDate) : null;
      this.promotionalInternationalRoamingUntilWithdrawn = this.plan.promotionalInternationalRoamEndDate ? (this.plan.promotionalInternationalRoamEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.frequentFlyerPromotionStart = this.plan.promotionalFrequentFlyerStartDate ? this.returnDateStruct(this.plan.promotionalFrequentFlyerStartDate) : null;
      this.frequentFlyerPromotionEnd = this.plan.promotionalFrequentFlyerEndDate ? this.returnDateStruct(this.plan.promotionalFrequentFlyerEndDate) : null;
      this.promotionalFrequentFlyerUntilWithdrawn = this.plan.promotionalFrequentFlyerEndDate ? (this.plan.promotionalFrequentFlyerEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.sportsPromotionStart = this.plan.promotionalContentSportStartDate ? this.returnDateStruct(this.plan.promotionalContentSportStartDate) : null;
      this.sportsPromotionEnd = this.plan.promotionalContentSportEndDate ? this.returnDateStruct(this.plan.promotionalContentSportEndDate) : null;
      this.promotionalSportsUntilWithdrawn = this.plan.promotionalContentSportEndDate ? (this.plan.promotionalContentSportEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.musicPromotionStart = this.plan.promotionalContentMusicStartDate ? this.returnDateStruct(this.plan.promotionalContentMusicStartDate) : null;
      this.musicPromotionEnd = this.plan.promotionalContentMusicEndDate ? this.returnDateStruct(this.plan.promotionalContentMusicEndDate) : null;
      this.promotionalMusicUntilWithdrawn = this.plan.promotionalContentMusicEndDate ? (this.plan.promotionalContentMusicEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.svodtvPromotionStart = this.plan.promotionalContentSvodStartDate ? this.returnDateStruct(this.plan.promotionalContentSvodStartDate) : null;
      this.svodtvPromotionEnd = this.plan.promotionalContentSvodEndDate ? this.returnDateStruct(this.plan.promotionalContentSvodEndDate) : null;
      this.promotionalSVODTVUntilWithdrawn = this.plan.promotionalContentSvodEndDate ? (this.plan.promotionalContentSvodEndDate.toString() == "9999-12-31T00:00:00" ? true : false): false;
      this.otherContentPromotionStart = this.plan.promotionalContentOtherStartDate ? this.returnDateStruct(this.plan.promotionalContentOtherStartDate) : null;
      this.otherContentPromotionEnd = this.plan.promotionalContentOtherEndDate ? this.returnDateStruct(this.plan.promotionalContentOtherEndDate) : null;
      this.promotionalOtherContentUntilWithdrawn = this.plan.promotionalContentOtherEndDate ? (this.plan.promotionalContentOtherEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.otherPromotionStart = this.plan.promotionalOtherStartDate ? this.returnDateStruct(this.plan.promotionalOtherStartDate) : null;
      this.otherPromotionEnd = this.plan.promotionalOtherEndDate ? this.returnDateStruct(this.plan.promotionalOtherEndDate) : null;
      this.otherPromotionUntilWithdrawn = this.plan.promotionalOtherEndDate ? (this.plan.promotionalOtherEndDate.toString() == "9999-12-31T00:00:00" ? true : false) : false;
      this.promotionalPrepaidInternationalBonusCreditsStart = this.plan.promotionalPrepaidInternationalBonusCreditsStart ? this.returnDateStruct(this.plan.promotionalPrepaidInternationalBonusCreditsStart) : null;
      this.promotionalPrepaidInternationalBonusCreditsEnd = this.plan.promotionalPrepaidInternationalBonusCreditsEnd ? this.returnDateStruct(this.plan.promotionalPrepaidInternationalBonusCreditsEnd) : null;
      this.promotionalPrepaidInternationalBonusCreditUntilWithdrawn = this.plan.promotionalPrepaidInternationalBonusCreditsEnd ? (this.plan.promotionalPrepaidInternationalBonusCreditsEnd.toString() == "9999-12-31T00:00:00" ? true : false) : false;

      this.bonusPrepaidCreditDataStart = this.plan.promotionalPrepaidBonusCreditStart ? this.returnDateStruct(this.plan.promotionalPrepaidBonusCreditStart) : null;
      this.bonusPrepaidCreditDataEnd = this.plan.promotionalPrepaidBonusCreditEnd ? this.returnDateStruct(this.plan.promotionalPrepaidBonusCreditEnd) : null;
      this.promotionalPrepaidInternationalBonusCreditUntilWithdrawn = this.plan.promotionalPrepaidBonusCreditEnd ? (this.plan.promotionalPrepaidBonusCreditEnd.toString() == "9999-12-31T00:00:00" ? true : false) : false;

      this.initialised = true;
    }
  }

  preventKeyStrokes(event) {
    let charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  private getHighDate(inputCheckBox: boolean) {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    console.log(this.deviceInfo["browser"]);
    if (this.deviceInfo["browser"] == 'ie' || this.deviceInfo["browser"] == 'ms-edge') {
      return inputCheckBox ? this.highDate : null;
    }
    else {
      return !inputCheckBox ? this.highDate : null;
    }
  }

  private returnDateStruct(datein: any) {
    let newDate = new Date(datein);
    return { date: { year: newDate.getFullYear(), month: (newDate.getMonth() + 1), day: newDate.getDate() } };
  }

  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su',
    width: '250px'
  };

  private formatDate(event) {
    if(event)
      return event.jsdate.toDateString(); //.getFullYear() + '-' + (event.jsdate.getMonth() + 1) + '-' + event.jsdate.getDate();
    return null;
  }

  feeDiscountClear() {
    this.plan.promotionalFeeDiscountType = null;
    this.plan.promotionalFeeDiscountValue = null;
    this.plan.promotionalFee = null;
    this.plan.promotionalFeeChannel = null;
    this.plan.promotionalFeeDetails = null;
    this.plan.promotionalFeeStartDate = null;
    this.plan.promotionalFeeEndDate = null;
  }

  studentDiscountClear() {
    this.plan.promotionalStudentDiscountType = null;
    this.plan.promotionalStudentDiscountValue = null;
    this.plan.promotionalStudentDiscountDetails = null;
    this.plan.promotionalStudentDiscountStartDate = null;
    this.plan.promotionalStudentDiscountEndDate = null;
  }

  feeFreeClear() {
    this.plan.promotionalFeeFreeMonthsAmount = null;
    this.plan.promotionalFeeFreeMonthsChannel = null;
    this.plan.promotionalFeeFreeMonthsDetails = null;
    this.plan.promotionalFeeFreeMonthsStartDate = null;
    this.plan.promotionalFeeFreeMonthsEndDate = null;
  }

  prepaidBonusCreditClear() {
    this.plan.promotionalPrepaidBonusCreditValue = null;
    this.plan.promotionalPrepaidBonusCreditDetails = null;
    this.plan.promotionalPrepaidBonusCreditStart = null;
    this.plan.promotionalPrepaidBonusCreditEnd = null;
  }

  bonusData1Clear() {
    this.plan.promotionalBonusData1Amount = null;
    this.plan.promotionalBonusData1Channel = null;
    this.plan.promotionalBonusData1Details = null;
    this.plan.promotionalBonusData1StartDate = null;
    this.plan.promotionalBonusData1EndDate = null;
  }

  bonusData2Clear() {
    this.plan.promotionalBonusData2Amount = null;
    this.plan.promotionalBonusData2Channel = null;
    this.plan.promotionalBonusData2Details = null;
    this.plan.promotionalBonusData2StartDate = null;
    this.plan.promotionalBonusData2EndDate = null;
  }

  domesticVoiceClear() {
    this.plan.promotionalDomesticVoiceType = null;
    this.plan.promotionalDomesticVoiceValue = null;
    this.plan.promotionalDomesticVoiceStartDate = null;
    this.plan.promotionalDomesticVoiceEndDate = null;
    this.plan.promotionalDomesticVoiceDetails = null;
  }

  domesticTextClear() {
    this.plan.promotionalDomesticTextType = null;
    this.plan.promotionalDomesticTextValue = null;
    this.plan.promotionalDomesticTextStartDate = null;
    this.plan.promotionalDomesticTextEndDate = null;
    this.plan.promotionalDomesticTextDetails = null;
  }

  internationalVoiceClear() {
    this.plan.promotionalInternationalVoiceType = null;
    this.plan.promotionalInternationalVoiceValue = null;
    this.plan.promotionalInternationalVoiceCountryAmount = null;
    this.plan.promotionalInternationalVoiceCountryList = null;
    this.plan.promotionalInternationalVoiceStartDate = null;
    this.plan.promotionalInternationalVoiceDetails = null;
    this.plan.promotionalInternationalVoiceEndDate = null;
  }

  internationalTextClear() {
    this.plan.promotionalInternationalTextType = null;
    this.plan.promotionalInternationalTextValue = null;
    this.plan.promotionalInternationalTextStartDate = null;
    this.plan.promotionalInternationalTextEndDate = null;
    this.plan.promotionalInternationalTextDetails = null;
  }

  prepaidInternationalBonusCreditClear() {
    this.plan.promotionalPrepaidInternationalBonusCreditsValue = null;
    this.plan.promotionalPrepaidInternationalBonusCreditsDetails = null;
    this.plan.promotionalPrepaidInternationalBonusCreditsStart = null;
    this.plan.promotionalPrepaidInternationalBonusCreditsEnd = null;
  }

  internationalRoamingClear() {
    this.plan.promotionalInternationalRoamDetails = null;
    this.plan.promotionalInternationalRoamStartDate = null;
    this.plan.promotionalInternationalRoamEndDate = null;
  }

  frequentFlyerClear() {
    this.plan.promotionalFrequentFlyerDetails = null;
    this.plan.promotionalFrequentFlyerStartDate = null;
    this.plan.promotionalFrequentFlyerEndDate = null;
  }

  clearPromotionalContent() {
    this.contentSportClear();
    this.contentMusicClear();
    this.contentSVODClear();
    this.contentOtherClear();
  }

  contentSportClear() {
    this.plan.promotionalContentSport = null;
    this.plan.promotionalContentSportStartDate = null;
    this.plan.promotionalContentSportEndDate = null;
  }

  contentMusicClear() {
    this.plan.promotionalContentMusic = null;
    this.plan.promotionalContentMusicStartDate = null;
    this.plan.promotionalContentMusicEndDate = null;
  }

  contentSVODClear() {
    this.plan.promotionalContentSvod = null;
    this.plan.promotionalContentSvodStartDate = null;
    this.plan.promotionalContentSvodEndDate = null;
  }

  contentOtherClear() {
    this.plan.promotionalContentOtherDetails = null;
    this.plan.promotionalContentOtherStartDate = null;
    this.plan.promotionalContentOtherEndDate = null;
  }

  otherClear()
  {
    this.plan.promotionalOtherDescription = null;
    this.plan.promotionalOtherDetails = null;
    this.plan.promotionalOtherStartDate = null;
  }
  preventEnter(event) {
    let charCode = (event.which) ? event.which : event.keyCode;

    if (charCode == 13)
      return false;
    return true;
  }

}
