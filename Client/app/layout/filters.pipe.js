var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Pipe } from '@angular/core';
import * as _ from 'lodash';
var MatchesProductCategoryPipe = /** @class */ (function () {
    function MatchesProductCategoryPipe() {
    }
    MatchesProductCategoryPipe.prototype.transform = function (items, productCategoryId) {
        if (!items || !productCategoryId || productCategoryId == '0') {
            return items;
        }
        return items.filter(function (item) { return item.productCategoryId.toString() === productCategoryId; });
    };
    MatchesProductCategoryPipe = __decorate([
        Pipe({
            name: 'matchesProductCategoryId'
        })
    ], MatchesProductCategoryPipe);
    return MatchesProductCategoryPipe;
}());
export { MatchesProductCategoryPipe };
var MatchesProductCategoryBrandPipe = /** @class */ (function () {
    function MatchesProductCategoryBrandPipe() {
    }
    MatchesProductCategoryBrandPipe.prototype.transform = function (items, productCategoryId, brandId) {
        items = items.filter(function (item) { return (item.productCategoryId.toString() === productCategoryId); });
        items = items.filter(function (item) { return (item.brandId != null); });
        items = items.filter(function (item) { return (item.brandId.toString() === brandId); });
        console.log("testing:: ", items);
        return items;
    };
    MatchesProductCategoryBrandPipe = __decorate([
        Pipe({
            name: 'matchesProductCategoryBrandId'
        })
    ], MatchesProductCategoryBrandPipe);
    return MatchesProductCategoryBrandPipe;
}());
export { MatchesProductCategoryBrandPipe };
var MatchesProductTypeCategoryPipe = /** @class */ (function () {
    function MatchesProductTypeCategoryPipe() {
    }
    MatchesProductTypeCategoryPipe.prototype.transform = function (items, productTypeCategoryId) {
        if (!items || !productTypeCategoryId || productTypeCategoryId == '0') {
            return items;
        }
        return items.filter(function (item) { return item.productTypeCategoryId.toString() === productTypeCategoryId; });
    };
    MatchesProductTypeCategoryPipe = __decorate([
        Pipe({
            name: 'matchesProductTypeCategoryId'
        })
    ], MatchesProductTypeCategoryPipe);
    return MatchesProductTypeCategoryPipe;
}());
export { MatchesProductTypeCategoryPipe };
var MatchesUnitPipe = /** @class */ (function () {
    function MatchesUnitPipe() {
    }
    MatchesUnitPipe.prototype.transform = function (items, unit) {
        if (!items || !unit || unit == 'All') {
            return items;
        }
        return items.filter(function (item) { return item.unit === unit; });
    };
    MatchesUnitPipe = __decorate([
        Pipe({
            name: 'matchesUnit'
        })
    ], MatchesUnitPipe);
    return MatchesUnitPipe;
}());
export { MatchesUnitPipe };
var UniqueListPipe = /** @class */ (function () {
    function UniqueListPipe() {
    }
    UniqueListPipe.prototype.transform = function (items, filterItem) {
        return _.uniqBy(items, filterItem);
    };
    UniqueListPipe = __decorate([
        Pipe({
            name: 'uniqueList'
        })
    ], UniqueListPipe);
    return UniqueListPipe;
}());
export { UniqueListPipe };
var OrderBy = /** @class */ (function () {
    function OrderBy() {
    }
    OrderBy_1 = OrderBy;
    OrderBy._orderByComparator = function (a, b) {
        if ((isNaN(parseFloat(a)) || !isFinite(a)) || (isNaN(parseFloat(b)) || !isFinite(b))) {
            //Isn't a number so lowercase the string to properly compare
            if (a.toLowerCase() < b.toLowerCase())
                return -1;
            if (a.toLowerCase() > b.toLowerCase())
                return 1;
        }
        else {
            //Parse strings as numbers to compare properly
            if (parseFloat(a) < parseFloat(b))
                return -1;
            if (parseFloat(a) > parseFloat(b))
                return 1;
        }
        return 0; //equal each other
    };
    OrderBy.prototype.transform = function (input, _a) {
        var _b = _a[0], config = _b === void 0 ? '+' : _b;
        console.log("input", input);
        if (!Array.isArray(input))
            return input;
        if (!Array.isArray(config) || (Array.isArray(config) && config.length == 1)) {
            var propertyToCheck = !Array.isArray(config) ? config : config[0];
            var desc = propertyToCheck.substr(0, 1) == '-';
            //Basic array
            if (!propertyToCheck || propertyToCheck == '-' || propertyToCheck == '+') {
                return !desc ? input.sort() : input.sort().reverse();
            }
            else {
                var property = propertyToCheck.substr(0, 1) == '+' || propertyToCheck.substr(0, 1) == '-'
                    ? propertyToCheck.substr(1)
                    : propertyToCheck;
                return input.sort(function (a, b) {
                    return !desc
                        ? OrderBy_1._orderByComparator(a[property], b[property])
                        : -OrderBy_1._orderByComparator(a[property], b[property]);
                });
            }
        }
        else {
            //Loop over property of the array in order and sort
            return input.sort(function (a, b) {
                for (var i = 0; i < config.length; i++) {
                    var desc = config[i].substr(0, 1) == '-';
                    var property = config[i].substr(0, 1) == '+' || config[i].substr(0, 1) == '-'
                        ? config[i].substr(1)
                        : config[i];
                    var comparison = !desc
                        ? OrderBy_1._orderByComparator(a[property], b[property])
                        : -OrderBy_1._orderByComparator(a[property], b[property]);
                    //Don't return 0 yet in case of needing to sort by next property
                    if (comparison != 0)
                        return comparison;
                }
                return 0; //equal each other
            });
        }
    };
    OrderBy = OrderBy_1 = __decorate([
        Pipe({ name: 'orderBy', pure: false })
    ], OrderBy);
    return OrderBy;
    var OrderBy_1;
}());
export { OrderBy };
