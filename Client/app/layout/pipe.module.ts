﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchesProductCategoryPipe, MatchesUnitPipe, MatchesProductTypeCategoryPipe, UniqueListPipe, OrderBy, MatchesProductCategoryBrandPipe } from './filters.pipe';

@NgModule({
    declarations: [MatchesProductCategoryPipe, MatchesUnitPipe, MatchesProductTypeCategoryPipe, UniqueListPipe, OrderBy, MatchesProductCategoryBrandPipe ],
    imports: [CommonModule,],
    exports: [MatchesProductCategoryPipe, MatchesUnitPipe, MatchesProductTypeCategoryPipe, UniqueListPipe, OrderBy, MatchesProductCategoryBrandPipe]
})
export class PipeModule { }
