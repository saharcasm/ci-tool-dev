﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'search', loadChildren: './search/search.module#SearchModule' },
            { path: 'plan', loadChildren: './plan/plan.module#PlanModule' },
            { path: 'reporting', loadChildren: './reporting/reporting.module#ReportingModule' },
            { path: 'maintain/product-name', loadChildren: './maintain/product-name/product-name.module#ProductNameModule' },
            { path: 'maintain/product-type', loadChildren: './maintain/product-type/product-type.module#ProductTypeModule' },
            { path: 'maintain/term', loadChildren: './maintain/term/term.module#TermModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' }
        ],
        
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }