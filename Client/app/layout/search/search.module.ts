﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { PipeModule } from '../pipe.module'

@NgModule({
  imports: [
      CommonModule,
      SearchRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      PipeModule
    ],
  declarations: [SearchComponent]
  
})
export class SearchModule { }
