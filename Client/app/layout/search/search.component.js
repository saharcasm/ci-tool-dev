var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
var SearchComponent = /** @class */ (function () {
    function SearchComponent(http) {
        this.http = http;
    }
    SearchComponent.prototype.searchClick = function () {
        var _this = this;
        // Run the search for Plans
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/api/search?brandid=' + this.selectedBrand + '&producttypeid=' + this.selectedProductType + '&termid=' + this.selectedTerm + '&fee=' + this.selectedFee + '&segment=' + this.selectedSegment + '&productCategoryId=' + this.selectedProductCategoryId + '&unit=' + this.selectedUnit, { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (sr) {
            console.log("sr:: ", sr);
            sr.sort(function (name1, name2) {
                if (name1.brand < name2.brand) {
                    return -1;
                }
                else if (name1.brand > name2.brand) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
            _this.searchResults = sr;
        });
    };
    Object.defineProperty(SearchComponent.prototype, "isFormValid", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    SearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        // Get the brands
        this.http.get('/api/brand/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (b) {
            _this.brands = b;
        });
        // Get Product Types
        this.http.get('/api/producttype/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (pt) {
            _this.productTypes = pt;
        });
        //Get Terms
        this.http.get('/api/term/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (t) {
            _this.terms = t;
        });
        //Default the selects
        this.selectedSegment = "Consumer";
        this.selectedBrand = "0";
        this.selectedFee = "0-9999";
        this.selectedProductCategoryId = "0";
        this.selectedProductType = "0";
        this.selectedTerm = "0";
        this.selectedUnit = "All";
        this.selectedPlanType = "All";
    };
    SearchComponent = __decorate([
        Component({
            selector: 'app-search',
            templateUrl: './search.component.html',
            styleUrls: ['./search.component.scss']
        }),
        __metadata("design:paramtypes", [Http])
    ], SearchComponent);
    return SearchComponent;
}());
export { SearchComponent };
