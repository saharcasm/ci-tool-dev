﻿import { Component, OnInit } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { FormControl, FormGroup } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Brand, ProductType, Term, MobileSearchResult } from "../models";

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
    public brands: Brand[];
    public productTypes: ProductType[];
    public terms: Term[];
    public MBBTerms: Term[];
    public phoneTerms: Term[];
    public searchResults: MobileSearchResult[];
    public selectedSegment: string;
    public selectedBrand: string;
    public selectedProductType: string;
    public selectedTerm: string;
    public selectedFee: string;
    public selectedProductCategoryId: string;
    public selectedUnit: string;
    public selectedPlanType: string;

    constructor(private http: Http) {}

    public searchClick()
    {
        // Run the search for Plans
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        this.http.get('/api/search?brandid=' + this.selectedBrand + '&producttypeid=' + this.selectedProductType + '&termid=' + this.selectedTerm + '&fee=' + this.selectedFee + '&segment=' + this.selectedSegment + '&productCategoryId=' + this.selectedProductCategoryId + '&unit=' + this.selectedUnit, { headers })
            .map(x => x.json())
            .subscribe(sr => {
               
                console.log("testing apisss:: ",'brandid= ' + this.selectedBrand);

                console.log("testing apis:: ",'&productCategoryId= ' + this.selectedProductCategoryId);

                console.log("testing apis:: ",'&selectedSegment= ' + this.selectedSegment);

                console.log("testing apis:: ",'&termid=' + this.selectedTerm);

                console.log("testing apis:: ",'&selectedUnit= ' + this.selectedUnit);
                console.log("testing apis:: ",'&selectedFee= ' + this.selectedFee);
                console.log("testing apis:: ",'&selectedProductType= ' + this.selectedProductType);
                console.log("-------------------------------------------------------------------");
               
                sr.sort( function(name1, name2) {
                    if ( name1.brand < name2.brand ){
                        return -1;
                    }else if( name1.brand > name2.brand ){
                        return 1;
                    }else{
                        return 0;    
                    }
                });
                this.searchResults = sr;
            });
    }

    public get isFormValid(): boolean {
        return true;
    }

    ngOnInit() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        // Get the brands
        this.http.get('/api/brand/active', { headers })
            .map(x => x.json())
            .subscribe(b => {
                this.brands = b;
            });

        // Get Product Types
        this.http.get('/api/producttype/active', { headers })
            .map(x => x.json())
            .subscribe(pt => {
                this.productTypes = pt;
            });

        //Get Terms
        this.http.get('/api/term/active', { headers })
            .map(x => x.json())
            .subscribe(t => {
                this.terms = t;
            });

        //Default the selects
        this.selectedSegment = "Consumer";        
        this.selectedBrand = "0";
        this.selectedFee = "0-9999";
        this.selectedProductCategoryId = "0";
        this.selectedProductType = "0";
        this.selectedTerm = "0";
        this.selectedUnit = "All";
        this.selectedPlanType = "All";
    }

}
