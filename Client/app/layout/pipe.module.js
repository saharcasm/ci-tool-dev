var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchesProductCategoryPipe, MatchesUnitPipe, MatchesProductTypeCategoryPipe, UniqueListPipe, OrderBy, MatchesProductCategoryBrandPipe } from './filters.pipe';
var PipeModule = /** @class */ (function () {
    function PipeModule() {
    }
    PipeModule = __decorate([
        NgModule({
            declarations: [MatchesProductCategoryPipe, MatchesUnitPipe, MatchesProductTypeCategoryPipe, UniqueListPipe, OrderBy, MatchesProductCategoryBrandPipe],
            imports: [CommonModule,],
            exports: [MatchesProductCategoryPipe, MatchesUnitPipe, MatchesProductTypeCategoryPipe, UniqueListPipe, OrderBy, MatchesProductCategoryBrandPipe]
        })
    ], PipeModule);
    return PipeModule;
}());
export { PipeModule };
