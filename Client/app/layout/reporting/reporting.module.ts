﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReportingRoutingModule } from './reporting-routing.module';
import { FormsModule } from '@angular/forms';
import { ReportingComponent } from './reporting.component';
import { UpdatesComponent } from './updates.component';
import { MyDatePickerModule } from 'mydatepicker';
import { PipeModule } from '../pipe.module';

@NgModule({
  imports: [
    CommonModule,
      ReportingRoutingModule,
      NgbModule.forRoot(),
      FormsModule,
      MyDatePickerModule,
      PipeModule
  ],
  declarations: [ReportingComponent, UpdatesComponent]
})
export class ReportingModule { }

