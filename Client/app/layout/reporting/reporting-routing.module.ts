﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportingComponent } from './reporting.component';
import { UpdatesComponent } from './updates.component';

const routes: Routes = [
    { path: '', component: ReportingComponent, pathMatch: 'full'},
    { path: 'updates', component: UpdatesComponent }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReportingRoutingModule { }
