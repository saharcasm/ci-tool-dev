var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
var ReportingComponent = /** @class */ (function () {
    function ReportingComponent(http) {
        this.http = http;
        this.myDatePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su',
            width: '250px'
        };
    }
    ReportingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.AMPM = "AM"; // Set the default value
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // Get the brands
        this.http.get('/api/brand/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (b) {
            _this.brands = b;
        });
        // Get Product Types
        this.http.get('/api/producttype/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (pt) {
            _this.productTypes = pt;
        });
        //Get Terms
        this.http.get('/api/term/active', { headers: headers })
            .map(function (x) { return x.json(); })
            .subscribe(function (t) {
            _this.terms = t;
        });
        //Default the selects
        this.selectedSegment = "Consumer";
        this.selectedBrand = "All";
        this.selectedFee = "0-9999";
        this.selectedProductCategoryId1 = "0";
        this.selectedProductCategoryId2 = "0";
        this.selectedProductCategoryId3 = "0";
        this.selectedProductType = "0";
        this.selectedTerm = "All";
        this.selectedUnit = "All";
        this.selectedPlanType = "All";
        this.selectPlanStatus = "All";
    };
    ReportingComponent.prototype.setPeriod = function (value) {
        this.AMPM = value;
    };
    ReportingComponent.prototype.downloadWasNow = function () {
        var _this = this;
        // Run the search for Plans
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('/api/report/wasnow?period=' + this.AMPM + '&productCategoryId=' + this.selectedProductCategoryId1, { headers: headers })
            .map(function (x) { return x.text(); })
            .subscribe(function (data) {
            _this.reorderDocs(data);
            // this.downloadFile(data, 'WasNowReport_' + this.AMPM);
        });
        this.http.get('/api/report/NewPlan?period=' + this.AMPM, { headers: headers })
            .map(function (x) { return x.text(); })
            .subscribe(function (data) {
            _this.downloadFile(data, 'NewPlanReport_' + _this.AMPM);
        });
    };
    ReportingComponent.prototype.sortByBrand = function (docs) {
        var reachedEnd = false;
        var count = docs.length / 2;
        var removeDoc;
        var sortedDocs = [];
        while (count != 0) {
            if (docs.length > 2) {
                var smallest = docs[0].Brand;
                console.log("all docs: ", docs);
                for (var i = 0; i < docs.length; i += 2) {
                    if (docs[i + 2].Brand < docs[i].Brand) {
                        removeDoc = i;
                        console.log(docs[i].Brand, i);
                    }
                }
                docs.splice(removeDoc, 2);
                sortedDocs.push(docs[removeDoc].Brand);
                sortedDocs.push(docs[removeDoc + 1]);
            }
            // else if(docs.length==2){
            //     removeDoc=0;
            //     sortedDocs.push(docs[removeDoc]);
            //     sortedDocs.push(docs[removeDoc+1]);
            // }
            count--;
        }
        // console.log(sortedDocs);
        return sortedDocs;
    };
    ReportingComponent.prototype.reorderDocs = function (data) {
        var docs = JSON.parse(data);
        // console.log(docs);
        // docs=this.sortByBrand(docs);
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        var hh = today.getHours();
        var min = today.getMinutes();
        var timestamp = yyyy + "_" + mm + "_" + dd + "_" + hh + "_" + min;
        var csvDocs = [];
        var space = {
            'space': "",
        };
        var index = 0;
        // for(var index=0; index<docs.length; index+=2)
        console.log(docs);
        while (index < docs.length) {
            var mergedDoc = Object.assign({}, docs[index], docs[index + 1]);
            var nowDoc = [];
            var wasDoc = [];
            delete mergedDoc['Unit'];
            var mergedProperties = Object.getOwnPropertyNames(mergedDoc);
            for (var property in mergedDoc) {
                nowDoc[property] = docs[index][property] || "";
                if (docs[index + 1]) {
                    if (docs[index + 1].Rank == "WAS") {
                        wasDoc[property] = docs[index + 1][property] || "";
                    }
                }
            }
            mergedProperties[10] = "STD_Fee";
            csvDocs.push(mergedProperties);
            csvDocs.push(nowDoc);
            if (wasDoc.hasOwnProperty('Rank')) {
                console.log("testing here::", wasDoc);
                csvDocs.push(wasDoc);
                index += 2;
            }
            else {
                index++;
            }
            csvDocs.push({});
        }
        var options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: false,
            showTitle: false
        };
        new Angular2Csv(csvDocs, 'WasNowReport_' + this.AMPM + "_" + timestamp + ".csv", options);
    };
    ReportingComponent.prototype.downloadOfferExpiry = function () {
        var _this = this;
        var startDate;
        var endDate;
        startDate = this.fromDate.jsdate.toDateString();
        endDate = this.toDate.jsdate.toDateString();
        // Run the search for Plans
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('api/report/OfferExpiry?startDate=' + startDate + '&endDate=' + endDate + '&productCategoryId=' + this.selectedProductCategoryId2, { headers: headers })
            .map(function (x) { return x.text(); })
            .subscribe(function (data) {
            //console.log(data);
            _this.downloadFile(data, 'OfferExpiryReport');
        });
    };
    ReportingComponent.prototype.downloadFullExtract = function () {
        var _this = this;
        var startDate;
        var endDate;
        var params;
        var feeStart;
        var feeEnd;
        var productCategory;
        startDate = this.extractFromDate ? this.extractFromDate.jsdate.toDateString() : '9999-12-31';
        endDate = this.extractToDate ? this.extractToDate.jsdate.toDateString() : '9999-12-31';
        switch (this.selectedProductCategoryId3) {
            case '1':
                productCategory = 'Postpaid';
                break;
            case '2':
                productCategory = 'Prepaid';
                break;
            default:
                productCategory = 'All';
        }
        params = "startDate=" + startDate
            + "&endDate=" + endDate
            + "&segment=" + this.selectedSegment
            + "&brand=" + this.selectedBrand
            + "&plantype=" + this.selectedPlanType
            + "&productType=" + this.selectedProductType
            + "&term=" + this.selectedTerm
            + "&standardFee=" + this.selectedFee
            + "&productstatus=" + this.selectPlanStatus
            + '&productCategory=' + this.selectedProductCategoryId3;
        // Run the search for Plans
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');
        this.http.get('api/report/FullExtract?' + params, { headers: headers })
            .map(function (x) { return x.text(); })
            .subscribe(function (data) {
            //console.log(data);
            _this.downloadFile(data, 'CompleteExtract');
        });
    };
    //Download file as delimited extract to .csv extension
    ReportingComponent.prototype.downloadFile = function (data, filename) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        var hh = today.getHours();
        var min = today.getMinutes();
        var timestamp = yyyy + "_" + mm + "_" + dd + "_" + hh + "_" + min;
        var blob = new Blob([data], { type: 'text/csv' });
        if (window.navigator.msSaveOrOpenBlob)
            window.navigator.msSaveBlob(blob, filename + "_" + timestamp + ".csv");
        else {
            var a = window.document.createElement("a");
            a.href = window.URL.createObjectURL(blob);
            a.download = filename + "_" + timestamp + ".csv";
            document.body.appendChild(a);
            a.click(); // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
            document.body.removeChild(a);
        }
    };
    ReportingComponent = __decorate([
        Component({
            selector: 'app-reporting',
            templateUrl: './reporting.component.html',
            styleUrls: ['./reporting.component.scss'],
        }),
        __metadata("design:paramtypes", [Http])
    ], ReportingComponent);
    return ReportingComponent;
}());
export { ReportingComponent };
