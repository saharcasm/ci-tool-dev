﻿import { Component, OnInit } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import 'rxjs/Rx';
import { Brand, ProductType, Term, SearchResult } from "../models";
import { IMyDpOptions } from 'mydatepicker';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';



@Component({
    selector: 'app-reporting',
    templateUrl: './reporting.component.html',
    styleUrls: ['./reporting.component.scss'],
})
export class ReportingComponent implements OnInit {
    public brands: Brand[];
    public productTypes: ProductType[];
    public terms: Term[];
    public AMPM: string;
    public fromDate: any;
    public toDate: any;
    public extractFromDate: any;
    public extractToDate: any;
    public selectedSegment: string;
    public selectedBrand: string;
    public selectedProductType: string;
    public selectedTerm: string;
    public selectedFee: string;
    public selectPlanStatus: string;
    public selectedPlanType: string;
    public selectedProductCategoryId1: string;
    public selectedProductCategoryId2: string;
    public selectedProductCategoryId3: string;
    public selectedUnit: string;
    

    constructor(private http: Http) { }
    ngOnInit() {

        this.AMPM = "AM";  // Set the default value
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        // Get the brands
        this.http.get('/api/brand/active', { headers })
        .map(x => x.json())
        .subscribe(b => {
            this.brands = b;
        });

        // Get Product Types
        this.http.get('/api/producttype/active', { headers })
        .map(x => x.json())
        .subscribe(pt => {
            this.productTypes = pt;
        });

        //Get Terms
        this.http.get('/api/term/active', { headers })
        .map(x => x.json())
        .subscribe(t => {
            this.terms = t;
        });

        //Default the selects
        this.selectedSegment = "Consumer";
        this.selectedBrand = "All";
        this.selectedFee = "0-9999";
        this.selectedProductCategoryId1 = "0";
        this.selectedProductCategoryId2 = "0";
        this.selectedProductCategoryId3 = "0";
        this.selectedProductType = "0";
        this.selectedTerm = "All";
        this.selectedUnit = "All";
        this.selectedPlanType = "All";
        this.selectPlanStatus = "All";
    }

    public myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'dd/mm/yyyy',
        firstDayOfWeek: 'su',
        width: '250px'
    };

    public setPeriod(value) {
        this.AMPM = value;
    }

    public downloadWasNow() {
        // Run the search for Plans
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        


        this.http.get('/api/report/wasnow?period=' + this.AMPM + '&productCategoryId=' + this.selectedProductCategoryId1, { headers })
        .map(x => x.text())
        .subscribe(data => {
            this.reorderDocs(data);
            // this.downloadFile(data, 'WasNowReport_' + this.AMPM);
        });
        this.http.get('/api/report/NewPlan?period=' + this.AMPM, { headers })
        .map(x => x.text())
        .subscribe(data => {
            this.downloadFile(data, 'NewPlanReport_' + this.AMPM);
        });
    }
    public sortByBrand(docs){
        let reachedEnd=false;
        let count=docs.length/2;
        let removeDoc;
        let sortedDocs=[];

        while(count!=0){
            if(docs.length>2){
                let smallest=docs[0].Brand;
                console.log("all docs: ", docs);
                for(let i=0; i<docs.length; i+=2){
                    if(docs[i+2].Brand<docs[i].Brand){
                        removeDoc=i;
                        console.log(docs[i].Brand, i);
                    }
                }
                docs.splice(removeDoc, 2);
                sortedDocs.push(docs[removeDoc].Brand);
                sortedDocs.push(docs[removeDoc+1]);

            }
            // else if(docs.length==2){
            //     removeDoc=0;
            //     sortedDocs.push(docs[removeDoc]);
            //     sortedDocs.push(docs[removeDoc+1]);

            // }


            count--;
        }
        // console.log(sortedDocs);
        return sortedDocs;
    }
    
    public reorderDocs(data){
        let docs=JSON.parse(data);
        // console.log(docs);
        // docs=this.sortByBrand(docs);
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        var hh = today.getHours();
        var min = today.getMinutes();
        var timestamp = yyyy + "_" + mm + "_" + dd + "_" + hh + "_" + min;

        var csvDocs=[];
        let space={
            'space': "",
        }
        let index=0;

        // for(var index=0; index<docs.length; index+=2)
        console.log(docs);
        while(index<docs.length){

            let mergedDoc = Object.assign({}, docs[index], docs[index+1]);


            let nowDoc=[];
            let wasDoc=[];

            delete mergedDoc['Unit'];

            let mergedProperties=Object.getOwnPropertyNames(mergedDoc);
            

            for (var property in mergedDoc) {
                nowDoc[property]=docs[index][property] || "";
                if(docs[index+1]){
                    if(docs[index+1].Rank=="WAS"){
                        wasDoc[property]=docs[index+1][property] || "";
                    }
                }
            }


            mergedProperties[10]="STD_Fee";
            csvDocs.push(mergedProperties);
            csvDocs.push(nowDoc);
            if(wasDoc.hasOwnProperty('Rank')){
                console.log("testing here::", wasDoc);
                csvDocs.push(wasDoc);
                index+=2;
            } 
            else{
                index++;
            }


            csvDocs.push({});


        }

        
        var options = { 
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: false, 
            showTitle: false 
        };

        new Angular2Csv(csvDocs, 'WasNowReport_' + this.AMPM + "_" + timestamp , options);
    }

    public downloadOfferExpiry() {
        var startDate: string;
        var endDate: string;

        startDate = this.fromDate.jsdate.toDateString();
        endDate = this.toDate.jsdate.toDateString();
        // Run the search for Plans
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        this.http.get('api/report/OfferExpiry?startDate=' + startDate + '&endDate=' + endDate + '&productCategoryId=' + this.selectedProductCategoryId2, { headers })
        .map(x => x.text())
        .subscribe(data => {
            //console.log(data);
            this.downloadFile(data, 'OfferExpiryReport');
        });
    }

    public downloadFullExtract() {
        var startDate: string;
        var endDate: string;
        var params: string;
        var feeStart: string;
        var feeEnd: string;
        var productCategory: string;
        startDate = this.extractFromDate ? this.extractFromDate.jsdate.toDateString() : '9999-12-31';
        endDate = this.extractToDate ? this.extractToDate.jsdate.toDateString() : '9999-12-31';

        switch (this.selectedProductCategoryId3) {
            case '1':
            productCategory = 'Postpaid';
            break;
            case '2':
            productCategory = 'Prepaid';
            break;
            default:
            productCategory = 'All';
        } 

        params = "startDate=" + startDate
        + "&endDate=" + endDate
        + "&segment=" + this.selectedSegment
        + "&brand=" + this.selectedBrand
        + "&plantype=" + this.selectedPlanType
        + "&productType=" + this.selectedProductType
        + "&term=" + this.selectedTerm
        + "&standardFee=" + this.selectedFee
        + "&productstatus=" + this.selectPlanStatus
        + '&productCategory=' + this.selectedProductCategoryId3;

        // Run the search for Plans
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        this.http.get('api/report/FullExtract?' + params, { headers })
        .map(x => x.text())
        .subscribe(data => {
            //console.log(data);
            this.downloadFile(data, 'CompleteExtract');
        });

    }

    //Download file as delimited extract to .csv extension
    downloadFile(data: string, filename: string) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        var hh = today.getHours();
        var min = today.getMinutes();
        var timestamp = yyyy + "_" + mm + "_" + dd + "_" + hh + "_" + min;

        var blob = new Blob([data], { type: 'text/csv' });
        if (window.navigator.msSaveOrOpenBlob)  // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
        window.navigator.msSaveBlob(blob, filename + "_" + timestamp + ".csv");
        else {
            var a = window.document.createElement("a");
            a.href = window.URL.createObjectURL(blob);
            a.download = filename + "_" + timestamp + ".csv";
            document.body.appendChild(a);
            a.click();  // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
            document.body.removeChild(a);
        }
    }
}
