import { Component, OnInit } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import 'rxjs/Rx';
import { Brand, ProductType, Term, SearchResult } from "../models";
import { IMyDpOptions } from 'mydatepicker';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';



@Component({
    selector: 'app-reporting',
    templateUrl: './updates.component.html',
    styleUrls: ['./reporting.component.scss'],
})
export class UpdatesComponent implements OnInit {
    public updatedPlans: any[];

    public getUpdatedPlans(){
        console.log("prochnost");
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Cache-Control', 'no-cache');
        headers.append('Pragma', 'no-cache');

        this.http.get('/api/report/gethidden', { headers })
        .map(x => x.text())
        .subscribe(data => {
            this.updatedPlans=JSON.parse(data);
            this.updatedPlans=this.updatedPlans.sort( function(name1, name2) {
                    if ( name1.Brand < name2.Brand ){
                        return -1;
                    }else if( name1.Brand > name2.Brand ){
                        return 1;
                    }else{
                        return 0;    
                    }
                });
            console.log("hidden::", this.updatedPlans);
        });


    }

    public toggleHidden(plan){
    console.log("the plan is:: ", plan.Date_Time_Created)
        let apiString = plan.Product_Category_ID == 1 ? '/api/mobile/togglepostpaid/' : '/api/mobile/toggleprepaid/';
        plan.Hidden= plan.Hidden ? 1 : 0;
        plan.Id=plan.ID;
        
        let headers = new Headers();

        headers.append('Content-Type', 'application/json');

        let body = JSON.stringify(plan);
        console.log('deployed:: ', plan);
        this.http.put(apiString, body, { headers }).subscribe(data => {
        }, error => {
        console.log(error);
 
      });
        return !plan.Hidden;
    }

    constructor(private http: Http) { }
    
    ngOnInit() {
        
        this.getUpdatedPlans();

       
    }

}
