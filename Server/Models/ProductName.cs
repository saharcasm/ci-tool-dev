﻿using System;
using System.Collections.Generic;

namespace CI_Tool_API.Models
{
  public partial class ProductName
  {
    public int ProductNameId { get; set; }
    public string ProductNameName { get; set; }
    public int? ProductCategoryId { get; set; }
    public int? BrandId { get; set; }
    public string Username { get; set; }
    public bool RecordStatus { get; set; }
    public DateTime StartDateTime { get; set; }
    public DateTime? EndDateTime { get; set; }
  }

  public partial class ProductNameSummary
  {
    public int ProductNameId { get; set; }
    public string ProductNameName { get; set; }
    public string ProductCategory { get; set; }
    public int? BrandId { get; set; }
    public string BrandName { get; set; }
    public bool? RecordStatus { get; set; }
  }
}
