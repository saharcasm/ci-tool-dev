﻿using System;
using System.Collections.Generic;

namespace CI_Tool_API.Models
{
    public partial class FrequentFlyerProgram
    {
        public int FrequentFlyerProgramId { get; set; }
        public string FrequentFlyerProgramName { get; set; }
        public string Username { get; set; }
        public bool RecordStatus { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
    }
}
