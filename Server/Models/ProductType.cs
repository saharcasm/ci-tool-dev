﻿using System;

namespace CI_Tool_API.Models
{
  public partial class ProductType
  {
    public int ProductTypeId { get; set; }
    public string ProductTypeName { get; set; }
    public int? ProductCategoryId { get; set; }
    public string Username { get; set; }
    public bool RecordStatus { get; set; }
    public DateTime StartDateTime { get; set; }
    public DateTime? EndDateTime { get; set; }
    public int ProductTypeCategoryId { get; set; }
  }

  public partial class ProductTypeSummary
  {
    public int ProductTypeId { get; set; }
    public string ProductTypeName { get; set; }
    public string ProductTypeCategory { get; set; }
    public string ProductCategory { get; set; }
    public bool RecordStatus { get; set; }
  }
}
