﻿using System;

namespace CI_Tool_API.Models
{
  public partial class SearchResult
  {
    public int PostpaidId { get; set; }
    public int ProductNameId { get; set; }
    public string ProductName { get; set; }
    public string Segment { get; set; }
    public int BrandId { get; set; }
    public string BrandName { get; set; }
    public int ProductTypeId { get; set; }
    public string ProductType { get; set; }
    public int TermId { get; set; }
    public int TermMonths { get; set; }
    public decimal StandardFee { get; set; }
  }
}

