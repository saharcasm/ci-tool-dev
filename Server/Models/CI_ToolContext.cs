﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;


namespace CI_Tool_API.Models
{
  public partial class CI_ToolContext : DbContext
  {
    public virtual DbSet<Brand> Brand { get; set; }
    public virtual DbSet<FrequentFlyerProgram> FrequentFlyerProgram { get; set; }
    public virtual DbSet<ProductCategory> ProductCategory { get; set; }
    public virtual DbSet<ProductName> ProductName { get; set; }
    public virtual DbSet<ProductType> ProductType { get; set; }
    public virtual DbSet<Term> Term { get; set; }
    public virtual DbSet<SearchResult> SearchResult { get; set; }
    public virtual DbSet<ProductNameSummary> ProductNameSummary { get; set; }
    public virtual DbSet<TermSummary> TermSummary { get; set; }
    public virtual DbSet<ProductTypeSummary> ProductTypeSummary { get; set; }
    public virtual DbSet<MobileSearchResult> MobileSearchResult { get; set; }
    public virtual DbSet<MobilePlan> MobilePlan { get; set; }
    
    public CI_ToolContext(DbContextOptions<CI_ToolContext> options) : base(options)
    {
      //any changes to the context options can now be done here
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      //modelBuilder.HasDefaultSchema("DEV");

      modelBuilder.Entity<MobilePlan>(entity =>
      {
        entity.HasKey(e => new { e.Id, e.ProductCategoryId });

        //entity.ToTable("MOBILE_CURRENT_VIEW", "DEV");
        entity.ToTable("MOBILE_CURRENT_VIEW");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.ProductCategoryId).HasColumnName("Product_Category_ID");

        entity.Property(e => e.Brand).HasColumnType("varchar(40)");

        entity.Property(e => e.BrandId).HasColumnName("Brand_ID");

        entity.Property(e => e.BundleDiscount)
            .HasColumnName("Bundle_Discount")
            .HasColumnType("varchar(50)");

        entity.Property(e => e.BundleDiscountDetails)
            .HasColumnName("Bundle_Discount_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.DateTimeCreated)
            .HasColumnName("Date_Time_Created")
            .HasColumnType("datetime");

        entity.Property(e => e.FrequentFlyerIndicator).HasColumnName("Frequent_Flyer_Indicator");

        entity.Property(e => e.FrequentFlyerProgram)
            .HasColumnName("Frequent_Flyer_Program")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.FrequentFlyerProgramDetails)
            .HasColumnName("Frequent_Flyer_Program_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.FrequentFlyerProgramId).HasColumnName("Frequent_Flyer_Program_ID");

        entity.Property(e => e.InternationalRoamAdditionalCost)
            .HasColumnName("International_Roam_Additional_Cost")
            .HasColumnType("decimal");

        entity.Property(e => e.InternationalRoamCountryAmount).HasColumnName("International_Roam_Country_Amount");

        entity.Property(e => e.InternationalRoamCountryList)
            .HasColumnName("International_Roam_Country_List")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.InternationalRoamDetails)
            .HasColumnName("International_Roam_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.InternationalRoamIncludedInPlan).HasColumnName("International_Roam_Included_In_Plan");

        entity.Property(e => e.InternationalRoamIndicator).HasColumnName("International_Roam_Indicator");

        entity.Property(e => e.InternationalTextCountriesAmount).HasColumnName("International_Text_Countries_Amount");

        entity.Property(e => e.InternationalTextCountriesList)
            .HasColumnName("International_Text_Countries_List")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.InternationalTextDetails)
            .HasColumnName("International_Text_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.InternationalTextInclusions)
            .HasColumnName("International_Text_Inclusions")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.InternationalTextValue)
            .HasColumnName("International_Text_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.InternationalVoice1AdditionalCost)
            .HasColumnName("International_Voice_1_Additional_Cost")
            .HasColumnType("decimal");

        entity.Property(e => e.InternationalVoice1CountriesAmount).HasColumnName("International_Voice_1_Countries_Amount");

        entity.Property(e => e.InternationalVoice1CountriesList)
            .HasColumnName("International_Voice_1_Countries_List")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.InternationalVoice1IncludedInPlan).HasColumnName("International_Voice_1_Included_In_Plan");

        entity.Property(e => e.InternationalVoice1Inclusions)
            .HasColumnName("International_Voice_1_Inclusions")
            .HasColumnType("varchar(50)");

        entity.Property(e => e.InternationalVoice1Value)
            .HasColumnName("International_Voice_1_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.InternationalVoice2AdditionalCost)
            .HasColumnName("International_Voice_2_Additional_Cost")
            .HasColumnType("decimal");

        entity.Property(e => e.InternationalVoice2CountriesAmount).HasColumnName("International_Voice_2_Countries_Amount");

        entity.Property(e => e.InternationalVoice2CountriesList)
            .HasColumnName("International_Voice_2_Countries_List")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.InternationalVoice2IncludedInPlan).HasColumnName("International_Voice_2_Included_In_Plan");

        entity.Property(e => e.InternationalVoice2Inclusions)
            .HasColumnName("International_Voice_2_Inclusions")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.InternationalVoice2Value)
            .HasColumnName("International_Voice_2_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.OtherInclusionsAdditionalComments)
            .HasColumnName("Other_Inclusions_Additional_Comments")
            .HasColumnType("varchar(600)");

        entity.Property(e => e.OtherInclusionsBusinessApps)
            .HasColumnName("Other_Inclusions_Business_Apps")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsCloudStorage)
            .HasColumnName("Other_Inclusions_Cloud_Storage")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsDataRollover)
            .HasColumnName("Other_Inclusions_Data_Rollover")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsDataShare)
            .HasColumnName("Other_Inclusions_Data_Share")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsDeviceTradeUp)
            .HasColumnName("Other_Inclusions_Device_TradeUp")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsIndicator).HasColumnName("Other_Inclusions_Indicator");

        entity.Property(e => e.OtherInclusionsNetworkGuarantee)
            .HasColumnName("Other_Inclusions_Network_Guarantee")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsNetworkTrial)
            .HasColumnName("Other_Inclusions_Network_Trial")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsOther)
            .HasColumnName("Other_Inclusions_Other")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsPrepaidAutopay)
            .HasColumnName("Other_Inclusions_Prepaid_Autopay")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsReferFriend)
            .HasColumnName("Other_Inclusions_Refer_Friend")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.OtherInclusionsWifi)
            .HasColumnName("Other_Inclusions_Wifi")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PlaceholderAttribute1)
            .HasColumnName("Placeholder_Attribute_1")
            .HasColumnType("varchar(50)");

        entity.Property(e => e.PlaceholderAttribute10).HasColumnName("Placeholder_Attribute_10");

        entity.Property(e => e.PlaceholderAttribute11).HasColumnName("Placeholder_Attribute_11");

        entity.Property(e => e.PlaceholderAttribute12).HasColumnName("Placeholder_Attribute_12");

        entity.Property(e => e.PlaceholderAttribute13)
            .HasColumnName("Placeholder_Attribute_13")
            .HasColumnType("date");

        entity.Property(e => e.PlaceholderAttribute14)
            .HasColumnName("Placeholder_Attribute_14")
            .HasColumnType("date");

        entity.Property(e => e.PlaceholderAttribute15)
            .HasColumnName("Placeholder_Attribute_15")
            .HasColumnType("date");

        entity.Property(e => e.PlaceholderAttribute16)
            .HasColumnName("Placeholder_Attribute_16")
            .HasColumnType("date");

        entity.Property(e => e.PlaceholderAttribute17)
            .HasColumnName("Placeholder_Attribute_17")
            .HasColumnType("datetime");

        entity.Property(e => e.PlaceholderAttribute18)
            .HasColumnName("Placeholder_Attribute_18")
            .HasColumnType("datetime");

        entity.Property(e => e.PlaceholderAttribute19)
            .HasColumnName("Placeholder_Attribute_19")
            .HasColumnType("datetime");

        entity.Property(e => e.PlaceholderAttribute2)
            .HasColumnName("Placeholder_Attribute_2")
            .HasColumnType("varchar(50)");

        entity.Property(e => e.PlaceholderAttribute20)
            .HasColumnName("Placeholder_Attribute_20")
            .HasColumnType("datetime");

        entity.Property(e => e.PlaceholderAttribute21).HasColumnName("Placeholder_Attribute_21");

        entity.Property(e => e.PlaceholderAttribute22).HasColumnName("Placeholder_Attribute_22");

        entity.Property(e => e.PlaceholderAttribute23).HasColumnName("Placeholder_Attribute_23");

        entity.Property(e => e.PlaceholderAttribute24).HasColumnName("Placeholder_Attribute_24");

        entity.Property(e => e.PlaceholderAttribute3)
            .HasColumnName("Placeholder_Attribute_3")
            .HasColumnType("varchar(50)");

        entity.Property(e => e.PlaceholderAttribute4)
            .HasColumnName("Placeholder_Attribute_4")
            .HasColumnType("varchar(50)");

        entity.Property(e => e.PlaceholderAttribute5)
            .HasColumnName("Placeholder_Attribute_5")
            .HasColumnType("decimal");

        entity.Property(e => e.PlaceholderAttribute6)
            .HasColumnName("Placeholder_Attribute_6")
            .HasColumnType("decimal");

        entity.Property(e => e.PlaceholderAttribute7)
            .HasColumnName("Placeholder_Attribute_7")
            .HasColumnType("decimal");

        entity.Property(e => e.PlaceholderAttribute8)
            .HasColumnName("Placeholder_Attribute_8")
            .HasColumnType("decimal");

        entity.Property(e => e.PlaceholderAttribute9).HasColumnName("Placeholder_Attribute_9");

        entity.Property(e => e.PlanType)
            .HasColumnName("Plan_Type")
            .HasColumnType("varchar(50)");

        entity.Property(e => e.ProductCategory)
            .HasColumnName("Product_Category")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.ProductChangeType)
            .HasColumnName("Product_Change_Type")
            .HasColumnType("varchar(15)");

        entity.Property(e => e.ProductName)
            .HasColumnName("Product_Name")
            .HasColumnType("varchar(100)");

        entity.Property(e => e.ProductNameId).HasColumnName("Product_Name_ID");

        entity.Property(e => e.ProductStatus).HasColumnName("Product_Status");

        entity.Property(e => e.ProductType)
            .HasColumnName("Product_Type")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.ProductTypeId).HasColumnName("Product_Type_ID");

        entity.Property(e => e.PromotionalBonusData1Amount)
            .HasColumnName("Promotional_Bonus_Data_1_Amount")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalBonusData1Channel)
            .HasColumnName("Promotional_Bonus_Data_1_Channel")
            .HasColumnType("varchar(20)");

        entity.Property(e => e.PromotionalBonusData1Details)
            .HasColumnName("Promotional_Bonus_Data_1_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalBonusData1EndDate)
            .HasColumnName("Promotional_Bonus_Data_1_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalBonusData1Indicator).HasColumnName("Promotional_Bonus_Data_1_Indicator");

        entity.Property(e => e.PromotionalBonusData1StartDate)
            .HasColumnName("Promotional_Bonus_Data_1_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalBonusData2Amount)
            .HasColumnName("Promotional_Bonus_Data_2_Amount")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalBonusData2Channel)
            .HasColumnName("Promotional_Bonus_Data_2_Channel")
            .HasColumnType("varchar(20)");

        entity.Property(e => e.PromotionalBonusData2Details)
            .HasColumnName("Promotional_Bonus_Data_2_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalBonusData2EndDate)
            .HasColumnName("Promotional_Bonus_Data_2_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalBonusData2Indicator).HasColumnName("Promotional_Bonus_Data_2_Indicator");

        entity.Property(e => e.PromotionalBonusData2StartDate)
            .HasColumnName("Promotional_Bonus_Data_2_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalContentIndicator).HasColumnName("Promotional_Content_Indicator");

        entity.Property(e => e.PromotionalContentMusic)
            .HasColumnName("Promotional_Content_Music")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalContentMusicEndDate)
            .HasColumnName("Promotional_Content_Music_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalContentMusicStartDate)
            .HasColumnName("Promotional_Content_Music_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalContentOtherDetails)
            .HasColumnName("Promotional_Content_Other_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalContentOtherEndDate)
            .HasColumnName("Promotional_Content_Other_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalContentOtherStartDate)
            .HasColumnName("Promotional_Content_Other_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalContentSport)
            .HasColumnName("Promotional_Content_Sport")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalContentSportEndDate)
            .HasColumnName("Promotional_Content_Sport_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalContentSportStartDate)
            .HasColumnName("Promotional_Content_Sport_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalContentSvod)
            .HasColumnName("Promotional_Content_SVOD")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalContentSvodEndDate)
            .HasColumnName("Promotional_Content_SVOD_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalContentSvodStartDate)
            .HasColumnName("Promotional_Content_SVOD_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalDomesticTextDetails)
            .HasColumnName("Promotional_Domestic_Text_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalDomesticTextEndDate)
            .HasColumnName("Promotional_Domestic_Text_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalDomesticTextIndicator).HasColumnName("Promotional_Domestic_Text_Indicator");

        entity.Property(e => e.PromotionalDomesticTextStartDate)
            .HasColumnName("Promotional_Domestic_Text_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalDomesticTextType)
            .HasColumnName("Promotional_Domestic_Text_Type")
            .HasColumnType("varchar(10)");

        entity.Property(e => e.PromotionalDomesticTextValue)
            .HasColumnName("Promotional_Domestic_Text_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalDomesticVoiceDetails)
            .HasColumnName("Promotional_Domestic_Voice_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalDomesticVoiceEndDate)
            .HasColumnName("Promotional_Domestic_Voice_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalDomesticVoiceIndicator).HasColumnName("Promotional_Domestic_Voice_Indicator");

        entity.Property(e => e.PromotionalDomesticVoiceStartDate)
            .HasColumnName("Promotional_Domestic_Voice_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalDomesticVoiceType)
            .HasColumnName("Promotional_Domestic_Voice_Type")
            .HasColumnType("varchar(10)");

        entity.Property(e => e.PromotionalDomesticVoiceValue)
            .HasColumnName("Promotional_Domestic_Voice_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalFee)
            .HasColumnName("Promotional_Fee")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalFeeChannel)
            .HasColumnName("Promotional_Fee_Channel")
            .HasColumnType("varchar(20)");

        entity.Property(e => e.PromotionalFeeDetails)
            .HasColumnName("Promotional_Fee_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalFeeDiscountType)
            .HasColumnName("Promotional_Fee_Discount_Type")
            .HasColumnType("varchar(20)");

        entity.Property(e => e.PromotionalFeeDiscountValue)
            .HasColumnName("Promotional_Fee_Discount_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalFeeEndDate)
            .HasColumnName("Promotional_Fee_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalFeeFreeMonthsAmount).HasColumnName("Promotional_Fee_Free_Months_Amount");

        entity.Property(e => e.PromotionalFeeFreeMonthsChannel)
            .HasColumnName("Promotional_Fee_Free_Months_Channel")
            .HasColumnType("varchar(20)");

        entity.Property(e => e.PromotionalFeeFreeMonthsDetails)
            .HasColumnName("Promotional_Fee_Free_Months_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalFeeFreeMonthsEndDate)
            .HasColumnName("Promotional_Fee_Free_Months_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalFeeFreeMonthsIndicator).HasColumnName("Promotional_Fee_Free_Months_Indicator");

        entity.Property(e => e.PromotionalFeeFreeMonthsStartDate)
            .HasColumnName("Promotional_Fee_Free_Months_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalFeeIndicator).HasColumnName("Promotional_Fee_Indicator");

        entity.Property(e => e.PromotionalFeeStartDate)
            .HasColumnName("Promotional_Fee_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalFrequentFlyerDetails)
            .HasColumnName("Promotional_Frequent_Flyer_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalFrequentFlyerEndDate)
            .HasColumnName("Promotional_Frequent_Flyer_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalFrequentFlyerIndicator).HasColumnName("Promotional_Frequent_Flyer_Indicator");

        entity.Property(e => e.PromotionalFrequentFlyerStartDate)
            .HasColumnName("Promotional_Frequent_Flyer_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalInclusionsAdditonalComments)
            .HasColumnName("Promotional_Inclusions_Additonal_Comments")
            .HasColumnType("varchar(600)");

        entity.Property(e => e.PromotionalInternationalRoamDetails)
            .HasColumnName("Promotional_International_Roam_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalInternationalRoamEndDate)
            .HasColumnName("Promotional_International_Roam_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalInternationalRoamIndicator).HasColumnName("Promotional_International_Roam_Indicator");

        entity.Property(e => e.PromotionalInternationalRoamStartDate)
            .HasColumnName("Promotional_International_Roam_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalInternationalTextDetails)
            .HasColumnName("Promotional_International_Text_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalInternationalTextEndDate)
            .HasColumnName("Promotional_International_Text_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalInternationalTextIndicator).HasColumnName("Promotional_International_Text_Indicator");

        entity.Property(e => e.PromotionalInternationalTextStartDate)
            .HasColumnName("Promotional_International_Text_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalInternationalTextType)
            .HasColumnName("Promotional_International_Text_Type")
            .HasColumnType("varchar(10)");

        entity.Property(e => e.PromotionalInternationalTextValue)
            .HasColumnName("Promotional_International_Text_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalInternationalVoiceCountryAmount).HasColumnName("Promotional_International_Voice_Country_Amount");

        entity.Property(e => e.PromotionalInternationalVoiceCountryList)
            .HasColumnName("Promotional_International_Voice_Country_List")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalInternationalVoiceDetails)
            .HasColumnName("Promotional_International_Voice_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalInternationalVoiceEndDate)
            .HasColumnName("Promotional_International_Voice_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalInternationalVoiceIndicator).HasColumnName("Promotional_International_Voice_Indicator");

        entity.Property(e => e.PromotionalInternationalVoiceStartDate)
            .HasColumnName("Promotional_International_Voice_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalInternationalVoiceType)
            .HasColumnName("Promotional_International_Voice_Type")
            .HasColumnType("varchar(10)");

        entity.Property(e => e.PromotionalInternationalVoiceValue)
            .HasColumnName("Promotional_International_Voice_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalOnlyOfferEndDate)
            .HasColumnName("Promotional_Only_Offer_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalOnlyOfferStartDate)
            .HasColumnName("Promotional_Only_Offer_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalOtherDescription)
            .HasColumnName("Promotional_Other_Description")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalOtherDetails)
            .HasColumnName("Promotional_Other_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalOtherEndDate)
            .HasColumnName("Promotional_Other_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalOtherIndicator).HasColumnName("Promotional_Other_Indicator");

        entity.Property(e => e.PromotionalOtherStartDate)
            .HasColumnName("Promotional_Other_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalPlanFee)
            .HasColumnName("Promotional_Plan_Fee")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalPrepaidBonusCreditDetails)
            .HasColumnName("Promotional_Prepaid_Bonus_Credit_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalPrepaidBonusCreditEnd)
            .HasColumnName("Promotional_Prepaid_Bonus_Credit_End")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalPrepaidBonusCreditIndicator).HasColumnName("Promotional_Prepaid_Bonus_Credit_Indicator");

        entity.Property(e => e.PromotionalPrepaidBonusCreditStart)
            .HasColumnName("Promotional_Prepaid_Bonus_Credit_Start")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalPrepaidBonusCreditValue)
            .HasColumnName("Promotional_Prepaid_Bonus_Credit_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalPrepaidInternationalBonusCreditsDetails)
            .HasColumnName("Promotional_Prepaid_International_Bonus_Credits_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalPrepaidInternationalBonusCreditsEnd)
            .HasColumnName("Promotional_Prepaid_International_Bonus_Credits_End")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalPrepaidInternationalBonusCreditsIndicator).HasColumnName("Promotional_Prepaid_International_Bonus_Credits_Indicator");

        entity.Property(e => e.PromotionalPrepaidInternationalBonusCreditsStart)
            .HasColumnName("Promotional_Prepaid_International_Bonus_Credits_Start")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalPrepaidInternationalBonusCreditsValue)
            .HasColumnName("Promotional_Prepaid_International_Bonus_Credits_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.PromotionalStudentDiscountDetails)
            .HasColumnName("Promotional_Student_Discount_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.PromotionalStudentDiscountEndDate)
            .HasColumnName("Promotional_Student_Discount_End_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalStudentDiscountIndicator).HasColumnName("Promotional_Student_Discount_Indicator");

        entity.Property(e => e.PromotionalStudentDiscountStartDate)
            .HasColumnName("Promotional_Student_Discount_Start_Date")
            .HasColumnType("date");

        entity.Property(e => e.PromotionalStudentDiscountType)
            .HasColumnName("Promotional_Student_Discount_Type")
            .HasColumnType("varchar(10)");

        entity.Property(e => e.PromotionalStudentDiscountValue)
            .HasColumnName("Promotional_Student_Discount_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.Segment).HasColumnType("varchar(30)");

        entity.Property(e => e.StandardContentIndicator).HasColumnName("Standard_Content_Indicator");

        entity.Property(e => e.StandardContentMusicAdditionalCost)
            .HasColumnName("Standard_Content_Music_Additional_Cost")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardContentMusicDataFree).HasColumnName("Standard_Content_Music_Data_Free");

        entity.Property(e => e.StandardContentMusicDetails)
            .HasColumnName("Standard_Content_Music_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentMusicIncludedInPlan).HasColumnName("Standard_Content_Music_Included_In_Plan");

        entity.Property(e => e.StandardContentMusicInclusions)
            .HasColumnName("Standard_Content_Music_Inclusions")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentMusicIndicator).HasColumnName("Standard_Content_Music_Indicator");

        entity.Property(e => e.StandardContentOther1AdditionalCost)
            .HasColumnName("Standard_Content_Other_1_Additional_Cost")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardContentOther1DataFree).HasColumnName("Standard_Content_Other_1_Data_Free");

        entity.Property(e => e.StandardContentOther1Details)
            .HasColumnName("Standard_Content_Other_1_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentOther1IncludedInPlan).HasColumnName("Standard_Content_Other_1_Included_In_Plan");

        entity.Property(e => e.StandardContentOther1Inclusions)
            .HasColumnName("Standard_Content_Other_1_Inclusions")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentOther1Indicator).HasColumnName("Standard_Content_Other_1_Indicator");

        entity.Property(e => e.StandardContentOther2AdditionalCost)
            .HasColumnName("Standard_Content_Other_2_Additional_Cost")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardContentOther2DataFree).HasColumnName("Standard_Content_Other_2_Data_Free");

        entity.Property(e => e.StandardContentOther2Details)
            .HasColumnName("Standard_Content_Other_2_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentOther2IncludedInPlan).HasColumnName("Standard_Content_Other_2_Included_In_Plan");

        entity.Property(e => e.StandardContentOther2Inclusions)
            .HasColumnName("Standard_Content_Other_2_Inclusions")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentOther2Indicator).HasColumnName("Standard_Content_Other_2_Indicator");

        entity.Property(e => e.StandardContentOther3AdditionalCost)
            .HasColumnName("Standard_Content_Other_3_Additional_Cost")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardContentOther3DataFree).HasColumnName("Standard_Content_Other_3_Data_Free");

        entity.Property(e => e.StandardContentOther3Details)
            .HasColumnName("Standard_Content_Other_3_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentOther3IncludedInPlan).HasColumnName("Standard_Content_Other_3_Included_In_Plan");

        entity.Property(e => e.StandardContentOther3Inclusions)
            .HasColumnName("Standard_Content_Other_3_Inclusions")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentOther3Indicator).HasColumnName("Standard_Content_Other_3_Indicator");

        entity.Property(e => e.StandardContentSportAdditionalCost)
            .HasColumnName("Standard_Content_Sport_Additional_Cost")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardContentSportDataFree).HasColumnName("Standard_Content_Sport_Data_Free");

        entity.Property(e => e.StandardContentSportDetails)
            .HasColumnName("Standard_Content_Sport_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentSportIncludedInPlan).HasColumnName("Standard_Content_Sport_Included_In_Plan");

        entity.Property(e => e.StandardContentSportInclusions)
            .HasColumnName("Standard_Content_Sport_Inclusions")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentSportIndicator).HasColumnName("Standard_Content_Sport_Indicator");

        entity.Property(e => e.StandardContentSvodAdditionalCost)
            .HasColumnName("Standard_Content_SVOD_Additional_Cost")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardContentSvodDataFree).HasColumnName("Standard_Content_SVOD_Data_Free");

        entity.Property(e => e.StandardContentSvodDetails)
            .HasColumnName("Standard_Content_SVOD_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentSvodIncludedInPlan).HasColumnName("Standard_Content_SVOD_Included_In_Plan");

        entity.Property(e => e.StandardContentSvodInclusions)
            .HasColumnName("Standard_Content_SVOD_Inclusions")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardContentSvodIndicator).HasColumnName("Standard_Content_SVOD_Indicator");

        entity.Property(e => e.StandardData)
            .HasColumnName("Standard_Data")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardDataOnlineSignUp)
            .HasColumnName("Standard_Data_Online_Sign_Up")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardDomesticCustomerToCustomerDetails)
            .HasColumnName("Standard_Domestic_Customer_To_Customer_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardDomesticCustomerToCustomerIndicator).HasColumnName("Standard_Domestic_Customer_To_Customer_Indicator");

        entity.Property(e => e.StandardDomesticCustomerToCustomerValue)
            .HasColumnName("Standard_Domestic_Customer_To_Customer_Value")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.StandardDomesticPrepaidExtraCreditDetails)
            .HasColumnName("Standard_Domestic_Prepaid_Extra_Credit_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardDomesticPrepaidExtraCreditIndicator).HasColumnName("Standard_Domestic_Prepaid_Extra_Credit_Indicator");

        entity.Property(e => e.StandardDomesticPrepaidExtraCreditValue)
            .HasColumnName("Standard_Domestic_Prepaid_Extra_Credit_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardDomesticTextAmount).HasColumnName("Standard_Domestic_Text_Amount");

        entity.Property(e => e.StandardDomesticTextDetails)
            .HasColumnName("Standard_Domestic_Text_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardDomesticTextInclusions)
            .HasColumnName("Standard_Domestic_Text_Inclusions")
            .HasColumnType("varchar(50)");

        entity.Property(e => e.StandardDomesticVoiceDetails)
            .HasColumnName("Standard_Domestic_Voice_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StandardDomesticVoiceInclusions)
            .HasColumnName("Standard_Domestic_Voice_Inclusions")
            .HasColumnType("varchar(50)");

        entity.Property(e => e.StandardDomesticVoiceValue)
            .HasColumnName("Standard_Domestic_Voice_Value")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardFee)
            .HasColumnName("Standard_Fee")
            .HasColumnType("decimal");

        entity.Property(e => e.StandardFeePaymentTermsDetails)
            .HasColumnName("Standard_Fee_Payment_Terms_Details")
            .HasColumnType("varchar(512)");

        entity.Property(e => e.StudentDiscountType)
            .HasColumnName("Student_Discount_Type")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.StudentDiscountValue)
            .HasColumnName("Student_Discount_Value")
            .HasColumnType("decimal");

        
        entity.Property(e => e.TermId).HasColumnName("Term_ID");

        entity.Property(e => e.Unit).HasColumnType("varchar(20)");

        entity.Property(e => e.Username).HasColumnType("varchar(50)");
      });

      modelBuilder.Entity<MobileSearchResult>(entity =>
      {
        entity.ToTable("MOBILE_SEARCH_VIEW");

        entity.HasKey(e => new { e.Id, e.ProductCategoryId });

        entity.Property(e => e.Id)
            .HasColumnName("ID")
            .ValueGeneratedNever();

        entity.Property(e => e.Brand).HasColumnType("varchar(40)");

        entity.Property(e => e.BrandId).HasColumnName("Brand_ID");

        entity.Property(e => e.ProductCategory)
            .HasColumnName("Product_Category")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.ProductCategoryId).HasColumnName("Product_Category_ID");

        entity.Property(e => e.ProductName)
            .HasColumnName("Product_Name")
            .HasColumnType("varchar(100)");

        entity.Property(e => e.ProductNameId).HasColumnName("Product_Name_ID");

        entity.Property(e => e.ProductType)
            .HasColumnName("Product_Type")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.ProductTypeId).HasColumnName("Product_Type_ID");

        entity.Property(e => e.Segment).HasColumnType("varchar(30)");

        entity.Property(e => e.StandardFee)
            .HasColumnName("Standard_Fee")
            .HasColumnType("decimal");

        entity.Property(e => e.TermId).HasColumnName("Term_ID");

        entity.Property(e => e.Unit)
            .HasColumnName("Unit")
            .HasColumnType("varchar(20)");

        entity.Property(e => e.PlanType)
            .HasColumnName("Plan_Type")
            .HasColumnType("varchar(20)");
      });

      modelBuilder.Entity<ProductNameSummary>(entity =>
      {
        entity.HasKey(e => new { e.ProductNameId });

        entity.ToTable("PRODUCT_NAME_SUMMARY");

        entity.Property(e => e.ProductNameId)
            .HasColumnName("Product_Name_ID")
            .ValueGeneratedOnAdd();

        entity.Property(e => e.ProductNameName)
            .HasColumnName("Product_Name")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.ProductCategory)
            .HasColumnName("Product_Category")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.BrandId)
            .HasColumnName("Brand_ID")
            .ValueGeneratedOnAdd();

        entity.Property(e => e.BrandName)
            .HasColumnName("Brand")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.RecordStatus)
            .HasColumnName("Record_Status")
            .ValueGeneratedOnAdd();
      });

      modelBuilder.Entity<TermSummary>(entity =>
      {
        entity.HasKey(e => new { e.TermId });
        entity.ToTable("TERM_SUMMARY");
        entity.Property(e => e.TermId)
            .HasColumnName("Term_ID")
            .ValueGeneratedOnAdd();
        entity.Property(e => e.TermValue)
            .HasColumnName("Term")
            .ValueGeneratedOnAdd();
        entity.Property(e => e.Unit)
          .HasColumnName("Unit")
          .HasColumnType("varchar(20)");
        entity.Property(e => e.ProductCategory)
          .HasColumnName("Product_Category")
          .HasColumnType("varchar(40)");
        entity.Property(e => e.RecordStatus).HasColumnName("Record_Status");
      });

      modelBuilder.Entity<ProductTypeSummary>(entity =>
      {
        entity.HasKey(e => new { e.ProductTypeId});
        entity.ToTable("PRODUCT_TYPE_SUMMARY");
        entity.Property(e => e.ProductTypeId)
            .HasColumnName("PRODUCT_TYPE_ID")
            .ValueGeneratedOnAdd();
        entity.Property(e => e.ProductTypeName)
            .HasColumnName("PRODUCT_TYPE")
            .HasColumnType("varchar(40)");
        entity.Property(e => e.ProductTypeCategory)
          .HasColumnName("PRODUCT_TYPE_CATEGORY")
          .HasColumnType("varchar(40)");
        entity.Property(e => e.ProductCategory)
          .HasColumnName("PRODUCT_CATEGORY")
          .HasColumnType("varchar(40)");
        entity.Property(e => e.RecordStatus).HasColumnName("RECORD_STATUS");
      });

      modelBuilder.Entity<SearchResult>(entity =>
      {
        entity.HasKey(e => new { e.PostpaidId });

        entity.ToTable("POSTPAID_SEARCH_VIEW");

        entity.Property(e => e.PostpaidId)
            .HasColumnName("Postpaid_ID")
            .ValueGeneratedOnAdd();

        entity.Property(e => e.ProductNameId)
            .HasColumnName("Product_Name_ID")
            .ValueGeneratedOnAdd();

        entity.Property(e => e.ProductName)
            .HasColumnName("Product_Name")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.Segment)
            .HasColumnName("Segment")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.BrandId)
            .HasColumnName("Brand_ID")
            .ValueGeneratedOnAdd();

        entity.Property(e => e.BrandName)
            .HasColumnName("Brand")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.ProductTypeId)
            .HasColumnName("Product_Type_ID")
            .ValueGeneratedOnAdd();

        entity.Property(e => e.ProductType)
            .HasColumnName("Product_Type")
            .HasColumnType("varchar(40)");

        entity.Property(e => e.TermId)
            .HasColumnName("Term_ID")
            .ValueGeneratedOnAdd();

        entity.Property(e => e.TermMonths)
            .HasColumnName("Term")
            .ValueGeneratedOnAdd();

        entity.Property(e => e.StandardFee)
            .HasColumnName("Standard_Fee")
            .HasColumnType("decimal");

      });

      modelBuilder.Entity<Brand>(entity =>
            {
              entity.HasKey(e => new { e.BrandId, e.StartDateTime })
                  .HasName("PK_BRAND_1");

              entity.ToTable("BRAND");

              entity.Property(e => e.BrandId)
                  .HasColumnName("Brand_ID")
                  .ValueGeneratedOnAdd();

              entity.Property(e => e.StartDateTime)
                  .HasColumnName("Start_Date_Time")
                  .HasColumnType("datetime");

              entity.Property(e => e.BrandName)
                  .HasColumnName("Brand")
                  .HasColumnType("varchar(40)");

              entity.Property(e => e.EndDateTime)
                  .HasColumnName("End_Date_Time")
                  .HasColumnType("datetime");

              entity.Property(e => e.ProductCategoryId).HasColumnName("Product_Category_ID");

              entity.Property(e => e.RecordStatus).HasColumnName("Record_Status");

              entity.Property(e => e.Username).HasColumnType("varchar(40)");
            });

      modelBuilder.Entity<FrequentFlyerProgram>(entity =>
      {
        entity.HasKey(e => new { e.FrequentFlyerProgramId, e.StartDateTime })
                  .HasName("PK_FREQUENT_FLYER_PROGRAM_1");

        entity.ToTable("FREQUENT_FLYER_PROGRAM");

        entity.Property(e => e.FrequentFlyerProgramId)
                  .HasColumnName("Frequent_Flyer_Program_ID")
                  .ValueGeneratedOnAdd();

        entity.Property(e => e.StartDateTime)
                  .HasColumnName("Start_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.EndDateTime)
                  .HasColumnName("End_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.FrequentFlyerProgramName)
                  .HasColumnName("Frequent_Flyer_Program")
                  .HasColumnType("varchar(40)");

        entity.Property(e => e.RecordStatus).HasColumnName("Record_Status");

        entity.Property(e => e.Username).HasColumnType("varchar(40)");
      });

      modelBuilder.Entity<ProductCategory>(entity =>
      {
        entity.HasKey(e => new { e.ProductCategoryId, e.StartDateTime })
                  .HasName("PK_PRODUCT_CATEGORY_1");

        entity.ToTable("PRODUCT_CATEGORY");

        entity.Property(e => e.ProductCategoryId)
                  .HasColumnName("Product_Category_ID")
                  .ValueGeneratedOnAdd();

        entity.Property(e => e.StartDateTime)
                  .HasColumnName("Start_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.EndDateTime)
                  .HasColumnName("End_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.ProductCategoryName)
                  .HasColumnName("Product_Category")
                  .HasColumnType("varchar(40)");

        entity.Property(e => e.RecordStatus).HasColumnName("Record_Status");

        entity.Property(e => e.Username).HasColumnType("varchar(40)");
      });

      modelBuilder.Entity<ProductName>(entity =>
      {
        entity.HasKey(e => new { e.ProductNameId, e.StartDateTime })
                  .HasName("PK_PRODUCT_NAME");

        entity.ToTable("PRODUCT_NAME");

        entity.Property(e => e.ProductNameId)
                  .HasColumnName("Product_Name_ID")
                  .ValueGeneratedOnAdd();

        entity.Property(e => e.StartDateTime)
                  .HasColumnName("Start_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.BrandId).HasColumnName("Brand_ID");

        entity.Property(e => e.EndDateTime)
                  .HasColumnName("End_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.ProductCategoryId).HasColumnName("Product_Category_ID");

        entity.Property(e => e.ProductNameName)
                  .HasColumnName("Product_Name")
                  .HasColumnType("varchar(100)");

        entity.Property(e => e.RecordStatus).HasColumnName("Record_Status");

        entity.Property(e => e.Username).HasColumnType("varchar(40)");
      });

      modelBuilder.Entity<ProductType>(entity =>
      {
        entity.HasKey(e => new { e.ProductTypeId, e.StartDateTime })
                  .HasName("PK_PRODUCT_TYPE_1");

        entity.ToTable("PRODUCT_TYPE");

        entity.Property(e => e.ProductTypeId)
                  .HasColumnName("Product_Type_ID")
                  .ValueGeneratedOnAdd();

        entity.Property(e => e.StartDateTime)
                  .HasColumnName("Start_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.EndDateTime)
                  .HasColumnName("End_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.ProductCategoryId).HasColumnName("Product_Category_ID");

        entity.Property(e => e.ProductTypeName)
                  .HasColumnName("Product_Type")
                  .HasColumnType("varchar(40)");

        entity.Property(e => e.RecordStatus).HasColumnName("Record_Status");

        entity.Property(e => e.Username).HasColumnType("varchar(40)");

        entity.Property(e => e.ProductTypeCategoryId).HasColumnName("Product_Type_Category_Id");
      });

      modelBuilder.Entity<Term>(entity =>
      {
        entity.HasKey(e => new { e.TermId, e.StartDateTime })
                  .HasName("PK_TERM");

        entity.ToTable("TERM");

        entity.Property(e => e.TermId)
                  .HasColumnName("Term_ID")
                  .ValueGeneratedOnAdd();

        entity.Property(e => e.StartDateTime)
                  .HasColumnName("Start_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.EndDateTime)
                  .HasColumnName("End_Date_Time")
                  .HasColumnType("datetime");

        entity.Property(e => e.ProductCategoryId).HasColumnName("Product_Category_ID");

        entity.Property(e => e.RecordStatus).HasColumnName("Record_Status");

        entity.Property(e => e.TermValue).HasColumnName("Term");

        entity.Property(e => e.Unit).HasColumnType("varchar(20)");

        entity.Property(e => e.Username).HasColumnType("varchar(40)");
      });
    }
  }
}