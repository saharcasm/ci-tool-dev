﻿using System;
using System.Collections.Generic;

namespace CI_Tool_API.Models
{
    public partial class Term
    {
        public int TermId { get; set; }
        public int? TermValue { get; set; }
        public string Unit { get; set; }
        public int? ProductCategoryId { get; set; }
        public string Username { get; set; }
        public bool RecordStatus { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
    }

  public partial class TermSummary
  {
    public int TermId { get; set; }
    public int? TermValue { get; set; }
    public string Unit { get; set; }
    public string ProductCategory { get; set; }
    public bool RecordStatus { get; set; }
  }
}
