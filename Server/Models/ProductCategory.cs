﻿using System;
using System.Collections.Generic;

namespace CI_Tool_API.Models
{
    public partial class ProductCategory
    {
        public int ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public string Username { get; set; }
        public bool RecordStatus { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
    }
}
