﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CI_Tool_API.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;


namespace CI_Tool_API.Controllers
{
    [Route("api/[controller]")]
    public class TermController : Controller
    {
    CI_ToolContext db;

    public TermController(CI_ToolContext context)
    {
      db = context;
    }

    // GET: api/values
    [HttpGet]
        public IEnumerable<Term> Get()
        {
            return db.Term.ToList();
        }

    // GET: api/values
    [HttpGet("Active")]
    public IEnumerable<Term> GetActive()
    {
      System.DateTime endDate = new System.DateTime(9999, 12, 31);
      return db.Term.Where(r => r.EndDateTime == endDate && r.RecordStatus);
    }

    // GET: api/values
    [HttpGet("Summary")]
    public IEnumerable<TermSummary> GetProductNameSummary()
    {
      return db.TermSummary.ToList();
    }

    // GET api/values/5
    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
      //CI_ToolContext db = new CI_ToolContext();
      System.DateTime endDate = new System.DateTime(9999, 12, 31);
      var item = db.Term.FirstOrDefault(t => t.TermId == id && t.EndDateTime == endDate);
      return new ObjectResult(item);
    }

    // POST api/values
    [HttpPost]
    public void Post([FromForm]int term, [FromForm]string unit, [FromForm]int productCategoryId, [FromForm]string username)
    {
      int outputParam = 0;

      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = "NEW_TERM";
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.Add(new SqlParameter("@iTerm", term));
        command.Parameters.Add(new SqlParameter("@iUnit", unit));
        command.Parameters.Add(new SqlParameter("@iProductCategoryID", productCategoryId));
        command.Parameters.Add(new SqlParameter("@iUsername", username));
        command.Parameters.Add(new SqlParameter("@oError", outputParam));

        db.Database.OpenConnection();

        command.ExecuteNonQuery();

      }
      return;
    }

    // PUT api/values/5
    [HttpPut("{id}")]
    public void Put(int id, [FromForm]int term, [FromForm]string unit, [FromForm]int productCategoryId, [FromForm]string username, [FromForm]bool recordStatus)
    {
      int outputParam = 0;

      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = "UPDATE_TERM";
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.Add(new SqlParameter("@iTermID", id));
        command.Parameters.Add(new SqlParameter("@iTerm", term));
        command.Parameters.Add(new SqlParameter("@iUnit", unit));
        command.Parameters.Add(new SqlParameter("@iProductCategoryID", productCategoryId));
        command.Parameters.Add(new SqlParameter("@iUsername", username));
        command.Parameters.Add(new SqlParameter("@iStatus", recordStatus));
        command.Parameters.Add(new SqlParameter("@oError", outputParam));

        db.Database.OpenConnection();

        command.ExecuteNonQuery();

      }
      return;
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
