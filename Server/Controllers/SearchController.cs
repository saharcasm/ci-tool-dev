﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CI_Tool_API.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectTelecaster.Server.Controllers
{
  [Route("api/[controller]")]
  public class SearchController : Controller
  {
    CI_ToolContext db;

    public SearchController(CI_ToolContext context)
    {
      db = context;
    }
    // GET: api/values
    [HttpGet]
    public IEnumerable<MobileSearchResult> Get([FromQuery] int brandId, int productTypeId, int termId, string fee, string segment, int productCategoryId, string unit)
    {
      string[] fees = fee.Split('-');
      decimal feeStart = decimal.Parse(fees[0]);
      decimal feeEnd = decimal.Parse(fees[1]);
      return db.MobileSearchResult.Where(
          s => s.BrandId == (brandId == 0 ? s.BrandId : brandId) 
          && s.ProductTypeId == (productTypeId == 0 ? s.ProductTypeId : productTypeId) 
          && s.TermId == (termId == 0 ? s.TermId : termId) 
          && s.Segment == (segment == "All" ? s.Segment : segment) 
          && s.StandardFee >= feeStart 
          && s.StandardFee <= feeEnd
          && s.ProductCategoryId == (productCategoryId == 0 ? s.ProductCategoryId : productCategoryId)
          && s.Unit == (unit == "All" ? s.Unit : unit)
          );
    }

    // GET api/values/5
    [HttpGet("{id}")]
    public string Get(int id)
    {
      return "value";
    }

    // POST api/values
    [HttpPost]
    public void Post([FromBody]string value)
    {
    }

    // PUT api/values/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
  }
}
