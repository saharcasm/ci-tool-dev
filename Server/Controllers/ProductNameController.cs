﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CI_Tool_API.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CI_Tool_API.Controllers
{
    [Route("api/[controller]")]
    public class ProductNameController : Controller
    {
    CI_ToolContext db;

    public ProductNameController(CI_ToolContext context)
    {
      db = context;
    }
    // GET: api/values
    [HttpGet]
        public IEnumerable<ProductName> Get()
          {
            return db.ProductName.ToList();
        }


    // GET: api/values
    [HttpGet("Active")]
    public IEnumerable<ProductName> GetActive()
    {
      System.DateTime endDate = new System.DateTime(9999, 12, 31);
      return db.ProductName.Where(r => r.EndDateTime == endDate && r.RecordStatus);
    }

    // GET: api/values
    [HttpGet("Summary")]
    public IEnumerable<ProductNameSummary> GetProductNameSummary()
    {
      return db.ProductNameSummary.ToList();
    }

    // GET api/values/5
    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
      System.DateTime endDate = new System.DateTime(9999, 12, 31);
      var item = db.ProductName.FirstOrDefault(t => t.ProductNameId == id && t.EndDateTime == endDate);
      return new ObjectResult(item);
    }

    // POST api/values
    [HttpPost]
    public void Post([FromForm]string productName, [FromForm]int brandId, [FromForm]string username, [FromForm]int categoryId)
    {
      int outputParam=0;
      
      //using (var context = new CI_ToolContext())
      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = "NEW_PRODUCT_NAME";
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.Add(new SqlParameter("@iProductName", productName));
        command.Parameters.Add(new SqlParameter("@iProductCategoryID", categoryId));
        command.Parameters.Add(new SqlParameter("@iBrandID", brandId));
        command.Parameters.Add(new SqlParameter("@iUsername", username));
        command.Parameters.Add(new SqlParameter("@oError", outputParam));
        
        db.Database.OpenConnection();

        command.ExecuteNonQuery();

      }
        return;
    }

    // PUT api/values/5
    [HttpPut("{id}")]
    public void Put(int id, [FromForm]string productName, [FromForm]int brandId, [FromForm]bool recordStatus, [FromForm]string username, [FromForm]int categoryId)
    {
      int outputParam = 0;

      //using (var context = new CI_ToolContext())
      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = "UPDATE_PRODUCT_NAME";
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.Add(new SqlParameter("@iProductNameId", id));
        command.Parameters.Add(new SqlParameter("@iProductName", productName));
        command.Parameters.Add(new SqlParameter("@iProductCategoryID", categoryId));
        command.Parameters.Add(new SqlParameter("@iBrandID", brandId));
        command.Parameters.Add(new SqlParameter("@iUsername", username));
        command.Parameters.Add(new SqlParameter("@iStatus", recordStatus));
        command.Parameters.Add(new SqlParameter("@oError", outputParam));

        db.Database.OpenConnection();

        command.ExecuteNonQuery();

      }
      return;
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
    }
}
