﻿using System;
using Microsoft.AspNetCore.Mvc;
using CI_Tool_API.Models;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectTelecaster.Server.Controllers
{
  [Route("api/[controller]")]



  public class ReportController : Controller
  {

    CI_ToolContext db;

    public ReportController(CI_ToolContext context)
    {
      db = context;
    }

    // GET: api/wasnow
    [HttpGet("WasNow")]
    public List<Dictionary<string, string>> GetWasNow([FromQuery]string period, int productCategoryId)
    {
      List<SqlParameter> par = new List<SqlParameter>();

      string sql = "SELECT * from [WAS_NOW_REPORT_EXTRACT](@iPeriod, @iProductCategory)";

      par.Add(new SqlParameter("@iPeriod", period));

      if (productCategoryId == 0)
      {
        par.Add(new SqlParameter("@iProductCategory", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iProductCategory", productCategoryId));
      }


      return test(sql, "|", par, true);
    }
    [HttpGet("GetHidden")]
    public List<Dictionary<string, object>> getForHidden()
    {
      List<SqlParameter> par = new List<SqlParameter>();

      string sql = "SELECT * from [MOBILE_HIDDEN_VIEW]";

      return runHiddenView(sql, "|", par, true);
    }

    // GET: api/newplan
    [HttpGet("NewPlan")]
    public string GetNewPlan([FromQuery]string period, int productCategoryId)
    {
      string retVal = "";
      List<SqlParameter> par = new List<SqlParameter>();

      string sql = "SELECT * from [NEW_PLANS_EXTRACT](@iPeriod, @iProductCategory)";
      par.Add(new SqlParameter("@iPeriod", period));

      if (productCategoryId == 0)
      {
        par.Add(new SqlParameter("@iProductCategory", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iProductCategory", productCategoryId));
      }
      retVal = GetPSVFromReader(sql, "|", par, false);

      return retVal;
    }

    // GET: api/OfferExpiry
    [HttpGet("OfferExpiry")]
    public string GetOfferExpiry([FromQuery]DateTime startDate, DateTime endDate, int productCategoryId)
    {
      string retVal = "";
      List<SqlParameter> par = new List<SqlParameter>();

      string sql = "SELECT * FROM [MOBILE_EXPIRY_REPORT](@iExpiryStartDate,@iExpiryEndDate, @iProductCategory)";
      par.Add(new SqlParameter("@iExpiryStartDate", startDate.ToString("yyyy-MM-dd")));
      par.Add(new SqlParameter("@iExpiryEndDate", endDate.ToString("yyyy-MM-dd")));
      if (productCategoryId == 0)
      {
        par.Add(new SqlParameter("@iProductCategory", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iProductCategory", productCategoryId));
      }
      retVal = GetPSVFromReader(sql, "|", par, false);

      return retVal;
    }

    // GET: api/FullExtract
    [HttpGet("FullExtract")]
    public string GetFullExtract([FromQuery]string segment, string brand, string planType, string productType, string term, string standardFee, string productStatus, DateTime startDate, DateTime endDate, string productCategory)
    {
      string retVal = "";
      string[] fees = standardFee.Split('-');
      double feeStart = double.Parse(fees[0]);
      double feeEnd = double.Parse(fees[1]);

      List<SqlParameter> par = new List<SqlParameter>();
      if (segment == "All")
      {
        par.Add(new SqlParameter("@iSegment", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iSegment", segment));
      }
      if (brand == "All")
      {
        par.Add(new SqlParameter("@iBrand", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iBrand", brand));
      }
      if (planType == "All")
      {
        par.Add(new SqlParameter("@iPlanType", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iPlanType", planType));
      }
      if (productType == "0")
      {
        par.Add(new SqlParameter("@iProductType", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iProductType", productType));
      }
      if (term == "All")
      {
        par.Add(new SqlParameter("@iTerm", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iTerm", term));
      }
      if (productStatus == "All")
      {
        par.Add(new SqlParameter("@iProductStatus", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iProductStatus", productStatus));
      }
      if (productCategory == "0")
      {
        par.Add(new SqlParameter("@iProductCategory", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iProductCategory", productCategory));
      }

      if (startDate == new DateTime(9999, 12, 31))
      {
        par.Add(new SqlParameter("@iDateTimeCreatedStart", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iDateTimeCreatedStart", startDate));
      }

      if (endDate == new DateTime(9999, 12, 31))
      {
        par.Add(new SqlParameter("@iDateTimeCreatedEnd", DBNull.Value));
      }
      else
      {
        par.Add(new SqlParameter("@iDateTimeCreatedEnd", endDate));
      }

      par.Add(new SqlParameter("@iStandardFeeStart", feeStart));
      par.Add(new SqlParameter("@iStandardFeeEnd", DBNull.Value));

      string sql = "SELECT * FROM [FULL_MOBILE_EXTRACT](@iSegment, @iBrand, @iPlanType, @iProductType, @iTerm, @iStandardFeeStart, @iStandardFeeEnd, @iProductStatus, @iDateTimeCreatedStart,@iDateTimeCreatedEnd,@iProductCategory)";

      retVal = GetPSVFromReader(sql, "|", par, false);
      return retVal;
    }


    private string GetPSVFromReader(string sqlStatement, string delimiter, bool dropEmptyColumns)
    {

      List<SqlParameter> parameters = new List<SqlParameter>();
      return GetPSVFromReader(sqlStatement, delimiter, parameters, dropEmptyColumns);

    }
    private List<Dictionary<string, string>> test(string sqlStatement, string delimiter, bool dropEmptyColumns)
    {

      List<SqlParameter> parameters = new List<SqlParameter>();
      return test(sqlStatement, delimiter, parameters, dropEmptyColumns);

    }

    private string GetPSVFromReader(string sqlStatement, string delimiter, List<SqlParameter> parameters, bool dropEmptyColumns)
    {
      StringBuilder sb = new StringBuilder();
      string tmpRow = "";
      string separator = delimiter;

      List<object[]> readerData = new List<object[]>();
      int size;
      object[] data;

      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = sqlStatement;

        for (int i = 0; i < parameters.Count; i++)
        {
          command.Parameters.Add(parameters[i]);
        }

        db.Database.OpenConnection();
        using (var reader = command.ExecuteReader())
        {
          if (reader.HasRows)
          {
            if (dropEmptyColumns)
            {
              while (reader.Read())
              {
                data = new object[reader.FieldCount];
                size = reader.GetValues(data);
                readerData.Add(data);
                
              }
              
              List<int> columns = new List<int>();
              //Work out which columns  we need to include
              if (readerData.Count > 0)
              {
                for (int i = 8; i < readerData[0].Length - 8; i++)
                {
                  object currentVal;
                  currentVal = readerData[0][i];
                  for (int x = 0; x < readerData.Count; x++)
                  {
                    if (currentVal.ToString() != readerData[x][i].ToString())
                    {
                      columns.Add(i);
                      break;
                    }
                  }

                }
              }
              Console.WriteLine(String.Format("{0}", readerData[0][1]));
              Console.WriteLine(String.Format("{0}", readerData[0][1]));
              //Iterate the result set only for those columns needed.
              tmpRow = "";
              separator = "";
              //Write out the column headings
              for (int i = 0; i < reader.FieldCount; i++)
              {
                if (i < 8 || columns.Contains(i))
                {
                  tmpRow += separator + reader.GetName(i);
                  separator = "|";
                }
              }
              sb.AppendLine(tmpRow);
              tmpRow = "";
              separator = "";

              for (int y = 0; y < readerData.Count; y++)
              {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                  if (i < 8 || columns.Contains(i))
                  {
                    tmpRow += separator + readerData[y][i].ToString();
                    separator = "|";
                  }
                }
                sb.AppendLine(tmpRow);
                tmpRow = "";
                separator = "";
              }

            }
            else
            {
              tmpRow = "";
              separator = "";
              //Write out the column headings
              for (int i = 0; i < reader.FieldCount; i++)
              {
                tmpRow += separator + reader.GetName(i);
                separator = "|";
              }
              sb.AppendLine(tmpRow);
              tmpRow = "";
              separator = "";

              // Get the records
              while (reader.Read())
              {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                  tmpRow += separator + reader[i].ToString();
                  separator = "|";
                }
                sb.AppendLine(tmpRow);
                tmpRow = "";
                separator = "";
              }
            }
          }
        }
      }



      return sb.ToString();
    }


    private List<Dictionary<string, string>> test(string sqlStatement, string delimiter, List<SqlParameter> parameters, bool dropEmptyColumns)
    {
      StringBuilder sb = new StringBuilder();

      string separator = delimiter;

      List<object[]> readerData = new List<object[]>();
      List<Dictionary<string, string>> details= new  List<Dictionary<string, string>>();

      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = sqlStatement;

        for (int i = 0; i < parameters.Count; i++)
        {
          command.Parameters.Add(parameters[i]);
        }

        db.Database.OpenConnection();
        using (var reader = command.ExecuteReader())
        {
          if (reader.HasRows)
          {
            if (dropEmptyColumns)
            {
              while (reader.HasRows && reader.Read())
              {
                var dict = new Dictionary<string, string>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                  if(!reader.IsDBNull(i)){
                    if(!String.IsNullOrEmpty(reader.GetString(i))){
                      Console.WriteLine(reader.GetName(i));
                      dict.Add(reader.GetName(i),  reader.GetString(i));
                    }
                  }
                }
                details.Add(dict);
              }
            }
            
          }
        }
      }



      return details;
    }


    private List<Dictionary<string, object>> runHiddenView(string sqlStatement, string delimiter, List<SqlParameter> parameters, bool dropEmptyColumns)
    {
      StringBuilder sb = new StringBuilder();

      string separator = delimiter;

      List<object[]> readerData = new List<object[]>();
      List<Dictionary<string, object>> details= new  List<Dictionary<string, object>>();

      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = sqlStatement;

        for (int i = 0; i < parameters.Count; i++)
        {
          command.Parameters.Add(parameters[i]);
        }

        db.Database.OpenConnection();
        using (var reader = command.ExecuteReader())
        {
          if (reader.HasRows)
          {
            if (dropEmptyColumns)
            {
              while (reader.HasRows && reader.Read())
              {
                var dict = new Dictionary<string, object>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                  if(!reader.IsDBNull(i)){
                    dict.Add(reader.GetName(i),  reader.GetValue(i));
                  }
                }
                details.Add(dict);
              }
            }
            
          }
        }
      }



      return details;
    }



    // GET api/values/5
    [HttpGet("{id}")]
    public string Get(int id)
    {
      return "value";
    }

    // POST api/values
    [HttpPost]
    public void Post([FromBody]string value)
    {
    }

    // PUT api/values/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
  }
}
