﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CI_Tool_API.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace CI_Tool_API.Controllers
{
    [Route("api/[controller]")]
    public class ProductTypeController : Controller
    {
    CI_ToolContext db;

    public ProductTypeController(CI_ToolContext context)
    {
      db = context;
    }

    // GET: api/values
    [HttpGet]
        public IEnumerable<ProductType> Get()
        {
            return db.ProductType.ToList();
        }

    // GET: api/values
    [HttpGet("Active")]
    public IEnumerable<ProductType> GetActive()
    {
      System.DateTime endDate = new System.DateTime(9999, 12, 31);
      return db.ProductType.Where(r => r.EndDateTime == endDate && r.RecordStatus);
    }

    // GET: api/values
    [HttpGet("Summary")]
    public IEnumerable<ProductTypeSummary> GetProductNameSummary()
    {
      return db.ProductTypeSummary.ToList();
    }

    // GET api/values/5
    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
      System.DateTime endDate = new System.DateTime(9999, 12, 31);
      var item = db.ProductType.FirstOrDefault(t => t.ProductTypeId == id && t.EndDateTime == endDate);
      return new ObjectResult(item);
    }

    // POST api/values
    [HttpPost]
    public void Post([FromForm]string productTypeName, [FromForm]int productTypeCategoryId, [FromForm]string username, [FromForm]int categoryId, [FromForm]int productCategoryId)
    {
      int outputParam = 0;

      //using (var context = new CI_ToolContext())
      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = "NEW_PRODUCT_TYPE";
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.Add(new SqlParameter("@iProductType", productTypeName));
        command.Parameters.Add(new SqlParameter("@iProductTypeCategoryID", productTypeCategoryId));
        command.Parameters.Add(new SqlParameter("@iProductCategoryID", productCategoryId));
        command.Parameters.Add(new SqlParameter("@iUsername", username));
        command.Parameters.Add(new SqlParameter("@oError", outputParam));

        db.Database.OpenConnection();

        command.ExecuteNonQuery();

      }
      return;
    }

    // PUT api/values/5
    [HttpPut("{id}")]
    public void Put(int id, [FromForm]string productTypeName, [FromForm]int productTypeCategoryId, [FromForm]string username, [FromForm]int categoryId, [FromForm]bool recordStatus, [FromForm]int productCategoryId)
    {
      int outputParam = 0;

      //using (var context = new CI_ToolContext())
      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = "UPDATE_PRODUCT_TYPE";
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.Add(new SqlParameter("@iProductTypeID", id));
        command.Parameters.Add(new SqlParameter("@iProductType", productTypeName));
        command.Parameters.Add(new SqlParameter("@iProductTypeCategoryID", productTypeCategoryId));
        command.Parameters.Add(new SqlParameter("@iProductCategoryID", productCategoryId));
        command.Parameters.Add(new SqlParameter("@iUsername", username));
        command.Parameters.Add(new SqlParameter("@iStatus", recordStatus));
        command.Parameters.Add(new SqlParameter("@oError", outputParam));

        db.Database.OpenConnection();

        command.ExecuteNonQuery();

      }
      return;
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
