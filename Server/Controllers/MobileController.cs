﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CI_Tool_API.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace CI_Tool_API.Controllers
{
  [Route("api/[controller]")]
  public class MobileController : Controller
  {
    CI_ToolContext db;

    public MobileController(CI_ToolContext context)
    {
      db = context;
  }

    // GET: api/values
  [HttpGet]
  public IEnumerable<MobilePlan> Get()
  {
      return db.MobilePlan.ToList();
  }

  [HttpGet("{id}")]
  public IActionResult Get(int id, [FromQuery] int categoryId)
  {
      var item = db.MobilePlan.FirstOrDefault(t => t.Id == id && t.ProductCategoryId == categoryId);
      return new ObjectResult(item);
  }

    // POST api/values  
  [HttpPost("Postpaid")]
  public void PostPostpaid([FromBody]MobilePlan plan)
  {
      int outputParam = 0;

      using (var command = db.Database.GetDbConnection().CreateCommand())
      {
        command.CommandText = "ADD_NEW_POSTPAID";
        command.CommandType = System.Data.CommandType.StoredProcedure;

        command.Parameters.Add(new SqlParameter("@iUsername", plan.Username));
        command.Parameters.Add(new SqlParameter("@iProduct_Status", plan.ProductStatus));
        command.Parameters.Add(new SqlParameter("@iProduct_Category_ID", plan.ProductCategoryId));
        command.Parameters.Add(new SqlParameter("@iSegment", plan.Segment));
        command.Parameters.Add(new SqlParameter("@iBrand_ID", plan.BrandId));
        command.Parameters.Add(new SqlParameter("@iProduct_Type_ID", plan.ProductTypeId));
        command.Parameters.Add(new SqlParameter("@iTerm_ID", plan.TermId));
        command.Parameters.Add(new SqlParameter("@iProduct_Name_ID", plan.ProductNameId));
        command.Parameters.Add(new SqlParameter("@iProduct_Change_Type", plan.ProductChangeType));
        command.Parameters.Add(new SqlParameter("@iPlan_Type", plan.PlanType));
        command.Parameters.Add(new SqlParameter("@iPromotional_Plan_Fee", plan.PromotionalPlanFee));
        command.Parameters.Add(new SqlParameter("@iPromotional_Only_Offer_Start_Date", plan.PromotionalOnlyOfferStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Only_Offer_End_Date", plan.PromotionalOnlyOfferEndDate));
        command.Parameters.Add(new SqlParameter("@iStandard_Fee", plan.StandardFee));
        command.Parameters.Add(new SqlParameter("@iStudent_Discount_Type", plan.StudentDiscountType));
        command.Parameters.Add(new SqlParameter("@iStudent_Discount_Value", plan.StudentDiscountValue));
        command.Parameters.Add(new SqlParameter("@iBundle_Discount", plan.BundleDiscount));
        command.Parameters.Add(new SqlParameter("@iBundle_Discount_Details", plan.BundleDiscountDetails));
        command.Parameters.Add(new SqlParameter("@iStandard_Data", plan.StandardData));
        command.Parameters.Add(new SqlParameter("@iStandard_Data_Online_Sign_Up", plan.StandardDataOnlineSignUp));
        command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Inclusions", plan.StandardDomesticVoiceInclusions));
        command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Value", plan.StandardDomesticVoiceValue));
        command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Details", plan.StandardDomesticVoiceDetails));
        command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Inclusions", plan.StandardDomesticTextInclusions));
        command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Amount", plan.StandardDomesticTextAmount));
        command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Details", plan.StandardDomesticTextDetails));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Inclusions", plan.InternationalVoice1Inclusions));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Value", plan.InternationalVoice1Value));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Included_In_Plan", plan.InternationalVoice1IncludedInPlan));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Additional_Cost", plan.InternationalVoice1AdditionalCost));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Countries_Amount", plan.InternationalVoice1CountriesAmount));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Countries_List", plan.InternationalVoice1CountriesList));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Inclusions", plan.InternationalVoice2Inclusions));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Value", plan.InternationalVoice2Value));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Included_In_Plan", plan.InternationalVoice2IncludedInPlan));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Additional_Cost", plan.InternationalVoice2AdditionalCost));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Countries_Amount", plan.InternationalVoice2CountriesAmount));
        command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Countries_List", plan.InternationalVoice2CountriesList));
        command.Parameters.Add(new SqlParameter("@iInternational_Text_Inclusions", plan.InternationalTextInclusions));
        command.Parameters.Add(new SqlParameter("@iInternational_Text_Value", plan.InternationalTextValue));
        command.Parameters.Add(new SqlParameter("@iInternational_Text_Countries_Amount", plan.InternationalTextCountriesAmount));
        command.Parameters.Add(new SqlParameter("@iInternational_Text_Countries_List", plan.InternationalTextCountriesList));
        command.Parameters.Add(new SqlParameter("@iInternational_Text_Details", plan.InternationalTextDetails));
        command.Parameters.Add(new SqlParameter("@iInternational_Roam_Indicator", plan.InternationalRoamIndicator));
        command.Parameters.Add(new SqlParameter("@iInternational_Roam_Included_In_Plan", plan.InternationalRoamIncludedInPlan));
        command.Parameters.Add(new SqlParameter("@iInternational_Roam_Additional_Cost", plan.InternationalRoamAdditionalCost));
        command.Parameters.Add(new SqlParameter("@iInternational_Roam_Details", plan.InternationalRoamDetails));
        command.Parameters.Add(new SqlParameter("@iInternational_Roam_Country_Amount", plan.InternationalRoamCountryAmount));
        command.Parameters.Add(new SqlParameter("@iInternational_Roam_Country_List", plan.InternationalRoamCountryList));
        command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Indicator", plan.FrequentFlyerIndicator));
        command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Program_ID", plan.FrequentFlyerProgramId));
        command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Program_Details", plan.FrequentFlyerProgramDetails));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Indicator", plan.StandardContentIndicator));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Indicator", plan.StandardContentSportIndicator));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Data_Free", plan.StandardContentSportDataFree));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Included_In_Plan", plan.StandardContentSportIncludedInPlan));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Additional_Cost", plan.StandardContentSportAdditionalCost));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Inclusions", plan.StandardContentSportInclusions));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Details", plan.StandardContentSportDetails));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Indicator", plan.StandardContentMusicIndicator));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Data_Free", plan.StandardContentMusicDataFree));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Included_In_Plan", plan.StandardContentMusicIncludedInPlan));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Additional_Cost", plan.StandardContentMusicAdditionalCost));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Inclusions", plan.StandardContentMusicInclusions));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Details", plan.StandardContentMusicDetails));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Indicator", plan.StandardContentSvodIndicator));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Data_Free", plan.StandardContentSvodDataFree));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Included_In_Plan", plan.StandardContentSvodIncludedInPlan));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Additional_Cost", plan.StandardContentSvodAdditionalCost));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Inclusions", plan.StandardContentSvodInclusions));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Details", plan.StandardContentSvodDetails));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Indicator", plan.StandardContentOther1Indicator));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Data_Free", plan.StandardContentOther1DataFree));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Included_In_Plan", plan.StandardContentOther1IncludedInPlan));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Additional_Cost", plan.StandardContentOther1AdditionalCost));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Inclusions", plan.StandardContentOther1Inclusions));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Details", plan.StandardContentOther1Details));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Indicator", plan.StandardContentOther2Indicator));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Data_Free", plan.StandardContentOther2DataFree));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Included_In_Plan", plan.StandardContentOther2IncludedInPlan));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Additional_Cost", plan.StandardContentOther2AdditionalCost));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Inclusions", plan.StandardContentOther2Inclusions));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Details", plan.StandardContentOther2Details));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Indicator", plan.OtherInclusionsIndicator));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Network_Trial", plan.OtherInclusionsNetworkTrial));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Network_Guarantee", plan.OtherInclusionsNetworkGuarantee));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Data_Share", plan.OtherInclusionsDataShare));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Data_Rollover", plan.OtherInclusionsDataRollover));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Device_TradeUp", plan.OtherInclusionsDeviceTradeUp));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Wifi", plan.OtherInclusionsWifi));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Cloud_Storage", plan.OtherInclusionsCloudStorage));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Refer_Friend", plan.OtherInclusionsReferFriend));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Business_Apps", plan.OtherInclusionsBusinessApps));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Other", plan.OtherInclusionsOther));
        command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Additional_Comments", plan.OtherInclusionsAdditionalComments));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Indicator", plan.PromotionalFeeIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Discount_Type", plan.PromotionalFeeDiscountType));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Discount_Value", plan.PromotionalFeeDiscountValue));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Channel", plan.PromotionalFeeChannel));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Details", plan.PromotionalFeeDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Start_Date", plan.PromotionalFeeStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_End_Date", plan.PromotionalFeeEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Indicator", plan.PromotionalStudentDiscountIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Type", plan.PromotionalStudentDiscountType));
        command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Value", plan.PromotionalStudentDiscountValue));
        command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Details", plan.PromotionalStudentDiscountDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Start_Date", plan.PromotionalStudentDiscountStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_End_Date", plan.PromotionalStudentDiscountEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Indicator", plan.PromotionalFeeFreeMonthsIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Amount", plan.PromotionalFeeFreeMonthsAmount));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Channel", plan.PromotionalFeeFreeMonthsChannel));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Details", plan.PromotionalFeeFreeMonthsDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Start_Date", plan.PromotionalFeeFreeMonthsStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_End_Date", plan.PromotionalFeeFreeMonthsEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Indicator", plan.PromotionalBonusData1Indicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Amount", plan.PromotionalBonusData1Amount));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Channel", plan.PromotionalBonusData1Channel));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Details", plan.PromotionalBonusData1Details));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Start_Date", plan.PromotionalBonusData1StartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_End_Date", plan.PromotionalBonusData1EndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Indicator", plan.PromotionalBonusData2Indicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Amount", plan.PromotionalBonusData2Amount));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Channel", plan.PromotionalBonusData2Channel));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Details", plan.PromotionalBonusData2Details));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Start_Date", plan.PromotionalBonusData2StartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_End_Date", plan.PromotionalBonusData2EndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Indicator", plan.PromotionalDomesticVoiceIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Type", plan.PromotionalDomesticVoiceType));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Value", plan.PromotionalDomesticVoiceValue));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Start_Date", plan.PromotionalDomesticVoiceStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_End_Date", plan.PromotionalDomesticVoiceEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Details", plan.PromotionalDomesticVoiceDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Indicator", plan.PromotionalDomesticTextIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Type", plan.PromotionalDomesticTextType));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Value", plan.PromotionalDomesticTextValue));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Start_Date", plan.PromotionalDomesticTextStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_End_Date", plan.PromotionalDomesticTextEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Details", plan.PromotionalDomesticTextDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Indicator", plan.PromotionalInternationalVoiceIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Type", plan.PromotionalInternationalVoiceType));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Value", plan.PromotionalInternationalVoiceValue));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Country_Amount", plan.PromotionalInternationalVoiceCountryAmount));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Country_List", plan.PromotionalInternationalVoiceCountryList));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Start_Date", plan.PromotionalInternationalVoiceStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Details", plan.PromotionalInternationalVoiceDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_End_Date", plan.PromotionalInternationalVoiceEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Indicator", plan.PromotionalInternationalTextIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Type", plan.PromotionalInternationalTextType));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Value", plan.PromotionalInternationalTextValue));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Start_Date", plan.PromotionalInternationalTextStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_End_Date", plan.PromotionalInternationalTextEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Details", plan.PromotionalInternationalTextDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Indicator", plan.PromotionalInternationalRoamIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Details", plan.PromotionalInternationalRoamDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Start_Date", plan.PromotionalInternationalRoamStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_End_Date", plan.PromotionalInternationalRoamEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Indicator", plan.PromotionalFrequentFlyerIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Details", plan.PromotionalFrequentFlyerDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Start_Date", plan.PromotionalFrequentFlyerStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_End_Date", plan.PromotionalFrequentFlyerEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Indicator", plan.PromotionalContentIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport", plan.PromotionalContentSport));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport_Start_Date", plan.PromotionalContentSportStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport_End_Date", plan.PromotionalContentSportEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music", plan.PromotionalContentMusic));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music_Start_Date", plan.PromotionalContentMusicStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music_End_Date", plan.PromotionalContentMusicEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD", plan.PromotionalContentSvod));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD_Start_Date", plan.PromotionalContentSvodStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD_End_Date", plan.PromotionalContentSvodEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_Details", plan.PromotionalContentOtherDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_Start_Date", plan.PromotionalContentOtherStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_End_Date", plan.PromotionalContentOtherEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Other_Indicator", plan.PromotionalOtherIndicator));
        command.Parameters.Add(new SqlParameter("@iPromotional_Other_Description", plan.PromotionalOtherDescription));
        command.Parameters.Add(new SqlParameter("@iPromotional_Other_Details", plan.PromotionalOtherDetails));
        command.Parameters.Add(new SqlParameter("@iPromotional_Other_Start_Date", plan.PromotionalOtherStartDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Other_End_Date", plan.PromotionalOtherEndDate));
        command.Parameters.Add(new SqlParameter("@iPromotional_Inclusions_Additonal_Comments", plan.PromotionalInclusionsAdditonalComments));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_1", plan.PlaceholderAttribute1));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_2", plan.PlaceholderAttribute2));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_3", plan.PlaceholderAttribute3));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_4", plan.PlaceholderAttribute4));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_5", plan.PlaceholderAttribute5));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_6", plan.PlaceholderAttribute6));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_7", plan.PlaceholderAttribute7));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_8", plan.PlaceholderAttribute8));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_9", plan.PlaceholderAttribute9));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_10", plan.PlaceholderAttribute10));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_11", plan.PlaceholderAttribute11));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_12", plan.PlaceholderAttribute12));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_13", plan.PlaceholderAttribute13));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_14", plan.PlaceholderAttribute14));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_15", plan.PlaceholderAttribute15));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_16", plan.PlaceholderAttribute16));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_17", plan.PlaceholderAttribute17));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_18", plan.PlaceholderAttribute18));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_19", plan.PlaceholderAttribute19));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_20", plan.PlaceholderAttribute20));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_21", plan.PlaceholderAttribute21));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_22", plan.PlaceholderAttribute22));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_23", plan.PlaceholderAttribute23));
        command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_24", plan.PlaceholderAttribute24));
        //New params for prepaid changes
        command.Parameters.Add(new SqlParameter("@iStandard_Fee_Payment_Terms_Details", plan.StandardFeePaymentTermsDetails));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Indicator", plan.StandardContentOther3Indicator));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Data_Free", plan.StandardContentOther3DataFree));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Included_In_Plan", plan.StandardContentOther3IncludedInPlan));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Additional_Cost", plan.StandardContentOther3AdditionalCost));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Inclusions", plan.StandardContentOther3Inclusions));
        command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Details", plan.StandardContentOther3Details));

        command.Parameters.Add(new SqlParameter("@oError", outputParam));


        for (int i = 0; i < command.Parameters.Count; i++)
        {
          if (command.Parameters[i].Value == null)
          {
            command.Parameters[i].Value = DBNull.Value;
        }
    }

    db.Database.OpenConnection();
    command.ExecuteNonQuery();
}

return;
}

    // PUT api/values/5
[HttpPut("Postpaid")]
public void PutPostpaid([FromBody]MobilePlan plan)
{
  int outputParam = 0;

  using (var command = db.Database.GetDbConnection().CreateCommand())
  {
    command.CommandText = "ADD_UPDATE_POSTPAID";
    command.CommandType = System.Data.CommandType.StoredProcedure;

    command.Parameters.Add(new SqlParameter("@iPostpaid_ID", plan.Id));
    command.Parameters.Add(new SqlParameter("@iUsername", plan.Username));
    command.Parameters.Add(new SqlParameter("@iProduct_Status", plan.ProductStatus));
    command.Parameters.Add(new SqlParameter("@iProduct_Category_ID", plan.ProductCategoryId));
    command.Parameters.Add(new SqlParameter("@iSegment", plan.Segment));
    command.Parameters.Add(new SqlParameter("@iBrand_ID", plan.BrandId));
    command.Parameters.Add(new SqlParameter("@iProduct_Type_ID", plan.ProductTypeId));
    command.Parameters.Add(new SqlParameter("@iTerm_ID", plan.TermId));
    command.Parameters.Add(new SqlParameter("@iProduct_Name_ID", plan.ProductNameId));
    command.Parameters.Add(new SqlParameter("@iProduct_Change_Type", plan.ProductChangeType));
    command.Parameters.Add(new SqlParameter("@iPlan_Type", plan.PlanType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Plan_Fee", plan.PromotionalPlanFee));
    command.Parameters.Add(new SqlParameter("@iPromotional_Only_Offer_Start_Date", plan.PromotionalOnlyOfferStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Only_Offer_End_Date", plan.PromotionalOnlyOfferEndDate));
    command.Parameters.Add(new SqlParameter("@iStandard_Fee", plan.StandardFee));
    command.Parameters.Add(new SqlParameter("@iStudent_Discount_Type", plan.StudentDiscountType));
    command.Parameters.Add(new SqlParameter("@iStudent_Discount_Value", plan.StudentDiscountValue));
    command.Parameters.Add(new SqlParameter("@iBundle_Discount", plan.BundleDiscount));
    command.Parameters.Add(new SqlParameter("@iBundle_Discount_Details", plan.BundleDiscountDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Data", plan.StandardData));
    command.Parameters.Add(new SqlParameter("@iStandard_Data_Online_Sign_Up", plan.StandardDataOnlineSignUp));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Inclusions", plan.StandardDomesticVoiceInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Value", plan.StandardDomesticVoiceValue));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Details", plan.StandardDomesticVoiceDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Inclusions", plan.StandardDomesticTextInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Amount", plan.StandardDomesticTextAmount));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Details", plan.StandardDomesticTextDetails));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Inclusions", plan.InternationalVoice1Inclusions));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Value", plan.InternationalVoice1Value));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Included_In_Plan", plan.InternationalVoice1IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Additional_Cost", plan.InternationalVoice1AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Countries_Amount", plan.InternationalVoice1CountriesAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Countries_List", plan.InternationalVoice1CountriesList));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Inclusions", plan.InternationalVoice2Inclusions));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Value", plan.InternationalVoice2Value));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Included_In_Plan", plan.InternationalVoice2IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Additional_Cost", plan.InternationalVoice2AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Countries_Amount", plan.InternationalVoice2CountriesAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Countries_List", plan.InternationalVoice2CountriesList));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Inclusions", plan.InternationalTextInclusions));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Value", plan.InternationalTextValue));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Countries_Amount", plan.InternationalTextCountriesAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Countries_List", plan.InternationalTextCountriesList));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Details", plan.InternationalTextDetails));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Indicator", plan.InternationalRoamIndicator));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Included_In_Plan", plan.InternationalRoamIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Additional_Cost", plan.InternationalRoamAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Details", plan.InternationalRoamDetails));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Country_Amount", plan.InternationalRoamCountryAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Country_List", plan.InternationalRoamCountryList));
    command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Indicator", plan.FrequentFlyerIndicator));
    command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Program_ID", plan.FrequentFlyerProgramId));
    command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Program_Details", plan.FrequentFlyerProgramDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Indicator", plan.StandardContentIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Indicator", plan.StandardContentSportIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Data_Free", plan.StandardContentSportDataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Included_In_Plan", plan.StandardContentSportIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Additional_Cost", plan.StandardContentSportAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Inclusions", plan.StandardContentSportInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Details", plan.StandardContentSportDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Indicator", plan.StandardContentMusicIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Data_Free", plan.StandardContentMusicDataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Included_In_Plan", plan.StandardContentMusicIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Additional_Cost", plan.StandardContentMusicAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Inclusions", plan.StandardContentMusicInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Details", plan.StandardContentMusicDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Indicator", plan.StandardContentSvodIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Data_Free", plan.StandardContentSvodDataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Included_In_Plan", plan.StandardContentSvodIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Additional_Cost", plan.StandardContentSvodAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Inclusions", plan.StandardContentSvodInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Details", plan.StandardContentSvodDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Indicator", plan.StandardContentOther1Indicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Data_Free", plan.StandardContentOther1DataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Included_In_Plan", plan.StandardContentOther1IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Additional_Cost", plan.StandardContentOther1AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Inclusions", plan.StandardContentOther1Inclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Details", plan.StandardContentOther1Details));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Indicator", plan.StandardContentOther2Indicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Data_Free", plan.StandardContentOther2DataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Included_In_Plan", plan.StandardContentOther2IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Additional_Cost", plan.StandardContentOther2AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Inclusions", plan.StandardContentOther2Inclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Details", plan.StandardContentOther2Details));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Indicator", plan.OtherInclusionsIndicator));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Network_Trial", plan.OtherInclusionsNetworkTrial));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Network_Guarantee", plan.OtherInclusionsNetworkGuarantee));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Data_Share", plan.OtherInclusionsDataShare));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Data_Rollover", plan.OtherInclusionsDataRollover));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Device_TradeUp", plan.OtherInclusionsDeviceTradeUp));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Wifi", plan.OtherInclusionsWifi));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Cloud_Storage", plan.OtherInclusionsCloudStorage));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Refer_Friend", plan.OtherInclusionsReferFriend));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Business_Apps", plan.OtherInclusionsBusinessApps));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Other", plan.OtherInclusionsOther));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Additional_Comments", plan.OtherInclusionsAdditionalComments));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Indicator", plan.PromotionalFeeIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Discount_Type", plan.PromotionalFeeDiscountType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Discount_Value", plan.PromotionalFeeDiscountValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Channel", plan.PromotionalFeeChannel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Details", plan.PromotionalFeeDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Start_Date", plan.PromotionalFeeStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_End_Date", plan.PromotionalFeeEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Indicator", plan.PromotionalStudentDiscountIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Type", plan.PromotionalStudentDiscountType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Value", plan.PromotionalStudentDiscountValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Details", plan.PromotionalStudentDiscountDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Start_Date", plan.PromotionalStudentDiscountStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_End_Date", plan.PromotionalStudentDiscountEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Indicator", plan.PromotionalFeeFreeMonthsIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Amount", plan.PromotionalFeeFreeMonthsAmount));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Channel", plan.PromotionalFeeFreeMonthsChannel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Details", plan.PromotionalFeeFreeMonthsDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Start_Date", plan.PromotionalFeeFreeMonthsStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_End_Date", plan.PromotionalFeeFreeMonthsEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Indicator", plan.PromotionalBonusData1Indicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Amount", plan.PromotionalBonusData1Amount));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Channel", plan.PromotionalBonusData1Channel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Details", plan.PromotionalBonusData1Details));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Start_Date", plan.PromotionalBonusData1StartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_End_Date", plan.PromotionalBonusData1EndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Indicator", plan.PromotionalBonusData2Indicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Amount", plan.PromotionalBonusData2Amount));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Channel", plan.PromotionalBonusData2Channel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Details", plan.PromotionalBonusData2Details));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Start_Date", plan.PromotionalBonusData2StartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_End_Date", plan.PromotionalBonusData2EndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Indicator", plan.PromotionalDomesticVoiceIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Type", plan.PromotionalDomesticVoiceType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Value", plan.PromotionalDomesticVoiceValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Start_Date", plan.PromotionalDomesticVoiceStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_End_Date", plan.PromotionalDomesticVoiceEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Details", plan.PromotionalDomesticVoiceDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Indicator", plan.PromotionalDomesticTextIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Type", plan.PromotionalDomesticTextType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Value", plan.PromotionalDomesticTextValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Start_Date", plan.PromotionalDomesticTextStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_End_Date", plan.PromotionalDomesticTextEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Details", plan.PromotionalDomesticTextDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Indicator", plan.PromotionalInternationalVoiceIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Type", plan.PromotionalInternationalVoiceType));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Value", plan.PromotionalInternationalVoiceValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Country_Amount", plan.PromotionalInternationalVoiceCountryAmount));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Country_List", plan.PromotionalInternationalVoiceCountryList));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Start_Date", plan.PromotionalInternationalVoiceStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Details", plan.PromotionalInternationalVoiceDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_End_Date", plan.PromotionalInternationalVoiceEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Indicator", plan.PromotionalInternationalTextIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Type", plan.PromotionalInternationalTextType));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Value", plan.PromotionalInternationalTextValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Start_Date", plan.PromotionalInternationalTextStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_End_Date", plan.PromotionalInternationalTextEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Details", plan.PromotionalInternationalTextDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Indicator", plan.PromotionalInternationalRoamIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Details", plan.PromotionalInternationalRoamDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Start_Date", plan.PromotionalInternationalRoamStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_End_Date", plan.PromotionalInternationalRoamEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Indicator", plan.PromotionalFrequentFlyerIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Details", plan.PromotionalFrequentFlyerDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Start_Date", plan.PromotionalFrequentFlyerStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_End_Date", plan.PromotionalFrequentFlyerEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Indicator", plan.PromotionalContentIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport", plan.PromotionalContentSport));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport_Start_Date", plan.PromotionalContentSportStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport_End_Date", plan.PromotionalContentSportEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music", plan.PromotionalContentMusic));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music_Start_Date", plan.PromotionalContentMusicStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music_End_Date", plan.PromotionalContentMusicEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD", plan.PromotionalContentSvod));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD_Start_Date", plan.PromotionalContentSvodStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD_End_Date", plan.PromotionalContentSvodEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_Details", plan.PromotionalContentOtherDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_Start_Date", plan.PromotionalContentOtherStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_End_Date", plan.PromotionalContentOtherEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Indicator", plan.PromotionalOtherIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Description", plan.PromotionalOtherDescription)); 
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Details", plan.PromotionalOtherDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Start_Date", plan.PromotionalOtherStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_End_Date", plan.PromotionalOtherEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Inclusions_Additonal_Comments", plan.PromotionalInclusionsAdditonalComments));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_1", plan.PlaceholderAttribute1));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_2", plan.PlaceholderAttribute2));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_3", plan.PlaceholderAttribute3));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_4", plan.PlaceholderAttribute4));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_5", plan.PlaceholderAttribute5));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_6", plan.PlaceholderAttribute6));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_7", plan.PlaceholderAttribute7));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_8", plan.PlaceholderAttribute8));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_9", plan.PlaceholderAttribute9));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_10", plan.PlaceholderAttribute10));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_11", plan.PlaceholderAttribute11));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_12", plan.PlaceholderAttribute12));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_13", plan.PlaceholderAttribute13));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_14", plan.PlaceholderAttribute14));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_15", plan.PlaceholderAttribute15));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_16", plan.PlaceholderAttribute16));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_17", plan.PlaceholderAttribute17));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_18", plan.PlaceholderAttribute18));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_19", plan.PlaceholderAttribute19));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_20", plan.PlaceholderAttribute20));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_21", plan.PlaceholderAttribute21));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_22", plan.PlaceholderAttribute22));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_23", plan.PlaceholderAttribute23));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_24", plan.PlaceholderAttribute24));
        //New params for prepaid changes
    command.Parameters.Add(new SqlParameter("@iStandard_Fee_Payment_Terms_Details", plan.StandardFeePaymentTermsDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Indicator", plan.StandardContentOther3Indicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Data_Free", plan.StandardContentOther3DataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Included_In_Plan", plan.StandardContentOther3IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Additional_Cost", plan.StandardContentOther3AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Inclusions", plan.StandardContentOther3Inclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Details", plan.StandardContentOther3Details));



    command.Parameters.Add(new SqlParameter("@oError", outputParam));

    for (int i = 0; i < command.Parameters.Count; i++)
    {
      if (command.Parameters[i].Value == null)
      {
        command.Parameters[i].Value = DBNull.Value;
    }
}

db.Database.OpenConnection();
command.ExecuteNonQuery();
}
return;
}


[HttpPost("Prepaid")]
public void PostPrepaid([FromBody]MobilePlan plan)
{
  int outputParam = 0;

  using (var command = db.Database.GetDbConnection().CreateCommand())
  {
    command.CommandText = "ADD_NEW_PREPAID";
    command.CommandType = System.Data.CommandType.StoredProcedure;

    command.Parameters.Add(new SqlParameter("@iUsername", plan.Username));
    command.Parameters.Add(new SqlParameter("@iProduct_Status", plan.ProductStatus));
    command.Parameters.Add(new SqlParameter("@iProduct_Category_ID", plan.ProductCategoryId));
    command.Parameters.Add(new SqlParameter("@iSegment", plan.Segment));
    command.Parameters.Add(new SqlParameter("@iBrand_ID", plan.BrandId));
    command.Parameters.Add(new SqlParameter("@iProduct_Type_ID", plan.ProductTypeId));
    command.Parameters.Add(new SqlParameter("@iTerm_ID", plan.TermId));
    command.Parameters.Add(new SqlParameter("@iProduct_Name_ID", plan.ProductNameId));
    command.Parameters.Add(new SqlParameter("@iProduct_Change_Type", plan.ProductChangeType));
    command.Parameters.Add(new SqlParameter("@iPlan_Type", plan.PlanType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Plan_Fee", plan.PromotionalPlanFee));
    command.Parameters.Add(new SqlParameter("@iPromotional_Only_Offer_Start_Date", plan.PromotionalOnlyOfferStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Only_Offer_End_Date", plan.PromotionalOnlyOfferEndDate));
    command.Parameters.Add(new SqlParameter("@iStandard_Fee", plan.StandardFee));
    command.Parameters.Add(new SqlParameter("@iStandard_Fee_Payment_Terms_Details", plan.StandardFeePaymentTermsDetails));
    command.Parameters.Add(new SqlParameter("@iStudent_Discount_Type", plan.StudentDiscountType));
    command.Parameters.Add(new SqlParameter("@iStudent_Discount_Value", plan.StudentDiscountValue));
    command.Parameters.Add(new SqlParameter("@iBundle_Discount", plan.BundleDiscount));
    command.Parameters.Add(new SqlParameter("@iBundle_Discount_Details", plan.BundleDiscountDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Data", plan.StandardData));
    command.Parameters.Add(new SqlParameter("@iStandard_Data_Online_Sign_Up", plan.StandardDataOnlineSignUp));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Inclusions", plan.StandardDomesticVoiceInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Value", plan.StandardDomesticVoiceValue));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Details", plan.StandardDomesticVoiceDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Customer_To_Customer_Indicator", plan.StandardDomesticCustomerToCustomerIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Customer_To_Customer_Value", plan.StandardDomesticCustomerToCustomerValue));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Customer_To_Customer_Details", plan.StandardDomesticCustomerToCustomerDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Inclusions", plan.StandardDomesticTextInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Amount", plan.StandardDomesticTextAmount));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Details", plan.StandardDomesticTextDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Prepaid_Extra_Credit_Indicator", plan.StandardDomesticPrepaidExtraCreditIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Prepaid_Extra_Credit_Value", plan.StandardDomesticPrepaidExtraCreditValue));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Prepaid_Extra_Credit_Details", plan.StandardDomesticPrepaidExtraCreditDetails));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Inclusions", plan.InternationalVoice1Inclusions));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Value", plan.InternationalVoice1Value));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Included_In_Plan", plan.InternationalVoice1IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Additional_Cost", plan.InternationalVoice1AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Countries_Amount", plan.InternationalVoice1CountriesAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Countries_List", plan.InternationalVoice1CountriesList));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Inclusions", plan.InternationalVoice2Inclusions));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Value", plan.InternationalVoice2Value));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Included_In_Plan", plan.InternationalVoice2IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Additional_Cost", plan.InternationalVoice2AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Countries_Amount", plan.InternationalVoice2CountriesAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Countries_List", plan.InternationalVoice2CountriesList));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Inclusions", plan.InternationalTextInclusions));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Value", plan.InternationalTextValue));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Countries_Amount", plan.InternationalTextCountriesAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Countries_List", plan.InternationalTextCountriesList));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Details", plan.InternationalTextDetails));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Indicator", plan.InternationalRoamIndicator));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Included_In_Plan", plan.InternationalRoamIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Additional_Cost", plan.InternationalRoamAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Details", plan.InternationalRoamDetails));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Country_Amount", plan.InternationalRoamCountryAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Country_List", plan.InternationalRoamCountryList));
    command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Indicator", plan.FrequentFlyerIndicator));
    command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Program_ID", plan.FrequentFlyerProgramId));
    command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Program_Details", plan.FrequentFlyerProgramDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Indicator", plan.StandardContentIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Indicator", plan.StandardContentSportIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Data_Free", plan.StandardContentSportDataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Included_In_Plan", plan.StandardContentSportIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Additional_Cost", plan.StandardContentSportAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Inclusions", plan.StandardContentSportInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Details", plan.StandardContentSportDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Indicator", plan.StandardContentMusicIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Data_Free", plan.StandardContentMusicDataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Included_In_Plan", plan.StandardContentMusicIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Additional_Cost", plan.StandardContentMusicAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Inclusions", plan.StandardContentMusicInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Details", plan.StandardContentMusicDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Indicator", plan.StandardContentSvodIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Data_Free", plan.StandardContentSvodDataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Included_In_Plan", plan.StandardContentSvodIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Additional_Cost", plan.StandardContentSvodAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Inclusions", plan.StandardContentSvodInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Details", plan.StandardContentSvodDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Indicator", plan.StandardContentOther1Indicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Data_Free", plan.StandardContentOther1DataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Included_In_Plan", plan.StandardContentOther1IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Additional_Cost", plan.StandardContentOther1AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Inclusions", plan.StandardContentOther1Inclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Details", plan.StandardContentOther1Details));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Indicator", plan.StandardContentOther2Indicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Data_Free", plan.StandardContentOther2DataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Included_In_Plan", plan.StandardContentOther2IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Additional_Cost", plan.StandardContentOther2AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Inclusions", plan.StandardContentOther2Inclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Details", plan.StandardContentOther2Details));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Indicator", plan.StandardContentOther3Indicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Data_Free", plan.StandardContentOther3DataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Included_In_Plan", plan.StandardContentOther3IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Additional_Cost", plan.StandardContentOther3AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Inclusions", plan.StandardContentOther3Inclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Details", plan.StandardContentOther3Details));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Indicator", plan.OtherInclusionsIndicator));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Network_Trial", plan.OtherInclusionsNetworkTrial));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Network_Guarantee", plan.OtherInclusionsNetworkGuarantee));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Data_Share", plan.OtherInclusionsDataShare));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Data_Rollover", plan.OtherInclusionsDataRollover));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Device_TradeUp", plan.OtherInclusionsDeviceTradeUp));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Wifi", plan.OtherInclusionsWifi));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Cloud_Storage", plan.OtherInclusionsCloudStorage));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Refer_Friend", plan.OtherInclusionsReferFriend));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Business_Apps", plan.OtherInclusionsBusinessApps));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Prepaid_Autopay", plan.OtherInclusionsPrepaidAutopay));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Other", plan.OtherInclusionsOther));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Additional_Comments", plan.OtherInclusionsAdditionalComments));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Indicator", plan.PromotionalFeeIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Discount_Type", plan.PromotionalFeeDiscountType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Discount_Value", plan.PromotionalFeeDiscountValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Channel", plan.PromotionalFeeChannel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Details", plan.PromotionalFeeDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Start_Date", plan.PromotionalFeeStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_End_Date", plan.PromotionalFeeEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Indicator", plan.PromotionalStudentDiscountIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Type", plan.PromotionalStudentDiscountType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Value", plan.PromotionalStudentDiscountValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Details", plan.PromotionalStudentDiscountDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Start_Date", plan.PromotionalStudentDiscountStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_End_Date", plan.PromotionalStudentDiscountEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Indicator", plan.PromotionalFeeFreeMonthsIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Amount", plan.PromotionalFeeFreeMonthsAmount));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Channel", plan.PromotionalFeeFreeMonthsChannel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Details", plan.PromotionalFeeFreeMonthsDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Start_Date", plan.PromotionalFeeFreeMonthsStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_End_Date", plan.PromotionalFeeFreeMonthsEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_Indicator", plan.PromotionalPrepaidBonusCreditIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_Value", plan.PromotionalPrepaidBonusCreditValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_Details", plan.PromotionalPrepaidBonusCreditDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_Start", plan.PromotionalPrepaidBonusCreditStart));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_End", plan.PromotionalPrepaidBonusCreditEnd));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Indicator", plan.PromotionalBonusData1Indicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Amount", plan.PromotionalBonusData1Amount));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Channel", plan.PromotionalBonusData1Channel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Details", plan.PromotionalBonusData1Details));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Start_Date", plan.PromotionalBonusData1StartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_End_Date", plan.PromotionalBonusData1EndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Indicator", plan.PromotionalBonusData2Indicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Amount", plan.PromotionalBonusData2Amount));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Channel", plan.PromotionalBonusData2Channel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Details", plan.PromotionalBonusData2Details));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Start_Date", plan.PromotionalBonusData2StartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_End_Date", plan.PromotionalBonusData2EndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Indicator", plan.PromotionalDomesticVoiceIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Type", plan.PromotionalDomesticVoiceType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Value", plan.PromotionalDomesticVoiceValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Start_Date", plan.PromotionalDomesticVoiceStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_End_Date", plan.PromotionalDomesticVoiceEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Details", plan.PromotionalDomesticVoiceDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Indicator", plan.PromotionalDomesticTextIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Type", plan.PromotionalDomesticTextType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Value", plan.PromotionalDomesticTextValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Start_Date", plan.PromotionalDomesticTextStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_End_Date", plan.PromotionalDomesticTextEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Details", plan.PromotionalDomesticTextDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Indicator", plan.PromotionalInternationalVoiceIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Type", plan.PromotionalInternationalVoiceType));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Value", plan.PromotionalInternationalVoiceValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Country_Amount", plan.PromotionalInternationalVoiceCountryAmount));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Country_List", plan.PromotionalInternationalVoiceCountryList));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Start_Date", plan.PromotionalInternationalVoiceStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Details", plan.PromotionalInternationalVoiceDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_End_Date", plan.PromotionalInternationalVoiceEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Indicator", plan.PromotionalInternationalTextIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Type", plan.PromotionalInternationalTextType));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Value", plan.PromotionalInternationalTextValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Start_Date", plan.PromotionalInternationalTextStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_End_Date", plan.PromotionalInternationalTextEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Details", plan.PromotionalInternationalTextDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Indicator", plan.PromotionalInternationalRoamIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Details", plan.PromotionalInternationalRoamDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Start_Date", plan.PromotionalInternationalRoamStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_End_Date", plan.PromotionalInternationalRoamEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_Indicator", plan.PromotionalPrepaidInternationalBonusCreditsIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_Value", plan.PromotionalPrepaidInternationalBonusCreditsValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_Details", plan.PromotionalPrepaidInternationalBonusCreditsDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_Start", plan.PromotionalPrepaidInternationalBonusCreditsStart));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_End", plan.PromotionalPrepaidInternationalBonusCreditsEnd));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Indicator", plan.PromotionalFrequentFlyerIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Details", plan.PromotionalFrequentFlyerDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Start_Date", plan.PromotionalFrequentFlyerStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_End_Date", plan.PromotionalFrequentFlyerEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Indicator", plan.PromotionalContentIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport", plan.PromotionalContentSport));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport_Start_Date", plan.PromotionalContentSportStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport_End_Date", plan.PromotionalContentSportEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music", plan.PromotionalContentMusic));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music_Start_Date", plan.PromotionalContentMusicStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music_End_Date", plan.PromotionalContentMusicEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD", plan.PromotionalContentSvod));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD_Start_Date", plan.PromotionalContentSvodStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD_End_Date", plan.PromotionalContentSvodEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_Details", plan.PromotionalContentOtherDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_Start_Date", plan.PromotionalContentOtherStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_End_Date", plan.PromotionalContentOtherEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Indicator", plan.PromotionalOtherIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Description", plan.PromotionalOtherDescription));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Details", plan.PromotionalOtherDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Start_Date", plan.PromotionalOtherStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_End_Date", plan.PromotionalOtherEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Inclusions_Additonal_Comments", plan.PromotionalInclusionsAdditonalComments));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_1", plan.PlaceholderAttribute1));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_2", plan.PlaceholderAttribute2));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_3", plan.PlaceholderAttribute3));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_4", plan.PlaceholderAttribute4));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_5", plan.PlaceholderAttribute5));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_6", plan.PlaceholderAttribute6));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_7", plan.PlaceholderAttribute7));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_8", plan.PlaceholderAttribute8));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_9", plan.PlaceholderAttribute9));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_10", plan.PlaceholderAttribute10));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_11", plan.PlaceholderAttribute11));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_12", plan.PlaceholderAttribute12));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_13", plan.PlaceholderAttribute13));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_14", plan.PlaceholderAttribute14));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_15", plan.PlaceholderAttribute15));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_16", plan.PlaceholderAttribute16));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_17", plan.PlaceholderAttribute17));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_18", plan.PlaceholderAttribute18));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_19", plan.PlaceholderAttribute19));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_20", plan.PlaceholderAttribute20));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_21", plan.PlaceholderAttribute21));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_22", plan.PlaceholderAttribute22));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_23", plan.PlaceholderAttribute23));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_24", plan.PlaceholderAttribute24));

    command.Parameters.Add(new SqlParameter("@oError", outputParam));


    for (int i = 0; i < command.Parameters.Count; i++)
    {
      if (command.Parameters[i].Value == null)
      {
        command.Parameters[i].Value = DBNull.Value;
    }
}

db.Database.OpenConnection();
command.ExecuteNonQuery();
}

return;
}

    // PUT api/values/5
[HttpPut("Prepaid")]
public void PutPrepaid([FromBody]MobilePlan plan)
{
  int outputParam = 0;

  using (var command = db.Database.GetDbConnection().CreateCommand())
  {
    command.CommandText = "ADD_UPDATE_PREPAID";
    command.CommandType = System.Data.CommandType.StoredProcedure;

    command.Parameters.Add(new SqlParameter("@iPrepaid_ID", plan.Id));
    command.Parameters.Add(new SqlParameter("@iUsername", plan.Username));
    command.Parameters.Add(new SqlParameter("@iProduct_Status", plan.ProductStatus));
    command.Parameters.Add(new SqlParameter("@iProduct_Category_ID", plan.ProductCategoryId));
    command.Parameters.Add(new SqlParameter("@iSegment", plan.Segment));
    command.Parameters.Add(new SqlParameter("@iBrand_ID", plan.BrandId));
    command.Parameters.Add(new SqlParameter("@iProduct_Type_ID", plan.ProductTypeId));
    command.Parameters.Add(new SqlParameter("@iTerm_ID", plan.TermId));
    command.Parameters.Add(new SqlParameter("@iProduct_Name_ID", plan.ProductNameId));
    command.Parameters.Add(new SqlParameter("@iProduct_Change_Type", plan.ProductChangeType));
    command.Parameters.Add(new SqlParameter("@iPlan_Type", plan.PlanType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Plan_Fee", plan.PromotionalPlanFee));
    command.Parameters.Add(new SqlParameter("@iPromotional_Only_Offer_Start_Date", plan.PromotionalOnlyOfferStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Only_Offer_End_Date", plan.PromotionalOnlyOfferEndDate));
    command.Parameters.Add(new SqlParameter("@iStandard_Fee", plan.StandardFee));
    command.Parameters.Add(new SqlParameter("@iStandard_Fee_Payment_Terms_Details", plan.StandardFeePaymentTermsDetails));
    command.Parameters.Add(new SqlParameter("@iStudent_Discount_Type", plan.StudentDiscountType));
    command.Parameters.Add(new SqlParameter("@iStudent_Discount_Value", plan.StudentDiscountValue));
    command.Parameters.Add(new SqlParameter("@iBundle_Discount", plan.BundleDiscount));
    command.Parameters.Add(new SqlParameter("@iBundle_Discount_Details", plan.BundleDiscountDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Data", plan.StandardData));
    command.Parameters.Add(new SqlParameter("@iStandard_Data_Online_Sign_Up", plan.StandardDataOnlineSignUp));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Inclusions", plan.StandardDomesticVoiceInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Value", plan.StandardDomesticVoiceValue));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Voice_Details", plan.StandardDomesticVoiceDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Customer_To_Customer_Indicator", plan.StandardDomesticCustomerToCustomerIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Customer_To_Customer_Value", plan.StandardDomesticCustomerToCustomerValue));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Customer_To_Customer_Details", plan.StandardDomesticCustomerToCustomerDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Inclusions", plan.StandardDomesticTextInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Amount", plan.StandardDomesticTextAmount));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Text_Details", plan.StandardDomesticTextDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Prepaid_Extra_Credit_Indicator", plan.StandardDomesticPrepaidExtraCreditIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Prepaid_Extra_Credit_Value", plan.StandardDomesticPrepaidExtraCreditValue));
    command.Parameters.Add(new SqlParameter("@iStandard_Domestic_Prepaid_Extra_Credit_Details", plan.StandardDomesticPrepaidExtraCreditDetails));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Inclusions", plan.InternationalVoice1Inclusions));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Value", plan.InternationalVoice1Value));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Included_In_Plan", plan.InternationalVoice1IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Additional_Cost", plan.InternationalVoice1AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Countries_Amount", plan.InternationalVoice1CountriesAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_1_Countries_List", plan.InternationalVoice1CountriesList));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Inclusions", plan.InternationalVoice2Inclusions));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Value", plan.InternationalVoice2Value));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Included_In_Plan", plan.InternationalVoice2IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Additional_Cost", plan.InternationalVoice2AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Countries_Amount", plan.InternationalVoice2CountriesAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Voice_2_Countries_List", plan.InternationalVoice2CountriesList));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Inclusions", plan.InternationalTextInclusions));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Value", plan.InternationalTextValue));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Countries_Amount", plan.InternationalTextCountriesAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Countries_List", plan.InternationalTextCountriesList));
    command.Parameters.Add(new SqlParameter("@iInternational_Text_Details", plan.InternationalTextDetails));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Indicator", plan.InternationalRoamIndicator));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Included_In_Plan", plan.InternationalRoamIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Additional_Cost", plan.InternationalRoamAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Details", plan.InternationalRoamDetails));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Country_Amount", plan.InternationalRoamCountryAmount));
    command.Parameters.Add(new SqlParameter("@iInternational_Roam_Country_List", plan.InternationalRoamCountryList));
    command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Indicator", plan.FrequentFlyerIndicator));
    command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Program_ID", plan.FrequentFlyerProgramId));
    command.Parameters.Add(new SqlParameter("@iFrequent_Flyer_Program_Details", plan.FrequentFlyerProgramDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Indicator", plan.StandardContentIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Indicator", plan.StandardContentSportIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Data_Free", plan.StandardContentSportDataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Included_In_Plan", plan.StandardContentSportIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Additional_Cost", plan.StandardContentSportAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Inclusions", plan.StandardContentSportInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Sport_Details", plan.StandardContentSportDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Indicator", plan.StandardContentMusicIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Data_Free", plan.StandardContentMusicDataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Included_In_Plan", plan.StandardContentMusicIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Additional_Cost", plan.StandardContentMusicAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Inclusions", plan.StandardContentMusicInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Music_Details", plan.StandardContentMusicDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Indicator", plan.StandardContentSvodIndicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Data_Free", plan.StandardContentSvodDataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Included_In_Plan", plan.StandardContentSvodIncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Additional_Cost", plan.StandardContentSvodAdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Inclusions", plan.StandardContentSvodInclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_SVOD_Details", plan.StandardContentSvodDetails));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Indicator", plan.StandardContentOther1Indicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Data_Free", plan.StandardContentOther1DataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Included_In_Plan", plan.StandardContentOther1IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Additional_Cost", plan.StandardContentOther1AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Inclusions", plan.StandardContentOther1Inclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_1_Details", plan.StandardContentOther1Details));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Indicator", plan.StandardContentOther2Indicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Data_Free", plan.StandardContentOther2DataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Included_In_Plan", plan.StandardContentOther2IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Additional_Cost", plan.StandardContentOther2AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Inclusions", plan.StandardContentOther2Inclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_2_Details", plan.StandardContentOther2Details));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Indicator", plan.StandardContentOther3Indicator));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Data_Free", plan.StandardContentOther3DataFree));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Included_In_Plan", plan.StandardContentOther3IncludedInPlan));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Additional_Cost", plan.StandardContentOther3AdditionalCost));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Inclusions", plan.StandardContentOther3Inclusions));
    command.Parameters.Add(new SqlParameter("@iStandard_Content_Other_3_Details", plan.StandardContentOther3Details));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Indicator", plan.OtherInclusionsIndicator));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Network_Trial", plan.OtherInclusionsNetworkTrial));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Network_Guarantee", plan.OtherInclusionsNetworkGuarantee));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Data_Share", plan.OtherInclusionsDataShare));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Data_Rollover", plan.OtherInclusionsDataRollover));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Device_TradeUp", plan.OtherInclusionsDeviceTradeUp));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Wifi", plan.OtherInclusionsWifi));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Cloud_Storage", plan.OtherInclusionsCloudStorage));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Refer_Friend", plan.OtherInclusionsReferFriend));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Business_Apps", plan.OtherInclusionsBusinessApps));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Prepaid_Autopay", plan.OtherInclusionsPrepaidAutopay));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Other", plan.OtherInclusionsOther));
    command.Parameters.Add(new SqlParameter("@iOther_Inclusions_Additional_Comments", plan.OtherInclusionsAdditionalComments));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Indicator", plan.PromotionalFeeIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Discount_Type", plan.PromotionalFeeDiscountType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Discount_Value", plan.PromotionalFeeDiscountValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Channel", plan.PromotionalFeeChannel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Details", plan.PromotionalFeeDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Start_Date", plan.PromotionalFeeStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_End_Date", plan.PromotionalFeeEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Indicator", plan.PromotionalStudentDiscountIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Type", plan.PromotionalStudentDiscountType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Value", plan.PromotionalStudentDiscountValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Details", plan.PromotionalStudentDiscountDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_Start_Date", plan.PromotionalStudentDiscountStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Student_Discount_End_Date", plan.PromotionalStudentDiscountEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Indicator", plan.PromotionalFeeFreeMonthsIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Amount", plan.PromotionalFeeFreeMonthsAmount));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Channel", plan.PromotionalFeeFreeMonthsChannel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Details", plan.PromotionalFeeFreeMonthsDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_Start_Date", plan.PromotionalFeeFreeMonthsStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Fee_Free_Months_End_Date", plan.PromotionalFeeFreeMonthsEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_Indicator", plan.PromotionalPrepaidBonusCreditIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_Value", plan.PromotionalPrepaidBonusCreditValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_Details", plan.PromotionalPrepaidBonusCreditDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_Start", plan.PromotionalPrepaidBonusCreditStart));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_Bonus_Credit_End", plan.PromotionalPrepaidBonusCreditEnd));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Indicator", plan.PromotionalBonusData1Indicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Amount", plan.PromotionalBonusData1Amount));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Channel", plan.PromotionalBonusData1Channel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Details", plan.PromotionalBonusData1Details));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_Start_Date", plan.PromotionalBonusData1StartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_1_End_Date", plan.PromotionalBonusData1EndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Indicator", plan.PromotionalBonusData2Indicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Amount", plan.PromotionalBonusData2Amount));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Channel", plan.PromotionalBonusData2Channel));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Details", plan.PromotionalBonusData2Details));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_Start_Date", plan.PromotionalBonusData2StartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Bonus_Data_2_End_Date", plan.PromotionalBonusData2EndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Indicator", plan.PromotionalDomesticVoiceIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Type", plan.PromotionalDomesticVoiceType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Value", plan.PromotionalDomesticVoiceValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Start_Date", plan.PromotionalDomesticVoiceStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_End_Date", plan.PromotionalDomesticVoiceEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Voice_Details", plan.PromotionalDomesticVoiceDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Indicator", plan.PromotionalDomesticTextIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Type", plan.PromotionalDomesticTextType));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Value", plan.PromotionalDomesticTextValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Start_Date", plan.PromotionalDomesticTextStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_End_Date", plan.PromotionalDomesticTextEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Domestic_Text_Details", plan.PromotionalDomesticTextDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Indicator", plan.PromotionalInternationalVoiceIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Type", plan.PromotionalInternationalVoiceType));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Value", plan.PromotionalInternationalVoiceValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Country_Amount", plan.PromotionalInternationalVoiceCountryAmount));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Country_List", plan.PromotionalInternationalVoiceCountryList));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Start_Date", plan.PromotionalInternationalVoiceStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_Details", plan.PromotionalInternationalVoiceDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Voice_End_Date", plan.PromotionalInternationalVoiceEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Indicator", plan.PromotionalInternationalTextIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Type", plan.PromotionalInternationalTextType));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Value", plan.PromotionalInternationalTextValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Start_Date", plan.PromotionalInternationalTextStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_End_Date", plan.PromotionalInternationalTextEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Text_Details", plan.PromotionalInternationalTextDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Indicator", plan.PromotionalInternationalRoamIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Details", plan.PromotionalInternationalRoamDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_Start_Date", plan.PromotionalInternationalRoamStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_International_Roam_End_Date", plan.PromotionalInternationalRoamEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_Indicator", plan.PromotionalPrepaidInternationalBonusCreditsIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_Value", plan.PromotionalPrepaidInternationalBonusCreditsValue));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_Details", plan.PromotionalPrepaidInternationalBonusCreditsDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_Start", plan.PromotionalPrepaidInternationalBonusCreditsStart));
    command.Parameters.Add(new SqlParameter("@iPromotional_Prepaid_International_Bonus_Credits_End", plan.PromotionalPrepaidInternationalBonusCreditsEnd));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Indicator", plan.PromotionalFrequentFlyerIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Details", plan.PromotionalFrequentFlyerDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_Start_Date", plan.PromotionalFrequentFlyerStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Frequent_Flyer_End_Date", plan.PromotionalFrequentFlyerEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Indicator", plan.PromotionalContentIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport", plan.PromotionalContentSport));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport_Start_Date", plan.PromotionalContentSportStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Sport_End_Date", plan.PromotionalContentSportEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music", plan.PromotionalContentMusic));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music_Start_Date", plan.PromotionalContentMusicStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Music_End_Date", plan.PromotionalContentMusicEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD", plan.PromotionalContentSvod));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD_Start_Date", plan.PromotionalContentSvodStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_SVOD_End_Date", plan.PromotionalContentSvodEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_Details", plan.PromotionalContentOtherDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_Start_Date", plan.PromotionalContentOtherStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Content_Other_End_Date", plan.PromotionalContentOtherEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Indicator", plan.PromotionalOtherIndicator));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Description", plan.PromotionalOtherDescription));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Details", plan.PromotionalOtherDetails));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_Start_Date", plan.PromotionalOtherStartDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Other_End_Date", plan.PromotionalOtherEndDate));
    command.Parameters.Add(new SqlParameter("@iPromotional_Inclusions_Additonal_Comments", plan.PromotionalInclusionsAdditonalComments));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_1", plan.PlaceholderAttribute1));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_2", plan.PlaceholderAttribute2));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_3", plan.PlaceholderAttribute3));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_4", plan.PlaceholderAttribute4));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_5", plan.PlaceholderAttribute5));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_6", plan.PlaceholderAttribute6));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_7", plan.PlaceholderAttribute7));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_8", plan.PlaceholderAttribute8));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_9", plan.PlaceholderAttribute9));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_10", plan.PlaceholderAttribute10));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_11", plan.PlaceholderAttribute11));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_12", plan.PlaceholderAttribute12));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_13", plan.PlaceholderAttribute13));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_14", plan.PlaceholderAttribute14));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_15", plan.PlaceholderAttribute15));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_16", plan.PlaceholderAttribute16));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_17", plan.PlaceholderAttribute17));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_18", plan.PlaceholderAttribute18));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_19", plan.PlaceholderAttribute19));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_20", plan.PlaceholderAttribute20));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_21", plan.PlaceholderAttribute21));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_22", plan.PlaceholderAttribute22));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_23", plan.PlaceholderAttribute23));
    command.Parameters.Add(new SqlParameter("@iPlaceholder_Attribute_24", plan.PlaceholderAttribute24));

    command.Parameters.Add(new SqlParameter("@oError", outputParam));

    for (int i = 0; i < command.Parameters.Count; i++)
    {
      if (command.Parameters[i].Value == null)
      {
        command.Parameters[i].Value = DBNull.Value;
    }
}

db.Database.OpenConnection();
command.ExecuteNonQuery();
}
return;
}

[HttpPut("toggleprepaid")]
public void togglePrepaid([FromBody] HidePlan plan){

    db.Database.OpenConnection();
    string updSql = @"UPDATE PREPAID SET Hidden = " + plan.Hidden + " Where Prepaid_ID = " + plan.Id + " AND Date_Time_Created = '" + plan.Date_Time_Created.ToString("yyyy-MM-ddTHH:mm:ss.fff") +"'";

    using (var command = db.Database.GetDbConnection().CreateCommand())
    {
        command.CommandText = updSql;
        db.Database.OpenConnection();
        var rowsAffected=command.ExecuteNonQuery();
        Console.WriteLine(rowsAffected);
    }
}

[HttpPut("togglePostpaid")]
public void togglePostpaid([FromBody] HidePlan plan){
   db.Database.OpenConnection();

   string updSql = @"UPDATE Postpaid SET Hidden =" + plan.Hidden + " Where Postpaid_ID = " + plan.Id + " AND Date_Time_Created = '" + plan.Date_Time_Created.ToString("yyyy-MM-ddTHH:mm:ss.fff") +"'";

   using (var command = db.Database.GetDbConnection().CreateCommand())
   {
    command.CommandText = updSql;
    db.Database.OpenConnection();
    var rowsAffected=command.ExecuteNonQuery();
    Console.WriteLine(rowsAffected);
}
}


    // DELETE api/values/5
[HttpDelete("{id}")]
public void Delete(int id)
{
}
}
}
