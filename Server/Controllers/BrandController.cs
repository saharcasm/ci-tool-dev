﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CI_Tool_API.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CI_Tool_API.Controllers
{
    [Route("api/[controller]")]
    public class BrandController : Controller
    {
    CI_ToolContext db;

    public BrandController(CI_ToolContext context)
    {
      db = context;
    }

    // GET: api/values
    [HttpGet]
        public IEnumerable<Brand> Get()
        {
            return db.Brand.ToList();
        }

    // GET: api/values
    [HttpGet("Active")]
    public IEnumerable<Brand> GetActiveBrands()
    {
      DateTime endDate = new DateTime(9999, 12, 31);
      return db.Brand.Where(r => r.EndDateTime == endDate && r.RecordStatus);
    }

    // GET api/values/5
    [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
